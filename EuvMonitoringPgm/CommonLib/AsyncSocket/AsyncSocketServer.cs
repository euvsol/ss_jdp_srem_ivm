﻿using System;
using System.Net;
using System.Net.Sockets;

namespace AsyncSocket
{
    public class AsyncSocketServer : AsyncSocketClass
    {
        private const int backLog = 100;
        private Socket listener;

        public AsyncSocketServer(int ServerNum)
        {
            this.servernum = ServerNum;
        }

        private void StartAccept()
        {
            try
            {
                this.listener.BeginAccept(new AsyncCallback(this.OnListenCallBack), (object) this.listener);
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        private void OnListenCallBack(IAsyncResult ar)
        {
            try
            {
                this.Accepted(new AsyncSocketAcceptEventArgs(this.servernum, ((Socket) ar.AsyncState).EndAccept(ar)));
                this.StartAccept();
            }
            catch (Exception ex)
            {
            }
        }

        public void Listen()
        {
            this.Listen(IPAddress.Any, 3333);
        }

        public void Listen(IPAddress _Addr, int _Port)
        {
            try
            {
                this.listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                this.listener.Bind((EndPoint) new IPEndPoint(_Addr, _Port));
                this.listener.Listen(100);
                this.StartAccept();
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        public void Stop()
        {
            try
            {
                if (this.listener == null || !this.listener.IsBound)
                    return;
                this.listener.Close(100);
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }
    }
}
