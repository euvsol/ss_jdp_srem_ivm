﻿using System;
using System.Net;
using System.Net.Sockets;

namespace AsyncSocket
{
    public class AsyncSocketClient : AsyncSocketClass
    {
        private Socket conn = (Socket) null;

        public AsyncSocketClient(int id)
        {
            this.id = id;
        }

        public AsyncSocketClient(int servernum, int id, Socket conn)
        {
            this.servernum = servernum;
            this.id = id;
            this.conn = conn;
        }

        public Socket Connection
        {
            get
            {
                return this.conn;
            }
            set
            {
                this.conn = value;
            }
        }

        public bool Connect(string hostAddress, int port)
        {
            try
            {
                IPEndPoint ipEndPoint = new IPEndPoint(Dns.GetHostAddresses(hostAddress)[0], port);
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.BeginConnect((EndPoint) ipEndPoint, new AsyncCallback(this.OnConnectCallback), (object) socket);
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
                return false;
            }
            return true;
        }

        private void OnConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket asyncState = (Socket) ar.AsyncState;
                asyncState.EndConnect(ar);
                this.conn = asyncState;
                this.Receive();
                this.Connected(new AsyncSocketConnectionEventArgs(this.servernum, this.id));
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        public void Receive()
        {
            try
            {
                StateObject stateObject = new StateObject(this.conn);
                stateObject.Worker.BeginReceive(stateObject.Buffer, 0, stateObject.BufferSize, SocketFlags.None, new AsyncCallback(this.OnReceiveCallBack), (object) stateObject);
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        private void OnReceiveCallBack(IAsyncResult ar)
        {
            try
            {
                StateObject asyncState = (StateObject) ar.AsyncState;
                int receiveBytes = asyncState.Worker.EndReceive(ar);
                if (receiveBytes <= 0)
                {
                    this.Close();
                }
                else
                {
                    AsyncSocketReceiveEventArgs e = new AsyncSocketReceiveEventArgs(this.servernum, this.id, receiveBytes, asyncState.Buffer);
                    if (receiveBytes > 0)
                        this.Received(e);
                    this.Receive();
                }
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        public bool Send(byte[] buffer)
        {
            try
            {
                Socket conn = this.conn;
                conn.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(this.OnSendCallBack), (object) conn);
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
                return false;
            }
            return true;
        }

        private void OnSendCallBack(IAsyncResult ar)
        {
            try
            {
                this.Sent(new AsyncSocketSendEventArgs(this.servernum, this.id, ((Socket) ar.AsyncState).EndSend(ar)));
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        public void Close()
        {
            try
            {
                Socket conn = this.conn;
                conn.Shutdown(SocketShutdown.Both);
                conn.BeginDisconnect(false, new AsyncCallback(this.OnCloseCallBack), (object) conn);
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }

        private void OnCloseCallBack(IAsyncResult ar)
        {
            try
            {
                Socket asyncState = (Socket) ar.AsyncState;
                asyncState.EndDisconnect(ar);
                asyncState.Close();
                this.Closed(new AsyncSocketConnectionEventArgs(this.servernum, this.id));
            }
            catch (Exception ex)
            {
                this.ErrorOccured(new AsyncSocketErrorEventArgs(this.servernum, this.id, ex));
            }
        }
    }
}
