﻿using System;

namespace AsyncSocket
{
    public class AsyncSocketReceiveEventArgs : EventArgs
    {
        private readonly int servernum = 0;
        private readonly int id = 0;
        private readonly int receiveBytes;
        private readonly byte[] receiveData;

        public AsyncSocketReceiveEventArgs(
          int servernum,
          int id,
          int receiveBytes,
          byte[] receiveData)
        {
            this.servernum = servernum;
            this.id = id;
            this.receiveBytes = receiveBytes;
            this.receiveData = receiveData;
        }

        public int ReceiveBytes
        {
            get
            {
                return this.receiveBytes;
            }
        }

        public byte[] ReceiveData
        {
            get
            {
                return this.receiveData;
            }
        }

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public int SERVERNUM
        {
            get
            {
                return this.servernum;
            }
        }
    }
}
