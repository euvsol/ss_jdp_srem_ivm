﻿using System;

namespace AsyncSocket
{
    public class AsyncSocketConnectionEventArgs : EventArgs
    {
        private readonly int servernum = 0;
        private readonly int id = 0;

        public AsyncSocketConnectionEventArgs(int servernum, int id)
        {
            this.servernum = servernum;
            this.id = id;
        }

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public int SERVERNUM
        {
            get
            {
                return this.servernum;
            }
        }
    }
}
