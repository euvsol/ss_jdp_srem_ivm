﻿namespace AsyncSocket
{
    public class AsyncSocketClass
    {
        protected int id;
        protected int servernum;

        public event AsyncSocketErrorEventHandler   OnError;
        public event AsyncSocketConnectEventHandler OnConnet;
        public event AsyncSocketCloseEventHandler   OnClose;
        public event AsyncSocketSendEventHandler    OnSend;
        public event AsyncSocketReceiveEventHandler OnReceive;
        public event AsyncSocketAcceptEventHandler  OnAccept;

        public AsyncSocketClass()
        {
            this.id = -1;
            this.servernum = 0;
        }

        public AsyncSocketClass(int servernum, int id)
        {
            this.servernum = servernum;
            this.id = id;
        }

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public int SERVERNUM
        {
            get
            {
                return this.servernum;
            }
        }

        protected virtual void ErrorOccured(AsyncSocketErrorEventArgs e)
        {
            AsyncSocketErrorEventHandler onError = this.OnError;
            if (onError == null)
                return;
            onError((object) this, e);
        }

        protected virtual void Connected(AsyncSocketConnectionEventArgs e)
        {
            AsyncSocketConnectEventHandler onConnet = this.OnConnet;
            if (onConnet == null)
                return;
            onConnet((object) this, e);
        }

        protected virtual void Closed(AsyncSocketConnectionEventArgs e)
        {
            AsyncSocketCloseEventHandler onClose = this.OnClose;
            if (onClose == null)
                return;
            onClose((object) this, e);
        }

        protected virtual void Sent(AsyncSocketSendEventArgs e)
        {
            AsyncSocketSendEventHandler onSend = this.OnSend;
            if (onSend == null)
                return;
            onSend((object) this, e);
        }

        protected virtual void Received(AsyncSocketReceiveEventArgs e)
        {
            AsyncSocketReceiveEventHandler onReceive = this.OnReceive;
            if (onReceive == null)
                return;
            onReceive((object) this, e);
        }

        protected virtual void Accepted(AsyncSocketAcceptEventArgs e)
        {
            AsyncSocketAcceptEventHandler onAccept = this.OnAccept;
            if (onAccept == null)
                return;
            onAccept((object) this, e);
        }
    }
}
