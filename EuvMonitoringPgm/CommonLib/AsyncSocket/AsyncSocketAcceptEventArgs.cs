﻿using System;
using System.Net.Sockets;

namespace AsyncSocket
{
    public class AsyncSocketAcceptEventArgs : EventArgs
    {
        private readonly int servernum = 0;
        private readonly Socket conn;

        public AsyncSocketAcceptEventArgs(int servernum, Socket conn)
        {
            this.servernum = servernum;
            this.conn = conn;
        }

        public Socket Worker
        {
            get
            {
                return this.conn;
            }
        }

        public int SERVERNUM
        {
            get
            {
                return this.servernum;
            }
        }
    }
}
