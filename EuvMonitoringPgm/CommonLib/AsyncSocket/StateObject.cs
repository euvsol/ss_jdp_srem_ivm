﻿using System.Net.Sockets;

namespace AsyncSocket
{
    public class StateObject
    {
        private const int BUFFER_SIZE = 4096;
        private Socket worker;
        private byte[] buffer;

        public StateObject(Socket worker)
        {
            this.worker = worker;
            this.buffer = new byte[4096];
        }

        public Socket Worker
        {
            get
            {
                return this.worker;
            }
            set
            {
                this.worker = value;
            }
        }

        public byte[] Buffer
        {
            get
            {
                return this.buffer;
            }
            set
            {
                this.buffer = value;
            }
        }

        public int BufferSize
        {
            get
            {
                return 4096;
            }
        }
    }
}
