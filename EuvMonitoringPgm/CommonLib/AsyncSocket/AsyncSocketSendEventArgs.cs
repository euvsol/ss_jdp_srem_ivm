﻿using System;

namespace AsyncSocket
{
    public class AsyncSocketSendEventArgs : EventArgs
    {
        private readonly int servernum = 0;
        private readonly int id = 0;
        private readonly int sendBytes;

        public AsyncSocketSendEventArgs(int servernum, int id, int sendBytes)
        {
            this.servernum = servernum;
            this.id = id;
            this.sendBytes = sendBytes;
        }

        public int SendBytes
        {
            get
            {
                return this.sendBytes;
            }
        }

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public int SERVERNUM
        {
            get
            {
                return this.servernum;
            }
        }
    }
}
