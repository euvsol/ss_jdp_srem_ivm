﻿using System;

namespace AsyncSocket
{
    public class AsyncSocketErrorEventArgs : EventArgs
    {
        private readonly int servernum = 0;
        private readonly int id = 0;
        private readonly Exception exception;

        public AsyncSocketErrorEventArgs(int servernum, int id, Exception exception)
        {
            this.servernum = servernum;
            this.id = id;
            this.exception = exception;
        }

        public Exception AsyncSocketException
        {
            get
            {
                return this.exception;
            }
        }

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public int SERVERNUM
        {
            get
            {
                return this.servernum;
            }
        }
    }
}
