﻿using System.Collections.Generic;
using System.Threading;

namespace CommonLib
{
    public class CommAutoSerial : CommSerial
    {
        private int CYCLE_INTERVAL = 10;
        private int DISCONNECT_WAIT = 2000;
        private bool DISCONNECT_NOT_USED = false;
        private Thread m_tSerial = (Thread) null;
        private bool m_bSendState = false;
        private object m_objSend = new object();
        private List<string> m_lsStateMsg = new List<string>();
        private List<string> m_lsSendMsg = new List<string>();
        private List<string> m_lsFrontdMsg = new List<string>();

        public event CommAutoSerial.EventSendTimeOut OnSendTimeOutEvent;
        public event CommAutoSerial.EventSendNowMsg OnSendNowMsgEvent;

        public CommAutoSerial()
        {
            this.m_tSerial = new Thread(new ThreadStart(this.ThreadAutoMsg));
            this.m_tSerial.IsBackground = true;
            this.m_tSerial.Start();
        }

        private void ThreadAutoMsg()
        {
            while (true)
            {
                if (this.GetPortStatus())
                {
                    if (!this.GetStateSended())
                    {
                        if (this.m_lsFrontdMsg.Count > 0)
                        {
                            for (int index = 0; index < this.m_lsFrontdMsg.Count; ++index)
                                this.m_lsSendMsg.Insert(index, this.m_lsFrontdMsg[index]);
                            this.m_lsFrontdMsg.Clear();
                        }
                        if (this.m_lsSendMsg.Count < 1)
                        {
                            for (int index = 0; index < this.m_lsStateMsg.Count; ++index)
                                this.m_lsSendMsg.Add(this.m_lsStateMsg[index]);
                        }
                        this.AutoSendWork();
                        if (this.DISCONNECT_NOT_USED)
                            this.SetReceiveFinshed();
                    }
                    else if (!this.DISCONNECT_NOT_USED)
                        this.AutoTimeOutCheck();
                }
                Thread.Sleep(this.CYCLE_INTERVAL);
            }
        }

        private void AutoSendWork()
        {
            if (this.m_lsSendMsg.Count <= 0)
                return;
            this.SetSendMsg(this.m_lsSendMsg[0]);
        }

        private void AutoTimeOutCheck()
        {
            if (!this.GetTimeOverCheck(this.DISCONNECT_WAIT))
                return;
            if (this.OnSendTimeOutEvent != null)
                this.OnSendTimeOutEvent(this.m_lsSendMsg[0]);
            this.m_lsFrontdMsg.Clear();
            this.m_lsSendMsg.Clear();
        }

        private void SetSendMsg(string sMsg)
        {
            lock (this.m_objSend)
            {
                this.SetSendState(true);
                this.SetTimeStart();
                this.SendData(sMsg);
                if (this.OnSendNowMsgEvent == null)
                    return;
                this.OnSendNowMsgEvent(sMsg);
            }
        }

        private void SetSendState(bool bState)
        {
            this.m_bSendState = bState;
        }

        protected void SetAutoStateMsg(string sMsg)
        {
            this.m_lsStateMsg.Add(sMsg);
        }

        protected void SetAutoSendMsg(string sMsg)
        {
            this.SetAutoSendMsg(sMsg, false);
        }

        protected void SetAutoSendMsg(string sMsg, bool bFront)
        {
            if (this.m_lsSendMsg.IndexOf(sMsg, 0) >= 0)
                return;
            if (bFront)
            {
                if (this.m_lsFrontdMsg.IndexOf(sMsg, 0) < 0)
                    this.m_lsFrontdMsg.Add(sMsg);
            }
            else
                this.m_lsSendMsg.Add(sMsg);
        }

        protected void SetReceiveFinshed()
        {
            if (this.m_lsSendMsg.Count > 0)
                this.m_lsSendMsg.RemoveAt(0);
            this.SetSendState(false);
        }

        protected bool GetStateSended()
        {
            return this.m_bSendState;
        }

        protected void SetIntervalTime(int nTimeMs)
        {
            this.CYCLE_INTERVAL = nTimeMs;
        }

        protected void SetDisconnectWaitTime(int nTimeMs)
        {
            this.DISCONNECT_WAIT = nTimeMs;
        }

        protected void SetDisconnectNotUsed(bool bState)
        {
            this.DISCONNECT_NOT_USED = bState;
        }

        public delegate void EventSendTimeOut(string sTimeOutMsg);
        public delegate void EventSendNowMsg(string sNowMsg);
    }
}
