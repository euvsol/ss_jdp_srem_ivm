﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace CommonLib
{
    public class CommXmlSerializer
    {
        public bool WriteXML(string sPath, object obj)
        {
            StreamWriter streamWriter = (StreamWriter) null;
            try
            {
                using (streamWriter = new StreamWriter((Stream) new FileStream(sPath, FileMode.Create, FileAccess.Write), Encoding.UTF8))
                {
                    new XmlSerializer(obj.GetType()).Serialize((TextWriter) streamWriter, obj);
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                streamWriter?.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public bool ReadXML(string sPath, Type type, ref object obj)
        {
            FileStream fileStream = (FileStream) null;
            try
            {
                fileStream = new FileStream(sPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                XmlSerializer xmlSerializer = new XmlSerializer(type);
                obj = xmlSerializer.Deserialize((Stream) fileStream);
                fileStream.Close();
            }
            catch (Exception ex)
            {
                fileStream?.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
    }
}
