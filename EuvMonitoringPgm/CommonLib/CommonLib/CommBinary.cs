﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace CommonLib
{
    public class CommBinary
    {
        public bool Write(string sPath, object obj)
        {
            FileStream fileStream = (FileStream) null;
            try
            {
                fileStream = new FileStream(sPath, FileMode.Create, FileAccess.Write);
                BinaryWriter binaryWriter = new BinaryWriter((Stream) fileStream);
                byte[] byteArray = this.StructureToByteArray(obj);
                binaryWriter.Write(byteArray);
                binaryWriter.Close();
                fileStream.Close();
            }
            catch (Exception ex)
            {
                fileStream?.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public bool Read(string sPath, Type type, ref object obj)
        {
            FileStream fileStream = (FileStream) null;
            try
            {
                  fileStream = new FileStream(sPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                  BinaryReader binaryReader = new BinaryReader((Stream) fileStream);
                  byte[] bytearray = binaryReader.ReadBytes((int) binaryReader.BaseStream.Length);
                  object obj1 = new object();
                  obj = this.ByteArrayToStructure(bytearray, type);
                  binaryReader.Close();
                  fileStream.Close();
            }
            catch (Exception ex)
            {
                  fileStream?.Close();
                  Console.WriteLine(ex.Message);
                  return false;
            }
            return true;
        }

        private object ByteArrayToStructure(byte[] bytearray, Type type)
        {
            int num1 = Marshal.SizeOf(type);
            IntPtr num2 = Marshal.AllocHGlobal(num1);
            Marshal.Copy(bytearray, 0, num2, num1);
            object structure = Marshal.PtrToStructure(num2, type);
            Marshal.FreeHGlobal(num2);
            return structure;
        }

        private byte[] StructureToByteArray(object obj)
        {
            int length = Marshal.SizeOf(obj);
            byte[] destination = new byte[length];
            IntPtr num = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr(obj, num, true);
            Marshal.Copy(num, destination, 0, length);
            Marshal.FreeHGlobal(num);
            return destination;
        }
    }
}
