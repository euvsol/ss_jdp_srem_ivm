﻿using AsyncSocket;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace CommonLib
{
    public class CommSocketServer
    {
        #region ** Delegate Events **

        public delegate void EventReceive(int nServerNum, int nClientID, string sBuff, byte[] sTemp);
        public event CommSocketServer.EventReceive OnReciveEvent;

        public delegate void EventConnect(int nServerNum, int nClientID);
        public event CommSocketServer.EventConnect OnConnectEvent;

        public delegate void EventDisconnect(int nServerNum, int nClientID);
        public event CommSocketServer.EventDisconnect OnDisconnectEvent;

        public delegate void EventError(int nServerNum, int nClientID, string sErrMsg);
        public event CommSocketServer.EventError OnErrorEvent;

        #endregion 

        private object objSend = new object();
        private bool m_bRunState = false;
        private List<AsyncSocketClient> m_ClientList = new List<AsyncSocketClient>();
        private AsyncSocketServer m_SocketServer = (AsyncSocketServer) null;
        private int m_nServerNum = -1;
        private Encoding m_EncodingType = Encoding.Default;
        private const int MAX_CLIENT_COUNT = 10;

        public CommSocketServer(uint nServerNum)
        {
            this.m_nServerNum = Convert.ToInt32(nServerNum);
            this.m_SocketServer = new AsyncSocketServer(this.m_nServerNum);
            this.m_SocketServer.OnAccept += new AsyncSocketAcceptEventHandler(this.ClientAccept);
        }

        public CommSocketServer(int nServerNum, Encoding _EncodingType)
        {
            this.m_EncodingType = _EncodingType;
            this.m_nServerNum = Convert.ToInt32(nServerNum);
            this.m_SocketServer = new AsyncSocketServer(this.m_nServerNum);
            this.m_SocketServer.OnAccept += new AsyncSocketAcceptEventHandler(this.ClientAccept);
        }

        private void ClientAccept(object sender, AsyncSocketAcceptEventArgs e)
        {
            if (!(this.GetConnectedClientCount() <= MAX_CLIENT_COUNT))
            {
                e.Worker.Close();
            }
            else
            {
                int connectedIpAddrCheck = this.GetConnectedIPAddrCheck(e.SERVERNUM, e.Worker);
                AsyncSocketClient asyncSocketClient = new AsyncSocketClient(e.SERVERNUM, connectedIpAddrCheck, e.Worker);
                asyncSocketClient.Receive();
                asyncSocketClient.OnClose += new AsyncSocketCloseEventHandler(this.ClientDisconnect);
                asyncSocketClient.OnError += new AsyncSocketErrorEventHandler(this.ClientError);
                asyncSocketClient.OnReceive += new AsyncSocketReceiveEventHandler(this.ClientReceive);
                this.m_ClientList.Add(asyncSocketClient);

                if (this.OnConnectEvent == null)
                    return;

                this.OnConnectEvent(e.SERVERNUM, connectedIpAddrCheck);
            }
        }

        private void ClientReceive(object sender, AsyncSocketReceiveEventArgs e)
        {
            string sBuff = this.m_EncodingType.GetString(e.ReceiveData, 0, e.ReceiveBytes);

            if (this.OnReciveEvent == null)
                return;

            this.OnReciveEvent(e.SERVERNUM, e.ID, sBuff, e.ReceiveData);
        }

        private void ClientDisconnect(object sender, AsyncSocketConnectionEventArgs e)
        {
            for (int index = 0; index < this.m_ClientList.Count; ++index)
            {
                if (this.m_ClientList[index].ID == e.ID)
                {
                    if (this.OnDisconnectEvent != null)
                        this.OnDisconnectEvent(e.SERVERNUM, e.ID);

                    this.m_ClientList.RemoveAt(e.ID);
                    break;
                }
            }
        }

        private void ClientError(object sender, AsyncSocketErrorEventArgs e)
        {
            if (this.OnErrorEvent == null)
                return;

            this.OnErrorEvent(e.SERVERNUM, e.ID, e.AsyncSocketException.Message);
        }

        public int GetConnectedClientCount()
        {
            return this.m_ClientList.Count;
        }

        public bool GetClientConnectionState(int nIdx)
        {
            if(nIdx != -1)
            {
                if (m_ClientList.Count == 0)
                    return false;

                return SocketConnected(m_ClientList[nIdx].Connection);
            }
                

            return false;
        }

        private bool SocketConnected(Socket _Client)
        {
            return !((_Client.Poll(1000, SelectMode.SelectRead) && (_Client.Available == 0)) || !_Client.Connected);
        }

        public void SetClientDisconnect(int nClientID)
        {
            if (this.OnDisconnectEvent != null)
                this.OnDisconnectEvent(m_nServerNum, nClientID);

            this.m_ClientList.RemoveAt(nClientID);
        }

        private int GetConnectedIPAddrCheck(int nServerNum, Socket _Client)
        {
            int toClienNumParser = this.GetIPAddrToClienNumParser(((IPEndPoint) _Client.RemoteEndPoint).Address.ToString());

            if (toClienNumParser < 0)
                return this.m_ClientList.Count;

            this.m_ClientList[toClienNumParser].Close();

            return toClienNumParser;
        }

        private int GetIPAddrToClienNumParser(string sIPAddr)
        {
            int num = -1;

            for (int index = 0; index < this.m_ClientList.Count; ++index)
            {
                if (((IPEndPoint) this.m_ClientList[index].Connection.RemoteEndPoint).Address.ToString() == sIPAddr)
                {
                    num = index;
                    break;
                }
            }

            return num;
        }

        private string GetClientNumToIpAddrParser(int nClientNum)
        {
            return nClientNum < 0 || this.m_ClientList.Count <= nClientNum ? "0.0.0.0" : ((IPEndPoint) this.m_ClientList[nClientNum].Connection.RemoteEndPoint).Address.ToString();
        }

        public bool GetServerRunState()
        {
            return this.m_bRunState;
        }

        private string SERVER_IP
        {
            get
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());
                string empty = string.Empty;

                for (int index = 0; index < hostEntry.AddressList.Length; ++index)
                {
                    if (hostEntry.AddressList[index].AddressFamily == AddressFamily.InterNetwork)
                        empty = hostEntry.AddressList[index].ToString();
                }

                return empty;
            }
        }

        public bool SetRun(int nPort)
        {
            return this.SetRun(this.SERVER_IP, nPort);
        }

        public bool SetRun(string sServerIP, int nPort)
        {
            if (this.m_bRunState)
                return false;

            this.m_SocketServer.Listen(IPAddress.Parse(sServerIP), nPort);
            this.m_bRunState = true;

            return true;
        }

        public void SetStop()
        {
            if (!this.m_bRunState)
                return;

            for (int index = 0; index < this.m_ClientList.Count; ++index)
                this.m_ClientList[index].Close();

            this.m_SocketServer.Stop();
            this.m_bRunState = false;
            this.m_ClientList.Clear();
        }

        public string GetServerIPAddress()
        {
            return this.SERVER_IP;
        }

        public string GetClientConnectedIPAddress(int nClientNum)
        {
            return this.GetClientNumToIpAddrParser(nClientNum);
        }

        public List<string> GetClientConnectedIPAddress()
        {
            List<string> stringList = new List<string>();

            for (int nClientNum = 0; nClientNum < this.m_ClientList.Count; ++nClientNum)
                stringList.Add(this.GetClientNumToIpAddrParser(nClientNum));

            return stringList;
        }

        public bool SetSend(string sClientIPAddr, string strSendMsg)
        {
            return this.m_bRunState && this.SetSend(this.GetIPAddrToClienNumParser(sClientIPAddr), strSendMsg);
        }

        public bool SetSend(int nClientNum, string strSendMsg)
        {
            if (!this.m_bRunState)
                return false;

            lock (this.objSend)
            {
                if (nClientNum < 0 || this.m_ClientList.Count <= nClientNum)
                    return false;
                this.m_ClientList[nClientNum].Send(this.m_EncodingType.GetBytes(strSendMsg));
            }

            return true;
        }

        public bool SetSend(int nClientNum, byte[] btSendMsg)
        {
            if (!this.m_bRunState)
                return false;

            lock (this.objSend)
            {
                if (nClientNum < 0 || this.m_ClientList.Count <= nClientNum)
                    return false;
                this.m_ClientList[nClientNum].Send(btSendMsg);
            }

            return true;
        }
    } 
}
