﻿using System.IO.Ports;
using System.Text;

namespace CommonLib
{
    public class CommSerial : CommStopWatch
    {
        private SerialPort _Serial = new SerialPort();
        private Encoding m_EncodingType = Encoding.ASCII;

        public event CommSerial.EventReceive OnReciveEvent;

        public CommSerial()
        {
        }

        public CommSerial(Encoding _EncodingType)
        {
            this.m_EncodingType = _EncodingType;
        }

        ~CommSerial()
        {
            if (!this._Serial.IsOpen)
                return;
            this._Serial.Close();
        }

        public bool OpenPort(string sPortName, int nBaudRate)
        {
            return this.OpenPort(sPortName, nBaudRate, false);
        }

        public bool OpenPort(string sPortName, int nBaudRate, bool bRtsEnable)
        {
            this._Serial.PortName = sPortName;
            this._Serial.BaudRate = nBaudRate;
            this._Serial.Parity = Parity.None;
            this._Serial.DataBits = 8;
            this._Serial.StopBits = StopBits.One;
            this._Serial.RtsEnable = bRtsEnable;
            this._Serial.ReadTimeout = 500;
            this._Serial.WriteTimeout = 500;
            this._Serial.Encoding = Encoding.GetEncoding("ks_c_5601-1987");
            bool flag = false;
            foreach (string portName in SerialPort.GetPortNames())
            {
                if (this._Serial.PortName.Equals(portName))
                {
                    flag = true;
                    break;
                }
            }
            if (!flag)
                return false;
            if (this._Serial.IsOpen)
            {
                this._Serial.Close();
                return false;
            }
            try
            {
                this._Serial.Open();
                this._Serial.DataReceived += new SerialDataReceivedEventHandler(this.ReceiveData);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void ClosePort()
        {
            bool flag = false;
            foreach (string portName in SerialPort.GetPortNames())
            {
                if (this._Serial.PortName.Equals(portName))
                {
                    flag = true;
                    break;
                }
            }
            try
            {
                if (!this._Serial.IsOpen || !flag)
                    return;
                this._Serial.Close();
                this._Serial.DataReceived -= new SerialDataReceivedEventHandler(this.ReceiveData);
            }
            catch
            {
            }
        }

        public bool GetPortStatus()
        {
            return this._Serial.IsOpen;
        }

        public string[] GetSystemPorts()
        {
            return SerialPort.GetPortNames();
        }

        private void ReceiveData(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] bytes = this.m_EncodingType.GetBytes(this._Serial.ReadExisting());
            if (bytes.Length < 1 || this.OnReciveEvent == null)
                return;
            this.OnReciveEvent(bytes);
        }

        public bool SendData(string sMessage)
        {
            if (!this._Serial.IsOpen)
                return false;
            try
            {
              this._Serial.Write(sMessage);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void SetEncodingType(Encoding _EncodingType)
        {
            this.m_EncodingType = _EncodingType;
        }

        public Encoding GetEncodingType()
        {
            return this.m_EncodingType;
        }

        public delegate void EventReceive(byte[] Buff);
    }
}
