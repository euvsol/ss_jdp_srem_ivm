﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace CommonLib
{
    public class CommSocketClient
    {
        private Socket m_SocClient = (Socket) null;
        private bool m_bConnectState = false;
        private byte[] byteReceiveMsg = new byte[1024];
        private byte[] byteSendMsg = new byte[1024];
        private Encoding m_EncodingType = Encoding.Default;

        public event CommSocketClient.EventReceive OnReceiveEvent;

        public event CommSocketClient.EventDisconnect OnDisconnectEvent;

        public CommSocketClient()
        {
        }

        public CommSocketClient(Encoding _EncodingType)
        {
            this.m_EncodingType = _EncodingType;
        }

        public bool Connect(string strIp, int nPort)
        {
            IPAddress address = IPAddress.Parse(strIp);
            try
            {
                this.m_SocClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                this.m_SocClient.Connect((EndPoint) new IPEndPoint(address, nPort));
                this.m_bConnectState = true;
                this.m_SocClient.BeginReceive(this.byteReceiveMsg, 0, this.byteReceiveMsg.Length, SocketFlags.None, new AsyncCallback(this.CallBack_ReceiveMsg), (object) this.byteReceiveMsg);
            }
            catch
            {
                this.m_bConnectState = false;
            }
            return this.m_bConnectState;
        }

        public bool IsConnected()
        {
            return this.m_bConnectState;
        }

        public bool Send(string strSend)
        {
            if (!this.m_bConnectState || string.IsNullOrEmpty(strSend))
                return false;
            this.byteSendMsg = this.m_EncodingType.GetBytes(strSend);
            try
            {
                this.m_SocClient.BeginSend(this.byteSendMsg, 0, this.byteSendMsg.Length, SocketFlags.None, new AsyncCallback(this.CallBack_SendMsg), (object) this.m_SocClient);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool SendByte(byte[] _byteData)
        {
            this.byteSendMsg = _byteData;
            try
            {
                this.m_SocClient.BeginSend(this.byteSendMsg, 0, this.byteSendMsg.Length, SocketFlags.None, new AsyncCallback(this.CallBack_SendMsg), (object) this.m_SocClient);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private void CallBack_SendMsg(IAsyncResult ar)
        {
            this.m_SocClient = (Socket) ar.AsyncState;
            try
            {
                if (this.m_SocClient.EndSend(ar) != 0)
                    return;
                this.Disconnect("SendMsg DataSize = 0");
            }
            catch
            {
                this.Disconnect("SocClient.EndSend Catch");
            }
        }

        private void CallBack_ReceiveMsg(IAsyncResult ar)
        {
            Exception exception;
            try
            {
                byte[] asyncState = (byte[]) ar.AsyncState;
                int count = this.m_SocClient.EndReceive(ar);
                if (count == 0)
                {
                    this.Disconnect("Host Disconnect");
                    if (this.OnDisconnectEvent == null)
                        return;
                    this.OnDisconnectEvent();
                }
                else
                {
                    try
                    {
                        string sBuffer = this.m_EncodingType.GetString(asyncState, 0, count);
                        if (this.OnReceiveEvent != null)
                            this.OnReceiveEvent(sBuffer);
                        this.m_SocClient.BeginReceive(this.byteReceiveMsg, 0, this.byteReceiveMsg.Length, SocketFlags.None, new AsyncCallback(this.CallBack_ReceiveMsg), (object) this.byteReceiveMsg);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                        this.Disconnect("ReceiveData Exception");
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                this.Disconnect("Host Disconnect");
            }
        }

        public void Disconnect()
        {
            this.Disconnect(nameof (Disconnect));
        }

        private void Disconnect(string strDisconnectMsg)
        {
            if (!this.m_bConnectState)
                return;
            this.m_bConnectState = false;
            try
            {
                this.m_SocClient.Disconnect(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public delegate void EventReceive(string sBuffer);

        public delegate void EventDisconnect();
    }
}
