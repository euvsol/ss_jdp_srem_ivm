﻿using System;
using System.IO;
using System.Text;

namespace CommonLib
{
    public class CommLog
    {
        private string m_sLogType = string.Empty;
        private string m_sLogFirstLine = string.Empty;
        private object objSave = new object();

        public CommLog(string sLogType)
        {
            this.m_sLogType = sLogType;
        }

        ~CommLog()
        {
        }

        public void AddFirstLine(string sFirstLine)
        {
            this.m_sLogFirstLine = sFirstLine;
        }

        public void AddLog(string strLogData)
        {
            this.AddLog(string.Empty, strLogData);
        }

        public void AddLog(Enum emSubSelect, string strLogData)
        {
            this.AddLog(emSubSelect.ToString(), strLogData);
        }

        public void AddLog(string sSubSelect, string strLogData)
        {
            lock (this.objSave)
                this.SetFileSave(sSubSelect, strLogData);
        }

        private void SetFileSave(string sSubSelect, string strLogData)
        {
            this.SetFileSave(sSubSelect, strLogData, "Log");
        }

        private void SetFileSave(string sSubSelect, string strLogData, string sExt)
        {
            string str1 = string.Empty;
            string[] strArray = new string[6]
            {
                Directory.GetCurrentDirectory(),
                "Log",
                this.m_sLogType,
                sSubSelect.ToString(),
                string.Format("{0:D4}", (object) DateTime.Now.Year),
                string.Format("{0:D2}", (object) DateTime.Now.Month)
            };
            foreach (string path2 in strArray)
            {
                str1 = Path.Combine(str1, path2);
                if (!Directory.Exists(str1))
                    Directory.CreateDirectory(str1);
            }
            string path1 = Path.Combine(str1, string.Format("{0:D2}.{1}", (object) DateTime.Now.Day, (object) sExt));
            string path3 = Path.Combine(str1, string.Format("{0:D2}_Tmp.{1}", (object) DateTime.Now.Day, (object) sExt));
            bool flag;
            FileStream fileStream;
            try
            {
                flag = !File.Exists(path1);
                fileStream = File.Open(path1, FileMode.Append, FileAccess.Write, FileShare.Read);
            }
            catch
            {
                flag = !File.Exists(path3);
                fileStream = File.Open(path3, FileMode.Append, FileAccess.Write, FileShare.Read);
            }
            object[] objArray1 = new object[7];
            object[] objArray2 = objArray1;
            DateTime now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int year = now.Year;
            objArray2[0] = (object) year;
            object[] objArray3 = objArray1;
            now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int month = now.Month;
            objArray3[1] = (object) month;
            object[] objArray4 = objArray1;
            now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int day = now.Day;
            objArray4[2] = (object) day;
            object[] objArray5 = objArray1;
            now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int hour = now.Hour;
            objArray5[3] = (object) hour;
            object[] objArray6 = objArray1;
            now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int minute = now.Minute;
            objArray6[4] = (object) minute;
            object[] objArray7 = objArray1;
            now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int second = now.Second;
            objArray7[5] = (object) second;
            object[] objArray8 = objArray1;
            now = DateTime.Now;
            // ISSUE: variable of a boxed type
            int millisecond = now.Millisecond;
            objArray8[6] = (object) millisecond;
            string str2 = string.Format("{0:D4}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}", objArray1);
            if (flag && !string.IsNullOrEmpty(this.m_sLogFirstLine))
            {
                byte[] bytes = Encoding.GetEncoding("ks_c_5601-1987").GetBytes(string.Format("{0},[{1}],{2}\r\n", (object) str2, (object) sSubSelect, (object) this.m_sLogFirstLine));
                fileStream.Write(bytes, 0, bytes.Length);
            }
            byte[] bytes1 = Encoding.GetEncoding("ks_c_5601-1987").GetBytes(string.Format("{0},[{1}],{2}\r\n", (object) str2, (object) sSubSelect, (object) strLogData));
            fileStream.Write(bytes1, 0, bytes1.Length);
            fileStream.Close();
        }
    }
}
