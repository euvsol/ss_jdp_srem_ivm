﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonLib
{
    public class CommIniFile
    {
        private string m_sFullPath = string.Empty;
        private const string DEFAULT_FILENAME = "Sample.ini";
        private const int DATASIZE = 260;

        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(
          string lpAppName,
          string lpKeyName,
          string lpString,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern uint GetPrivateProfileInt(
          string lpAppName,
          string lpKeyName,
          int nDefault,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(
          string lpAppName,
          string lpKeyName,
          string lpDefault,
          StringBuilder lpReturnedString,
          int nSize,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSectionNames(
          byte[] lpReturnedString,
          int nSize,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(
          string section,
          byte[] lpReturnedString,
          int nSize,
          string lpFileName);

        public bool SetPath(string sDirPath, string sFileName)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            if (string.IsNullOrEmpty(sFileName))
                sFileName = "Sample.ini";
            this.m_sFullPath = Path.Combine(currentDirectory, sDirPath, sFileName);
            FileInfo fileInfo = new FileInfo(this.m_sFullPath);
            if (fileInfo.Exists)
                return true;
            DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(currentDirectory, sDirPath));
            if (!directoryInfo.Exists)
                directoryInfo.Create();
            fileInfo.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            return false;
        }

        public bool SetValue(string sSection, string sKey, string sValue)
        {
            return CommIniFile.WritePrivateProfileString(sSection, sKey, sValue, this.m_sFullPath);
        }

        public bool SetValue(string sSection, string sKey, int nValue)
        {
            return CommIniFile.WritePrivateProfileString(sSection, sKey, nValue.ToString(), this.m_sFullPath);
        }

        public bool SetValue(string sSection, string sKey, double fValue)
        {
            return CommIniFile.WritePrivateProfileString(sSection, sKey, fValue.ToString(), this.m_sFullPath);
        }

        public string GetBigValue(string sSection, string sKey)
        {
            StringBuilder lpReturnedString = new StringBuilder(4096);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            return lpReturnedString.ToString();
        }

        public string GetValue(string sSection, string sKey)
        {
            StringBuilder lpReturnedString = new StringBuilder(512);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            return lpReturnedString.ToString();
        }

        public int GetValueI(string sSection, string sKey)
        {
            int num = 0;
            StringBuilder lpReturnedString = new StringBuilder(512);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            try
            {
                num = Convert.ToInt32(lpReturnedString.ToString());
            }
            catch (FormatException ex)
            {
            }
            return num;
        }

        public double GetValueF(string sSection, string sKey)
        {
            double num = 0.0;
            StringBuilder lpReturnedString = new StringBuilder(512);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            try
            {
                num = Convert.ToDouble(lpReturnedString.ToString());
            }
            catch (FormatException ex)
            {
            }
            return num;
        }

        public void DeleteSection(string sSection)
        {
            CommIniFile.WritePrivateProfileString(sSection, (string) null, (string) null, this.m_sFullPath);
        }

        public void DeleteKey(string sSection, string sKey)
        {
            CommIniFile.WritePrivateProfileString(sSection, sKey, (string) null, this.m_sFullPath);
        }

        public int GetSectionCount()
        {
            byte[] lpReturnedString = new byte[260];
            List<byte[]> numArrayList = new List<byte[]>();
            int index1 = 0;
            int index2 = 0;
            int profileSectionNames = CommIniFile.GetPrivateProfileSectionNames(lpReturnedString, 260, this.m_sFullPath);
            for (int index3 = 0; index3 < profileSectionNames; ++index3)
            {
                numArrayList.Add(new byte[index3 + 260]);
                if (lpReturnedString[index3] == (byte) 0)
                {
                    numArrayList[index1][index2] = (byte) 0;
                    index2 = 0;
                    ++index1;
                }
                else
                    numArrayList[index1][index2++] = lpReturnedString[index3];
            }
            return index1;
        }

        public int GetKeyCount(string sSection)
        {
            byte[] lpReturnedString = new byte[260];
            List<byte[]> numArrayList = new List<byte[]>();
            int index1 = 0;
            int index2 = 0;
            int privateProfileSection = CommIniFile.GetPrivateProfileSection(sSection, lpReturnedString, 260, this.m_sFullPath);
            for (int index3 = 0; index3 < privateProfileSection; ++index3)
            {
                numArrayList.Add(new byte[index3 + 260]);
                if (lpReturnedString[index3] == (byte) 0)
                {
                    numArrayList[index1][index2] = (byte) 0;
                    index2 = 0;
                    ++index1;
                }
                else
                    numArrayList[index1][index2++] = lpReturnedString[index3];
            }
            return index1;
        }
    }
}
