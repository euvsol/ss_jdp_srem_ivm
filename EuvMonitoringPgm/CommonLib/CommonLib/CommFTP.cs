﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace CommonLib
{
    public class CommFTP
    {
        private string m_sOriginUrl = string.Empty;
        private string m_sFinalUrl = string.Empty;
        private string m_strID = string.Empty;
        private string m_strPW = string.Empty;
        private bool m_bPassive = true;

        public CommFTP()
        {
        }

        public CommFTP(string sServerIP, string sID, string sPW, uint nPort)
        {
            this.SetInfo(sServerIP, sID, sPW, nPort, true);
        }

        public void SetInfo(string sServerIP, string sID, string sPW, uint nPort)
        {
            this.SetInfo(sServerIP, sID, sPW, nPort, true);
        }

        public void SetInfo(string sServerIP, string sID, string sPW, uint nPort, bool bPassive)
        {
            this.m_sOriginUrl = "ftp://" + sServerIP + ":" + nPort.ToString() + "/";
            this.m_sFinalUrl = this.m_sOriginUrl;
            this.m_strID = sID;
            this.m_strPW = sPW;
            this.m_bPassive = bPassive;
        }

        private FtpWebRequest GetRequest(string URI)
        {
            try
            {
                FtpWebRequest ftpWebRequest = (FtpWebRequest) WebRequest.Create(URI);
                ftpWebRequest.Credentials = (ICredentials) new NetworkCredential(this.m_strID, this.m_strPW);
                ftpWebRequest.UseBinary = true;
                ftpWebRequest.KeepAlive = false;
                ftpWebRequest.UsePassive = this.m_bPassive;
                return ftpWebRequest;
            }
            finally
            {
            }
        }

        private bool SetFtpAction(string sName, string sAction)
        {
            return this.SetFtpAction(sName, string.Empty, sAction);
        }

        private bool SetFtpAction(string sName, string sReName, string sAction)
        {
            string str = this.m_sFinalUrl + "/" + sName;
            FtpWebRequest request = this.GetRequest(str);
            request.Method = sAction;
            switch (sAction)
            {
                case "RMD":
                    this.DirectoryChildDelete(str);
                    break;
                case "RENAME":
                    request.RenameTo = sReName;
                    break;
            }
            try
            {
                request.GetResponse().Close();
            }
            catch
            {
                return false;
            }
            return true;
        }

        private bool DirectoryChildDelete(string sUrlDirectory)
        {
            List<string> lsFileDirectory = new List<string>();
            string empty = string.Empty;
            this.GetReadFileDirectory(sUrlDirectory + "/", ref lsFileDirectory);
            if (lsFileDirectory.Count < 1)
                return false;
            for (int index = 0; index < lsFileDirectory.Count; ++index)
            {
                string str = sUrlDirectory + "/" + lsFileDirectory[index];
                this.DirectoryChildDelete(str);
                FtpWebRequest request = this.GetRequest(str);
                if (!string.IsNullOrEmpty(Path.GetExtension(str)))
                    request.Method = "DELE";
                else
                    request.Method = "RMD";
                try
                {
                    request.GetResponse().Close();
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        public void SetDirectoryPath(string sPath)
        {
            if (string.IsNullOrEmpty(sPath) || !string.IsNullOrEmpty(Path.GetExtension(sPath)))
                return;
            if (sPath.StartsWith(".."))
            {
                if (this.m_sFinalUrl == this.m_sOriginUrl)
                    return;
                string str = this.m_sFinalUrl;
                if (str[str.Length - 1] == '/')
                    str = str.Remove(str.Length - 1, 1);
                for (int index = 0; index < str.Length && str[str.Length - 1] != '/'; ++index)
                    str = str.Remove(str.Length - 1, 1);
                this.m_sFinalUrl = str;
            }
            else
                this.m_sFinalUrl = this.m_sFinalUrl + sPath + "/";
        }

        public string GetNowUrl()
        {
            return this.m_sFinalUrl;
        }

        public bool GetReadFileDirectory(ref List<string> lsFileDirectory)
        {
            return this.GetReadFileDirectory(this.m_sFinalUrl, ref lsFileDirectory);
        }

        public bool GetReadFileDirectory(string sUrl, ref List<string> lsFileDirectory)
        {
            string empty = string.Empty;
            try
            {
                FtpWebRequest request = this.GetRequest(sUrl);
                request.Method = "NLST";
                FtpWebResponse response = (FtpWebResponse) request.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream(), Encoding.Default);
                string[] strArray = streamReader.ReadToEnd().Split(new string[1]
                {
                    "\r\n"
                }, StringSplitOptions.RemoveEmptyEntries);
                lsFileDirectory.AddRange((IEnumerable<string>) strArray);
                streamReader.Close();
                response.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SetMakeDirectory(string sDirName)
        {
            return this.SetFtpAction(sDirName, "MKD");
        }

        public bool SetDeleteFile(string sFileName)
        {
            return this.SetFtpAction(sFileName, "DELE");
        }

        public bool SetDeleteDirectory(string sDirName)
        {
            return this.SetFtpAction(sDirName, "RMD");
        }

        public bool SetRenameFile(string sFileName, string newFileName)
        {
            return this.SetFtpAction(sFileName, newFileName, "RENAME");
        }

        public bool SetUploadFile(string sFilePath)
        {
            return this.SetUploadFile(this.m_sFinalUrl, sFilePath);
        }

        public bool SetUploadFile(string sUrl, string sFilePath)
        {
            if (!System.IO.File.Exists(sFilePath))
                return false;
            FileInfo fileInfo = new FileInfo(sFilePath);
            FtpWebRequest request = this.GetRequest(sUrl + "/" + fileInfo.Name);
            request.Method = "STOR";
            request.ContentLength = fileInfo.Length;
            try
            {
                int count1 = 2048;
                byte[] buffer = new byte[count1];
                FileStream fileStream = fileInfo.OpenRead();
                Stream requestStream = request.GetRequestStream();
                for (int count2 = fileStream.Read(buffer, 0, count1); count2 != 0; count2 = fileStream.Read(buffer, 0, count1))
                    requestStream.Write(buffer, 0, count2);
                requestStream.Close();
                fileStream.Close();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool SetDownloadFile(string sFileName, string sDownloadPath)
        {
            return this.SetDownloadFile(this.m_sFinalUrl, sFileName, sDownloadPath);
        }

        public bool SetDownloadFile(string sUrl, string sFileName, string sDownloadPath)
        {
            if (string.IsNullOrEmpty(Path.GetExtension(sFileName)))
                return false;
            FtpWebRequest request = this.GetRequest(sUrl + "/" + sFileName);
            request.Method = "RETR";
            FtpWebResponse response = (FtpWebResponse) request.GetResponse();
            try
            {
                Stream responseStream = response.GetResponseStream();
                FileStream fileStream = new FileStream(Path.Combine(sDownloadPath, sFileName), FileMode.Create);
                int count1 = 1024;
                byte[] buffer = new byte[count1];
                for (int count2 = responseStream.Read(buffer, 0, count1); count2 > 0; count2 = responseStream.Read(buffer, 0, count1))
                    fileStream.Write(buffer, 0, count2);
                responseStream.Close();
                fileStream.Close();
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
