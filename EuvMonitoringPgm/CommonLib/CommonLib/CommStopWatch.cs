﻿using System.Diagnostics;

namespace CommonLib
{
    public class CommStopWatch
    {
        private Stopwatch sw = new Stopwatch();

        ~CommStopWatch()
        {
            this.sw.Stop();
        }

        public void SetTimeStart()
        {
            this.sw.Restart();
        }

        public int GetTimeCheck()
        {
            return (int) this.sw.ElapsedMilliseconds;
        }

        public bool GetTimeOverCheck(int nMiliseconds)
        {
            return this.sw.ElapsedMilliseconds >= (long) nMiliseconds;
        }
    }
}
