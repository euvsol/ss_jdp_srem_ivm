﻿using System.Data;
using System.Data.OleDb;
using System.IO;

namespace CommonLib
{
    public class CommOleDB
    {
        private bool m_bConnect = false;
        private string m_sConnStr = string.Empty;
        private OleDbConnection m_OleConn = new OleDbConnection();

        public void SetDbConnect(string sDBPath)
        {
            if (Path.GetExtension(sDBPath).ToLower().EndsWith("mdb"))
            {
                this.m_sConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sDBPath + ";User Id=admin;Password=;";
            }
            else
            {
                if (!Path.GetExtension(sDBPath).ToLower().EndsWith("accdb"))
                    return;
                this.m_sConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sDBPath + ";";
            }
            this.m_OleConn = new OleDbConnection(this.m_sConnStr);
            try
            {
                this.m_OleConn.Open();
                this.m_bConnect = true;
            }
            catch
            {
                this.m_bConnect = false;
            }
        }

        public bool GetDbConnected()
        {
            return this.m_bConnect;
        }

        public DataSet GetDbDataset(string sSql)
        {
            DataSet dataSet = new DataSet();
            if (this.m_bConnect)
                new OleDbDataAdapter(sSql, this.m_OleConn).Fill(dataSet);
            return dataSet;
        }

        public void SetDbDataChange(string sSql)
        {
            if (!this.m_bConnect)
                return;
            using (OleDbCommand oleDbCommand = new OleDbCommand(sSql, this.m_OleConn))
                oleDbCommand.ExecuteNonQuery();
        }
    }
}
