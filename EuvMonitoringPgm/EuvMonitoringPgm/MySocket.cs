﻿using CommonLib;
using EuvMonitoringPgm.Data;
using System;
using System.Text;

namespace EuvMonitoringPgm
{
    public class MySocket : CommSocketServer
    {
        #region ** Delegate Events **

        public delegate void DelegateGuiDataUpdate();
        public event DelegateGuiDataUpdate MyEventGuiDataUpdate;

        public delegate void DelegateClientConnect();
        public event DelegateClientConnect MyEventClientConnected;

        public delegate void DelegateClientDisconnect();
        public event DelegateClientDisconnect MyEventClientDisconnected;

        public delegate void DelegateClientMsgReceive(string sRecvMsg);
        public event DelegateClientMsgReceive MyEventClientMsgReceived;

        public delegate void DelegateClientError(string sErrMsg);
        public event DelegateClientError MyEventClientError;

        #endregion

        int m_nClientID = -1;

        const int HEADER_INFO = 2;
        
        public MySocket() : base(0, Encoding.ASCII)
        {
            this.OnConnectEvent    += new EventConnect(OnEventConnect);
            this.OnDisconnectEvent += new EventDisconnect(OnEventDisconnect);
            this.OnReciveEvent     += new EventReceive(OnEventReceive);
            this.OnErrorEvent      += new EventError(OnEventError);
        }

        private void OnEventConnect(int nServerNum, int nClientID)
        {
            string sMsg = string.Format(">> EventConnect - ServerNum({0}), ClientID({1})", nServerNum, nClientID);
            Console.WriteLine(sMsg);

            if (MyEventClientConnected != null) MyEventClientConnected();

            m_nClientID = nClientID;
        }

        private void OnEventDisconnect(int nServerNum, int nClientID)
        {
            string sMsg = string.Format(">> EventDisconnect - ServerNum({0}), ClientID({1})", nServerNum, nClientID);
            Console.WriteLine(sMsg);

            if (MyEventClientDisconnected != null) MyEventClientDisconnected();

            m_nClientID = -1;
        }

        private void OnEventReceive(int nServerNum, int nClientID, string sBuff, byte[] sTemp)
        {
#if false   
            byte[] arrBytes = Encoding.Default.GetBytes(sBuff);

            #region ** Get Header Information (MsgId, MsgSize) **

            var HeaderInfo = new int[HEADER_INFO];
            for(var index = 0; index < HEADER_INFO; index++)
                HeaderInfo[index] = BitConverter.ToInt32(arrBytes, index * sizeof(int));

            #endregion
#else
            #region ** Get Header Information (MsgId, MsgSize) **

            var HeaderInfo = new int[HEADER_INFO];
            for (var index = 0; index < HEADER_INFO; index++)
                HeaderInfo[index] = BitConverter.ToInt32(sTemp, index * sizeof(int));

            #endregion

            byte[] arrBytes = new byte[HeaderInfo[(int)Header.MsgSize]];
            Array.Clear(arrBytes, 0, HeaderInfo[(int)Header.MsgSize]);
            Array.Copy(sTemp, 0, arrBytes, 0, HeaderInfo[(int)Header.MsgSize]);
#endif

            switch (HeaderInfo[(int)Header.MsgId])
            {
                case (int)MsgId.Monitoring:
                    MonitoringDataPacket packet = new MonitoringDataPacket();
                    packet.Deserialize(ref arrBytes);

                    #region ** System Data **

                    UiCommon.Knd.Data.SYSTEM.IsConnAdam = packet.ConnectAdam;
                    UiCommon.Knd.Data.SYSTEM.IsConnAfm = packet.ConnectAfm;
                    UiCommon.Knd.Data.SYSTEM.IsConnCrevis = packet.ConnectCrevis;
                    UiCommon.Knd.Data.SYSTEM.IsConnFilterStage = packet.ConnectFilterStage;
                    UiCommon.Knd.Data.SYSTEM.IsConnLlcTmp = packet.ConnectLlcTmp;
                    UiCommon.Knd.Data.SYSTEM.IsConnMcTmp = packet.ConnectMcTmp;
                    UiCommon.Knd.Data.SYSTEM.IsConnMts = packet.ConnectMts;
                    UiCommon.Knd.Data.SYSTEM.IsConnNaviStage = packet.ConnectNaviStage;
                    UiCommon.Knd.Data.SYSTEM.IsConnOmCam = packet.ConnectCam1;
                    UiCommon.Knd.Data.SYSTEM.IsConnRevolver = packet.ConnectRevolver;
                    UiCommon.Knd.Data.SYSTEM.IsConnScanStage = packet.ConnectScanStage;
                    UiCommon.Knd.Data.SYSTEM.IsConnSource = packet.ConnectSource;
                    UiCommon.Knd.Data.SYSTEM.IsConnVacuumGauge = packet.ConnectVacuumGauge;
                    UiCommon.Knd.Data.SYSTEM.IsConnVmtr = packet.ConnectVmtr;
                    UiCommon.Knd.Data.SYSTEM.IsConnXrayCamera = packet.ConnectXrayCamera;

                    #endregion

                    #region ** Monitoring Data **

                    UiCommon.Knd.Data.EQUIP.IsSeqWork = packet.SequenceWork;
                    
                    UiCommon.Knd.Data.EQUIP.IsPodOnLpm = packet.LpmPodExist;
                    UiCommon.Knd.Data.EQUIP.IsMaskInPod = packet.LpmMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsLpmInit = packet.LpmInitComp;
                    UiCommon.Knd.Data.EQUIP.IsLpmOpened = packet.LpmPodOpened;
                    UiCommon.Knd.Data.EQUIP.IsLpmWorking = packet.LpmWorking;
                    UiCommon.Knd.Data.EQUIP.IsLpmError = packet.LpmError;

                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = packet.AtrMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotInit = packet.AtrInitComp;
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = packet.AtrWorking;
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotError = packet.AtrError;

                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipper = packet.FlipperMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsFlipperInit = packet.FlipperInitComp;
                    UiCommon.Knd.Data.EQUIP.IsFlipperWorking = packet.FlipperWorking;
                    UiCommon.Knd.Data.EQUIP.IsFlipperError = packet.FlipperError;

                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotator = packet.RotatorMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsRotatorInit = packet.RotatorInitComp;
                    UiCommon.Knd.Data.EQUIP.IsRotatorWorking = packet.RotatorWorking;
                    UiCommon.Knd.Data.EQUIP.IsRotatorError = packet.RotatorError;

                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLC = packet.LlcMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsLlcGateOpened = packet.LlcGateValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsLlcTmpGateOpened = packet.LlcTmpGateValveOpened;
                    UiCommon.Knd.Data.EQUIP.LlcTmpState = packet.LlcTmpState;
                    UiCommon.Knd.Data.EQUIP.IsLlcTmpError = packet.LlcTmpError;

                    UiCommon.Knd.Data.EQUIP.IsLlcForelineOpened = packet.LlcForelineValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsLlcSlowRoughOpened = packet.LlcSlowRoughingValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsLlcFastRoughOpened = packet.LlcFastRoughingValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsLlcDryPumpWork = packet.LlcDryPumpWorking;
                    UiCommon.Knd.Data.EQUIP.IsLlcDryPumpAlarm = packet.LlcDryPumpAlarm;
                    UiCommon.Knd.Data.EQUIP.IsLlcDryPumpWarning = packet.LlcDryPumpWarning;
                    UiCommon.Knd.Data.EQUIP.LlcVacuum = packet.LlcVacuumRate;

                    UiCommon.Knd.Data.EQUIP.IsMaskOnChuck = packet.McChuckMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsTrGateOpened = packet.McTrGateValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsMcTmpGateOpened = packet.McTmpGateValveOpened;
                    UiCommon.Knd.Data.EQUIP.McTmpState = packet.McTmpState;
                    UiCommon.Knd.Data.EQUIP.IsMcTmpError = packet.McTmpError;
                    UiCommon.Knd.Data.EQUIP.IsMcForelineOpened = packet.McForelineValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsMcSlowRoughOpened = packet.McSlowRoughingValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsMcFastRoughOpened = packet.McFastRoughingValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsMcDryPumpWork = packet.McDryPumpWorking;
                    UiCommon.Knd.Data.EQUIP.IsMcDryPumpAlarm = packet.McDryPumpAlarm;
                    UiCommon.Knd.Data.EQUIP.IsMcDryPumpWarning = packet.McDryPumpWarning;
                    UiCommon.Knd.Data.EQUIP.McVacuum = packet.McVacuumRate;

                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtr = packet.VmtrMaskExist;
                    UiCommon.Knd.Data.EQUIP.IsVmtrServoOn = packet.VmtrServoOn;
                    UiCommon.Knd.Data.EQUIP.IsVmtrInit = packet.VmtrInitComp;
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = packet.VmtrWorking;
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = packet.VmtrExtended;
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = packet.VmtrRetracted;
                    UiCommon.Knd.Data.EQUIP.IsVmtrError = packet.VmtrError;
                    UiCommon.Knd.Data.EQUIP.IsVmtrEmo = packet.VmtrEMO;
                    UiCommon.Knd.Data.EQUIP.VmtrPosT = packet.VmtrPositionT;
                    UiCommon.Knd.Data.EQUIP.VmtrPosL = packet.VmtrPositionL;
                    UiCommon.Knd.Data.EQUIP.VmtrPosZ = packet.VmtrPositionZ;

                    UiCommon.Knd.Data.EQUIP.IsNaviLoadingPos = packet.NaviLoadingPos;
                    UiCommon.Knd.Data.EQUIP.NaviRealPosX = packet.NaviFeedbackPosX;
                    UiCommon.Knd.Data.EQUIP.NaviRealPosY = packet.NaviFeedbackPosY;

                    UiCommon.Knd.Data.EQUIP.IsSlowVentValveOpened = packet.SlowVentValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsSlowInletValveOpened = packet.SlowInletValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsSlowOutletValveOpened = packet.SlowOutletValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsFastVentValveOpened = packet.FastVentValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsFastInletValveOpened = packet.FastInletValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsFastOutletValveOpened = packet.FastOutletValveOpened;
                    UiCommon.Knd.Data.EQUIP.Mfc1N2Voltage = packet.Mfc1N2.ToString("F2");
                    UiCommon.Knd.Data.EQUIP.Mfc2N2Voltage = packet.Mfc2N2.ToString("F2");

                    UiCommon.Knd.Data.EQUIP.SrcMcVacuum = packet.SrcMcVacuumRate;
                    UiCommon.Knd.Data.EQUIP.SrcScVacuum = packet.SrcScVacuumRate;
                    UiCommon.Knd.Data.EQUIP.SrcBcVacuum = packet.SrcBcVacuumRate;
                    UiCommon.Knd.Data.EQUIP.IsSrcEuvLaserSol = packet.EuvLaserShutterSol;
                    UiCommon.Knd.Data.EQUIP.IsSrcEuvOn = packet.EuvOn;
                    UiCommon.Knd.Data.EQUIP.IsSrcMechShutterOpened = packet.SrcMechShutterOpened;
                    UiCommon.Knd.Data.EQUIP.SrcMirrorStagePos = packet.SrcMirrorStagePos;

                    UiCommon.Knd.Data.EQUIP.IsWaterSupplyValveOpened = packet.WaterSupplyValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsWaterReturnValveOpened = packet.WaterReturnValveOpened;
                    UiCommon.Knd.Data.EQUIP.IsMainAir = packet.MainAir;
                    UiCommon.Knd.Data.EQUIP.IsMainWater = packet.MainWater;
                    UiCommon.Knd.Data.EQUIP.IsWaterLeakLlcTmp = packet.WaterLeakLlcTmp;
                    UiCommon.Knd.Data.EQUIP.IsWaterLeakMcTmp = packet.WaterLeakMcTmp;
                    UiCommon.Knd.Data.EQUIP.IsSmokeDetectCb = packet.SmokeDetectCb;
                    UiCommon.Knd.Data.EQUIP.IsSmokeDetectVacSt = packet.SmokeDetectVacSt;
                    UiCommon.Knd.Data.EQUIP.IsAlarmWaterReturnTemp = packet.AlarmWaterReturnTemp;
                    UiCommon.Knd.Data.EQUIP.IsAvailableSrcGateOpened = packet.AvailableSrcGateOpened;

                    UiCommon.Knd.Data.EQUIP.IsMcMaskTiltError1 = packet.McMaskTiltError1;
                    UiCommon.Knd.Data.EQUIP.IsMcMaskTiltError2 = packet.McMaskTiltError2;
                    UiCommon.Knd.Data.EQUIP.IsLlcMaskTiltError1 = packet.LlcMaskTiltError1;
                    UiCommon.Knd.Data.EQUIP.IsLlcMaskTiltError2 = packet.LlcMaskTiltError2;

                    UiCommon.Knd.Data.EQUIP.MaskLocation = packet.MaskLocation;

#endregion

                    if (MyEventGuiDataUpdate != null)
                        MyEventGuiDataUpdate();
                    break;
                case (int)MsgId.Message:
                    MessageDataPacket _msg = new MessageDataPacket();
                    _msg.Deserialize(ref arrBytes);
                    if (MyEventClientMsgReceived != null)
                        MyEventClientMsgReceived(_msg.Message);
                    break;
                default:
                    break;
            }
        }

        private void OnEventError(int nServerNum, int nClientID, string sErrMsg)
        {
            String sMsg = String.Format(">> EventError - ServerNum({0}), ClientID({1}), ErrorMsg({2})", nServerNum, nClientID, sErrMsg);
            Console.WriteLine(sMsg);

            if (MyEventClientError != null) MyEventClientError(sErrMsg);

            if (!GetClientConnectionState(m_nClientID))
                SetClientDisconnect(m_nClientID);
        }
    }
}