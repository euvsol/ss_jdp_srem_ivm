﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using EuvMonitoringPgm.Data;
using EuvMonitoringPgm.UserControls;

namespace EuvMonitoringPgm
{
    public partial class UiCommon
    {
        private static UiCommon _inst = null;
        private static Object sync = new Object();

        public static UiCommon Knd
        {
            get
            {
                lock (sync)
                {
                    if (_inst == null) _inst = new UiCommon();
                    return _inst;
                }
            }
        }

        private Function _func = new Function();
        public Function Func
        {
            get { return _func; }
        }

        private Variable _var = new Variable();
        public Variable Var
        {
            get { return _var; }
        }

        private EquipmentData _data = new EquipmentData();
        public EquipmentData Data
        {
            get { return _data; }
        }

        CommIniFile _ini = new CommIniFile();
        public CommIniFile Ini
        {
            get { return _ini; }
        }

    }

    public class Function
    {
        /// <summary>
        /// Show Custom MessageBox
        /// </summary>
        /// <param name="eType">Question / Warning</param>
        /// <param name="sMsg">Text</param>
        /// <returns></returns>
        public bool ShowMessageBox(CustomMessageBox.MsgType eType, string sMsg)
        {
            CustomMessageBox dlg = new CustomMessageBox(eType, sMsg);
            dlg.Topmost = true;
            dlg.ShowDialog();

            if (dlg.DialogResult.HasValue && dlg.DialogResult.Value) return true;

            return false;
        }

        /// <summary>
        /// Check Window Opened or not
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sName"></param>
        /// <returns></returns>
        public bool IsWindowOpened<T>(string sName = "") where T : Window
        {
            return string.IsNullOrEmpty(sName)
                ? Application.Current.Windows.OfType<T>().Any()
                : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(sName));
        }

        /// <summary>
        /// Get Current Date & Time
        /// </summary>
        /// <returns></returns>
        public string GetCurrentDateTime()
        {
            #region ** Current DateTime **

            DateTime dt = DateTime.Now;

            string[] strDateTime = { dt.Year.ToString(), string.Format("{0:00}", dt.Month),  string.Format("{0:00}", dt.Day  ),
                                     string.Format("{0:00}", dt.Hour), string.Format("{0:00}", dt.Minute), string.Format("{0:00}", dt.Second) };

            return strDateTime[0] + "/" + strDateTime[1] + "/" + strDateTime[2] + "  " + strDateTime[3] + ":" + strDateTime[4] + ":" + strDateTime[5];

            #endregion
        }

        /// <summary>
        /// Get Current Time
        /// </summary>
        /// <returns></returns>
        public string GetCurrentTime()
        {
            #region ** Current Time **

            DateTime dt = DateTime.Now;

            string[] strTime = { string.Format("{0:00}", dt.Hour), string.Format("{0:00}", dt.Minute), string.Format("{0:00}", dt.Second), string.Format("{0:000}", dt.Millisecond) };

            return strTime[0] + ":" + strTime[1] + ":" + strTime[2] + "." + strTime[3];

            #endregion
        }
    }

    public class Variable
    {
        private string m_sLogFile = "EuvUsage.log";
        private string m_sLogPath = "d:\\EUVSolution\\Log\\";

        private string m_sIpAddress = "127.0.0.1";
        private int    m_nPort      = 9191;

        private int m_nNaviRealRangeX = 350;
        private int m_nNaviRealRangeY = 180;

        private int m_nNaviUIRangeX = 290;
        private int m_nNaviUIRangeY = -170;

        //XGem 관련 
        private int m_nEtcDataLength         = 5;
        private int m_nCalibrationDataLength = 22;
        private int m_nRecoveryDataLength    = 24;
        private int m_nVacuumDataLength      = 24;
        private int m_nPtrDataLength         = 7;
        private int m_nPhaseDataLength       = 56;
        private int m_nXRayDataLength        = 31;

        private int m_nEtcDataStart         = 5501;
        private int m_nCalibrationDataStart = 5511;
        private int m_nRecoveryDataStart    = 5541;
        private int m_nVacuumDataStart      = 5571;
        private int m_nPtrDataStart         = 5601;
        private int m_nPhaseDataStart       = 5611;
        private int m_nXRayDataStart        = 5671;

        

        public string LogFile
        {
            get { return m_sLogFile; }
        }

        public string LogPath
        {
            get { return m_sLogPath; }
        }

        public string IpAddress
        {
            get { return m_sIpAddress; }
        }

        public int Port
        {
            get { return m_nPort; }
        }

        public int NaviRealRangeX
        {
            get { return m_nNaviRealRangeX; }
        }

        public int NaviRealRangeY
        {
            get { return m_nNaviRealRangeY; }
        }

        public int NaviUIRangeX
        {
            get { return m_nNaviUIRangeX; }
        }

        public int NaviUIRangeY
        {
            get { return m_nNaviUIRangeY; }
        }

        public int EtcDataLength
        {
            get { return m_nEtcDataLength; }
        }

        public int CalibrationDataLength
        {
            get { return m_nCalibrationDataLength; }
        }

        public int RecoveryDataLength
        {
            get { return m_nRecoveryDataLength; }
        }

        public int VacuumDataLength
        {
            get { return m_nVacuumDataLength; }
        }

        public int PtrDataLength
        {
            get { return m_nPtrDataLength; }
        }

        public int PhaseDataLength
        {
            get { return m_nPhaseDataLength; }
        }

        public int XRayDataLength
        {
            get { return m_nXRayDataLength; }
        }

        public int EtcDataStart
        {
            get { return m_nEtcDataStart; }
        }

        public int CalibrationDataStart
        {
            get { return m_nCalibrationDataStart; }
        }

        public int RecoveryDataStart
        {
            get { return m_nRecoveryDataStart; }
        }

        public int VacuumDataStart
        {
            get { return m_nVacuumDataStart; }
        }

        public int PtrDataStart
        {
            get { return m_nPtrDataStart; }
        }

        public int PhaseDataStart
        {
            get { return m_nPhaseDataStart; }
        }

        public int XRayDataStart
        {
            get { return m_nXRayDataStart; }
        }
    }

    public class EquipmentData
    {
        private SystemData m_clsSysData = new SystemData();
        private MonitoringData m_clsEquipData = new MonitoringData();

        public SystemData SYSTEM
        {
            get { return m_clsSysData; }
        }

        public MonitoringData EQUIP
        {
            get { return m_clsEquipData; }
        }
    }

    public class CommIniFile
    {
        private string m_sFullPath = string.Empty;
        private const string DEFAULT_FILENAME = "Sample.ini";
        private const int DATASIZE = 260;

        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(
          string lpAppName,
          string lpKeyName,
          string lpString,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern uint GetPrivateProfileInt(
          string lpAppName,
          string lpKeyName,
          int nDefault,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(
          string lpAppName,
          string lpKeyName,
          string lpDefault,
          StringBuilder lpReturnedString,
          int nSize,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSectionNames(
          byte[] lpReturnedString,
          int nSize,
          string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(
          string section,
          byte[] lpReturnedString,
          int nSize,
          string lpFileName);

        public bool SetPath(string sDirPath, string sFileName)
        {
            string currentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (string.IsNullOrEmpty(sFileName))
                sFileName = "Sample.ini";

            this.m_sFullPath = Path.Combine(currentDirectory, sDirPath, sFileName);

            FileInfo fileInfo = new FileInfo(this.m_sFullPath);
            if (fileInfo.Exists)
                return true;
            DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(currentDirectory, sDirPath));
            if (!directoryInfo.Exists)
                directoryInfo.Create();
            fileInfo.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            return false;
        }

        public bool SetValue(string sSection, string sKey, string sValue)
        {
            return CommIniFile.WritePrivateProfileString(sSection, sKey, sValue, this.m_sFullPath);
        }

        public bool SetValue(string sSection, string sKey, int nValue)
        {
            return CommIniFile.WritePrivateProfileString(sSection, sKey, nValue.ToString(), this.m_sFullPath);
        }

        public bool SetValue(string sSection, string sKey, double fValue)
        {
            return CommIniFile.WritePrivateProfileString(sSection, sKey, fValue.ToString(), this.m_sFullPath);
        }

        public string GetBigValue(string sSection, string sKey)
        {
            StringBuilder lpReturnedString = new StringBuilder(4096);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            return lpReturnedString.ToString();
        }

        public string GetValue(string sSection, string sKey)
        {
            StringBuilder lpReturnedString = new StringBuilder(512);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            return lpReturnedString.ToString();
        }

        public int GetValueI(string sSection, string sKey)
        {
            int num = 0;
            StringBuilder lpReturnedString = new StringBuilder(512);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            try
            {
                num = Convert.ToInt32(lpReturnedString.ToString());
            }
            catch (FormatException ex)
            {
            }
            return num;
        }

        public double GetValueF(string sSection, string sKey)
        {
            double num = 0.0;
            StringBuilder lpReturnedString = new StringBuilder(512);
            CommIniFile.GetPrivateProfileString(sSection, sKey, "", lpReturnedString, lpReturnedString.Capacity, this.m_sFullPath);
            try
            {
                num = Convert.ToDouble(lpReturnedString.ToString());
            }
            catch (FormatException ex)
            {
            }
            return num;
        }

        public void DeleteSection(string sSection)
        {
            CommIniFile.WritePrivateProfileString(sSection, (string)null, (string)null, this.m_sFullPath);
        }

        public void DeleteKey(string sSection, string sKey)
        {
            CommIniFile.WritePrivateProfileString(sSection, sKey, (string)null, this.m_sFullPath);
        }

        public int GetSectionCount()
        {
            byte[] lpReturnedString = new byte[260];
            List<byte[]> numArrayList = new List<byte[]>();
            int index1 = 0;
            int index2 = 0;
            int profileSectionNames = CommIniFile.GetPrivateProfileSectionNames(lpReturnedString, 260, this.m_sFullPath);
            for (int index3 = 0; index3 < profileSectionNames; ++index3)
            {
                numArrayList.Add(new byte[index3 + 260]);
                if (lpReturnedString[index3] == (byte)0)
                {
                    numArrayList[index1][index2] = (byte)0;
                    index2 = 0;
                    ++index1;
                }
                else
                    numArrayList[index1][index2++] = lpReturnedString[index3];
            }
            return index1;
        }

        public int GetKeyCount(string sSection)
        {
            byte[] lpReturnedString = new byte[260];
            List<byte[]> numArrayList = new List<byte[]>();
            int index1 = 0;
            int index2 = 0;
            int privateProfileSection = CommIniFile.GetPrivateProfileSection(sSection, lpReturnedString, 260, this.m_sFullPath);
            for (int index3 = 0; index3 < privateProfileSection; ++index3)
            {
                numArrayList.Add(new byte[index3 + 260]);
                if (lpReturnedString[index3] == (byte)0)
                {
                    numArrayList[index1][index2] = (byte)0;
                    index2 = 0;
                    ++index1;
                }
                else
                    numArrayList[index1][index2++] = lpReturnedString[index3];
            }
            return index1;
        }
    }
}
