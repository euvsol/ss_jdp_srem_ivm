﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace EuvMonitoringPgm.Converter
{
    public class UsageValueToProgressBarColorConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            int _value;

            if (!int.TryParse(value.ToString(), out _value)) return Brushes.LightGray;

            if (_value >= 75)
            {
                return Brushes.Red;
            }
            else if (_value >= 40)
            {
                return Brushes.Yellow;
            }
            else
            {
                return Brushes.LawnGreen;
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class UsageValueToFontColorConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            int _value;

            if (!int.TryParse(value.ToString(), out _value)) return Brushes.Gray;

            if (_value >= 75)
            {
                return Brushes.Red;
            }
            else
            {
                return Brushes.Black;
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class UsageValueToProgressBarPositionConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            int _value;

            if (!int.TryParse(value.ToString(), out _value)) return 0;

            return _value / 1.67;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
