﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace EuvMonitoringPgm.Converter
{
    public class CheckToVisibilityConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if ((bool)value == true)
                return Visibility.Visible;

            return Visibility.Collapsed;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class ReverseIsCheckedConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class DialogTypeToQuestionAlertVisibleConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!int.TryParse(value.ToString(), out int _value))
                return Visibility.Collapsed;

            if (parameter.ToString().Equals("Question"))
                return _value == 0 ? Visibility.Visible : Visibility.Collapsed;
            else
                return _value == 1 ? Visibility.Visible : Visibility.Collapsed;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class ProgressBarEuvUsingTimeConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!double.TryParse(value.ToString(), out double _value))
                return 0;

            if (parameter.ToString().Equals("TotalMin"))
                return (int)(_value / 60);
            else if (parameter.ToString().Equals("Hours"))
                return (int)(_value / 60 / 60);
            else if (parameter.ToString().Equals("Minutes"))
                return (int)(_value / 60 % 60);
            else
                return 0;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }


    public class IsCheckedDryPumpStatusConverter : IMultiValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object[] values, Type targetType, Object parameter, CultureInfo culture)
        {
            bool[] _values = Array.ConvertAll(values, o => (bool)o);

            if (Array.Exists(_values, elemet => elemet == true))
                return true;
            else
                return false;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class MultipleCheckToVisibleConverter : IMultiValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object[] values, Type targetType, Object parameter, CultureInfo culture)
        {
            bool[] _values = Array.ConvertAll(values, o => (bool)o);

            if (Array.Exists(_values, elemet => elemet == true))
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class EuvBeamStatusConverter : IMultiValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object[] values, Type targetType, Object parameter, CultureInfo culture)
        {
            if((string)parameter == "1")
            {
                if ((bool)values[0] && (bool)values[1])
                    return true;
                else
                    return false;
            }
            else
            {
                if ((bool)values[0] && !(bool)values[1])
                    return true;
                else
                    return false;
            }
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class DoubleToDurationConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Duration duration = new Duration(TimeSpan.FromSeconds((double)value));
            return duration;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    public class NullToVisibilityConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (value == null)
                return Visibility.Visible;

            return Visibility.Collapsed;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
        #endregion
    }

    public class BooleanToSelectedIndexConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            int nValue;

            if (value == null)
                nValue = -1;

            nValue = (bool)value == true ? 1 : 0;

            return nValue;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
        #endregion
    }

    public class IntToIsThreeStateConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if(!int.TryParse(value.ToString(), out int nValue))
                return false;

            if (nValue == 0)
                return false;
            else if (nValue == 1)
                return true;
            else
                return null;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
        #endregion
    }

    public class ConfigViewerMarginConverter : IValueConverter
    {
        #region ** IValueConverter Members **

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            bool bValue = (bool)value;

            if ((string)parameter == "Srem")
            {
                if(bValue)
                    return new Thickness(0, 0, 0, 0);
                else
                    return new Thickness(-1910, 0, 0, 0);
            }
            else if ((string)parameter == "Ptr")
            {
                if (bValue)
                    return new Thickness(0, 0, 0, 0);
                else
                    return new Thickness(0, 0, -1910, 0);
            }
            else if ((string)parameter == "Phase")
            {
                if (bValue)
                    return new Thickness(0, 0, 0, 0);
                else
                    return new Thickness(0, 0, 0, -850);
            }
            else if ((string)parameter == "XRay")
            {
                if (bValue)
                    return new Thickness(0, 50, 0, 0);
                else
                    return new Thickness(0, -850, 0, 0);
            }
            else
            {
                return new Thickness(0, 0, 0, 0);
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
