﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Reflection;

using Forms = System.Windows.Forms; //System.Windows.Forms.Application , System.Windows.Application 모호성때문..

using EuvMonitoringPgm.Data;
using EuvMonitoringPgm.View;
using EuvMonitoringPgm.UserControls;
using CommonLib;

namespace EuvMonitoringPgm
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        #region ** System Usage Information **

        PerformanceCounter _cpu;
        DriveInfo _hdd1 = new DriveInfo("c:\\");
        DriveInfo _hdd2 = new DriveInfo("d:\\");

        #endregion

        #region ** NotifyIcon **

        public Forms.NotifyIcon ni = new Forms.NotifyIcon();
        Forms.MenuItem itemShow = new Forms.MenuItem();
        Forms.MenuItem itemHide = new Forms.MenuItem();
        Forms.MenuItem itemExit = new Forms.MenuItem();

        #endregion

        MySocket mySock  = null;

        Thread SystemThread     = null;

        ViewMain       viewMain   = null;

        public MySocket Socket
        {
            get { return mySock; }
        }

        public MainWindow()
        {
            InitializeComponent();

            #region ** Notify ICon **

            Forms.ContextMenu menu = new Forms.ContextMenu();
            
            itemShow.Index = 0;
            itemShow.Text = "Show";
            itemShow.Click += delegate (object click, EventArgs eClick)
            {
                ShowApplication();
            };

            itemHide.Index = 0;
            itemHide.Text = "Hide";
            itemHide.Enabled = false;
            itemHide.Click += delegate (object click, EventArgs eClick)
            {
                HideApplication();
            };

            itemExit.Index = 0;
            itemExit.Text = "Exit";
            itemExit.Click += delegate (object click, EventArgs eClick)
            {
                ExitApplication();
                ni.Dispose();
            };

            menu.MenuItems.Add(itemShow);
            menu.MenuItems.Add(itemHide);
            menu.MenuItems.Add(itemExit);

            ni.Icon = Properties.Resources.Pgm;
            ni.ContextMenu = menu;
            ni.Text = "SREM UI";

            #endregion

            DataContext = UiCommon.Knd.Data.SYSTEM;
            UiCommon.Knd.Ini.SetPath("Config", "SysConfig.ini");
            UiCommon.Knd.Data.EQUIP.UseFlipper = UiCommon.Knd.Ini.GetValueI("Mts", "UseFlipper") == 1 ? true : false;
            UiCommon.Knd.Data.EQUIP.UseRotator = UiCommon.Knd.Ini.GetValueI("Mts", "UseRotator") == 1 ? true : false;
            UiCommon.Knd.Data.EQUIP.UseBSTool = UiCommon.Knd.Ini.GetValueI("BSTool", "Use") == 1 ? true : false;
            double dStagePosX = UiCommon.Knd.Ini.GetValueF("StagePos", "PosX");
            double dStagePosY = UiCommon.Knd.Ini.GetValueF("StagePos", "PosY");
            double dSensorPos = UiCommon.Knd.Ini.GetValueF("StagePos", "SensorPos");
            double dOmLeftPos = UiCommon.Knd.Ini.GetValueF("StagePos", "OmLeft");
            double dOmTopPos = UiCommon.Knd.Ini.GetValueF("StagePos", "OmTop");
            double dEuvLeftPos = UiCommon.Knd.Ini.GetValueF("StagePos", "EuvLeft");
            double dEuvTopPos = UiCommon.Knd.Ini.GetValueF("StagePos", "EuvTop");
            UiCommon.Knd.Data.EQUIP.StageMargin = new Thickness(0, 0, dStagePosX, dStagePosY);
            UiCommon.Knd.Data.EQUIP.SensorMargin = new Thickness(0, 0, dSensorPos, 0);
            UiCommon.Knd.Data.EQUIP.OmMargin = new Thickness(dOmLeftPos, dOmTopPos, 0, 0);
            UiCommon.Knd.Data.EQUIP.EuvMargin = new Thickness(dEuvLeftPos, dEuvTopPos, 0, 0);


            #region ** Set Performance Counter **

            _cpu = new PerformanceCounter();
            _cpu.CategoryName = "Processor";
            _cpu.CounterName = "% Processor Time";
            _cpu.InstanceName = "_Total";
            _cpu.NextValue();

            #endregion

            UiCommon.Knd.Data.SYSTEM.Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            mySock = new MySocket();

            viewMain   = new ViewMain();

            #region ** Reg. Socket Event **

            mySock.MyEventClientConnected       += new MySocket.DelegateClientConnect(OnConnected);
            mySock.MyEventClientDisconnected    += new MySocket.DelegateClientDisconnect(OnDisconnected);
            mySock.MyEventClientMsgReceived     += new MySocket.DelegateClientMsgReceive(OnMsgReceived);
            mySock.MyEventClientError           += new MySocket.DelegateClientError(OnError);
            mySock.MyEventGuiDataUpdate         += new MySocket.DelegateGuiDataUpdate(viewMain.UpdateEquipmentStatus);
            
            #endregion

            SystemThread = new Thread(new ThreadStart(DoWork));
            SystemThread.IsBackground = true;
        }

        
        private void ShowApplication()
        {
            itemShow.Enabled = false;
            itemHide.Enabled = true;

            this.Left = 1425;
            this.Top  = 0;

            if(!this.IsVisible)
                this.Show();

            this.Activate();
            this.Focus();
        }

        private void HideApplication()
        {
            itemShow.Enabled = true;
            itemHide.Enabled = false;

            this.Hide();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MainGrid.Children.Add(viewMain);
            SystemThread.Start();
        }

        private void OnMsgReceived(string sRecvMsg)
        {
            string[] Recv = sRecvMsg.Split('_');

            if (Recv.Length == 0) return;

            if(Recv[0].Equals("EQ"))
            {
                if (Recv[1].Equals("MTS"))
                {
                    if(Recv[2].Equals("INIT"))
                    {
                        viewMain.MoveMtsRobot((int)MtsRobotSt.HOME);
                        viewMain.RotateMaterial();
                        viewMain.FlipMaterial();
                        viewMain.UnloadLPModule();
                    }
                }
                else if(Recv[1].Equals("LPM"))
                {
                    switch (Recv[2])
                    {
                        case "LOAD":
                            viewMain.LoadLPModule();
                            break;
                        case "UNLOAD":
                            viewMain.UnloadLPModule();
                            break;
                    }
                }
                else if(Recv[1].Equals("ATR"))
                {
                    switch(Recv[2])
                    {
                        case "INIT":
                            viewMain.MoveMtsRobot((int)MtsRobotSt.HOME);
                            break;
                        case "PICK":
                            if(Recv[3].Equals("LPM"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PICK_LPM);
                            else if(Recv[3].Equals("STAGE"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PICK_LLC);
                            else if (Recv[3].Equals("FLIPPER"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PICK_FLIPPER);
                            else if (Recv[3].Equals("ROTATOR"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PICK_ROTATOR);
                            break;
                        case "PLACE":
                            if (Recv[3].Equals("LPM"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PLACE_LPM);
                            else if (Recv[3].Equals("STAGE"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PLACE_LLC);
                            else if (Recv[3].Equals("FLIPPER"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PLACE_FLIPPER);
                            else if (Recv[3].Equals("ROTATOR"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.PLACE_ROTATOR);
                            break;
                        case "READY":
                            if (Recv[3].Equals("STAGE"))
                                viewMain.MoveMtsRobot((int)MtsRobotSt.READY_LLC);
                            break;
                        case "STOP":
                            viewMain.PauseAtrRobot();
                            break;
                    }
                }
                else if(Recv[1].Equals("ROTATOR"))
                {
                    switch (Recv[2])
                    {
                        case "WORKING":
                            viewMain.RotateMaterial();
                            break;
                    }
                }
                else if (Recv[1].Equals("FLIPPER"))
                {
                    switch (Recv[2])
                    {
                        case "WORKING":
                            viewMain.FlipMaterial();
                            break;
                    }
                }
                else if (Recv[1].Equals("VMTR"))
                {
                    switch (Recv[2])
                    {
                        case "INIT":
                            viewMain.MoveVmtrRobot((int)VmtrRobotSt.HOME);
                            break;
                        case "PICK":
                            if (Recv[3].Equals("LLC"))
                                viewMain.MoveVmtrRobot((int)VmtrRobotSt.PICK_LLC);
                            else if (Recv[3].Equals("MC"))
                                viewMain.MoveVmtrRobot((int)VmtrRobotSt.PICK_MC);
                            break;
                        case "PLACE":
                            if (Recv[3].Equals("LLC"))
                                viewMain.MoveVmtrRobot((int)VmtrRobotSt.PLACE_LLC);
                            else if (Recv[3].Equals("MC"))
                                viewMain.MoveVmtrRobot((int)VmtrRobotSt.PLACE_MC);
                            break;
                        case "READY":
                            if (Recv[3].Equals("LLC"))
                                viewMain.MoveVmtrRobot((int)VmtrRobotSt.READY_LLC);
                            if (Recv[3].Equals("MC"))
                                viewMain.MoveVmtrRobot((int)VmtrRobotSt.READY_MC);
                            break;
                        case "STOP":
                            viewMain.PauseVmtrRobot();
                            break;
                    }
                }
                else if (Recv[1].Equals("MATERIAL"))
                {
                    viewMain.SetMaterial(Recv[2]);
                }
            }
            else if(Recv[0].Equals("APP"))
            {
                if (Recv[1].Equals("SHOW"))
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        ShowApplication();
                    });
                }
                else if(Recv[1].Equals("HIDE"))
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        HideApplication();
                    });
                }
            }
        }

        private void OnError(string sErrMsg)
        {
            //WriteMessage("[Error] : " + sErrMsg);
        }

        private void OnDisconnected()
        {
            //WriteMessage("[Socket] : Client Disconnected.");

            UiCommon.Knd.Data.SYSTEM.IsOnlineEq = false;
        }

        private void OnConnected()
        {
            //WriteMessage("[Socket] : Client Connected.");

            UiCommon.Knd.Data.SYSTEM.IsOnlineEq = true;
        }

        private void SendMessageToClient(string sMsg)
        {
            MessageDataPacket data = new MessageDataPacket();
            data.message_id = (int)MsgId.Message;
            data.message_size = Marshal.SizeOf(typeof(MessageDataPacket)) - 8;

            data.Message = sMsg;

            byte[] arrData = data.Serialize();
            mySock.SetSend(0, arrData);
        }

        private void DoWork()
        {
            while (true)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    GetSystemUsage();
                });

                Thread.Sleep(500);
            }
        }

        private void GetSystemUsage()
        {
            #region ** System Usage Information **

            UiCommon.Knd.Data.SYSTEM.CpuUsage = Convert.ToInt32(_cpu.NextValue().ToString("N0"));
            UiCommon.Knd.Data.SYSTEM.RamUsage = Convert.ToInt32(GetCurrentRamUsage());

            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (DriveInfo drive in drives)
            {
                if (drive.DriveType == DriveType.Fixed)
                {
                    switch (drive.Name)
                    {
                        case "c:\\":
                        case "C:\\":
                            UiCommon.Knd.Data.SYSTEM.Hdd1Usage = Convert.ToInt32(GetCurrentHDDUsage(_hdd1));
                            break;

                        case "d:\\":
                        case "D:\\":
                            UiCommon.Knd.Data.SYSTEM.Hdd2Usage = Convert.ToInt32(GetCurrentHDDUsage(_hdd2));
                            break;
                    }
                }
            }

            UiCommon.Knd.Data.SYSTEM.Datetime = UiCommon.Knd.Func.GetCurrentDateTime();

            #endregion
        }

        private void TopGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            if (!UiCommon.Knd.Func.ShowMessageBox(CustomMessageBox.MsgType.Question, "프로그램을 종료할까요?")) return;

            ExitApplication();
        }

        private void ExitApplication()
        {
            ThreadStart ts = delegate ()
            {
                Dispatcher.BeginInvoke((Action)delegate ()
                {
                    mySock.SetStop();
                    viewMain.Stop();

                    Application.Current.Shutdown();
                });
            };

            Thread t = new Thread(ts);
            t.Start();
        }

        #region ** Get System Usage (Memory, Hard Disk) **

        private Decimal GetCurrentRamUsage()
        {
            Int64 phav = PerformanceInfo.GetPhysicalAvailableMemoryInMiB();
            Int64 tot = PerformanceInfo.GetTotalMemoryInMiB();

            Decimal percentFree = ((Decimal)phav / (Decimal)tot) * 100;
            Decimal percentOccupied = 100 - percentFree;

            return percentOccupied;
        }

        private String GetCurrentHDDUsage(DriveInfo tmpDrive)
        {
            String[] strDiskInfo = { tmpDrive.TotalSize.ToString(), tmpDrive.TotalFreeSpace.ToString() };
            String strFullSizeofD = (Convert.ToDouble(strDiskInfo[0]) / 1024 / 1024 / 1024).ToString("N2");
            String strRestSizeofD = (Convert.ToDouble(strDiskInfo[1]) / 1024 / 1024 / 1024).ToString("N2");

            return ((Convert.ToDouble(strFullSizeofD) - Convert.ToDouble(strRestSizeofD)) / Convert.ToDouble(strFullSizeofD) * 100).ToString("N0");
        }

        #endregion

        #region ** Performance Information **
        public static class PerformanceInfo
        {
            [DllImport("psapi.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool GetPerformanceInfo([Out] out PerformanceInformation PerformanceInformation, [In] Int32 Size);

            [StructLayout(LayoutKind.Sequential)]
            public struct PerformanceInformation
            {
                public Int32 Size;
                public IntPtr CommitTotal;
                public IntPtr CommitLimit;
                public IntPtr CommitPeak;
                public IntPtr PhysicalTotal;
                public IntPtr PhysicalAvailable;
                public IntPtr SystemCache;
                public IntPtr KernelTotal;
                public IntPtr KernelPaged;
                public IntPtr KernelNonPaged;
                public IntPtr PageSize;
                public Int32 HandlesCount;
                public Int32 ProcessCount;
                public Int32 ThreadCount;
            }

            public static Int64 GetPhysicalAvailableMemoryInMiB()
            {
                PerformanceInformation pi = new PerformanceInformation();

                if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
                {
                    return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / 1048576));
                }
                else
                {
                    return -1;
                }
            }

            public static Int64 GetTotalMemoryInMiB()
            {
                PerformanceInformation pi = new PerformanceInformation();

                if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
                {
                    return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / 1048576));
                }
                else
                {
                    return -1;
                }
            }
        }

        #endregion

        private void TopGrid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        private void BtnHideApp_Click(object sender, RoutedEventArgs e)
        {
            HideApplication();
        }
    }
}
