﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace EuvMonitoringPgm
{
    /// <summary>
    /// SplashScreenWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class SplashScreenWindow : Window, ISplashScreen
    {
        public SplashScreenWindow()
        {
            InitializeComponent();

            lblVersion.Content = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public void AddMessage(string msg, Brush color)
        {
            Dispatcher.Invoke((Action)delegate ()
            {
                TextRange tr = new TextRange(lbRichMessage.Document.ContentEnd, lbRichMessage.Document.ContentEnd);
                tr.Text = msg + "\r";
                tr.ApplyPropertyValue(TextElement.ForegroundProperty, color);
                lbRichMessage.ScrollToEnd();
            });
        }

        public void LoadComplete()
        {
            Dispatcher.InvokeShutdown();
        }
    }

    public interface ISplashScreen
    {
        void AddMessage(string msg, Brush color);
        void LoadComplete();
    }
}
