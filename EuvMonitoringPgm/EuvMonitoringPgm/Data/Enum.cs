﻿
namespace EuvMonitoringPgm.Data
{
    enum MaterialLoc
    {
        UNKNOWN = -1,
        MTS_POD,
        MTS_ROBOT,
        MTS_ROTATOR,
        MTS_FLIPPER,
        LLC,
        VAC_ROBOT,
        CHUCK
    }

    enum MtsRobotSt
    {
        HOME = 0,
        PICK_LPM,
        PLACE_LPM,
        PICK_LLC,
        PLACE_LLC,
        PICK_FLIPPER,
        PLACE_FLIPPER,
        PICK_ROTATOR,
        PLACE_ROTATOR,
        READY_LLC
    }

    enum MtsRobotDir
    {
        HOME = 0,
        LPM  = 90,
        UTIL = 180,
        LLC  = 270
    }

    enum VmtrRobotSt
    {
        HOME = 0,
        PICK_LLC,
        PLACE_LLC,
        PICK_MC,
        PLACE_MC,
        READY_LLC,
        READY_MC
    }

    enum VmtrRobotDir
    {
        HOME = -180,
        LLC  = -90,
        MC   =  0
    }

    enum SeqProcess
    {
        OnMtsPodLoading = 0,
        OnAtrHandLoading,
        OnFlipperLoading,
        OnFlipperAfterFlipLoading,
        OnAtrAfterFlipLoading,
        OnRotatorLoading,
        OnRotatorAfterRotateLoading,
        OnAtrAfterRotateLoading,
        OnLlcLoading,
        OnLlcVacuumLoading,
        OnVmtrHandLoading,
        OnChuckLoading,
        LoadingComplete,

        OnChuck,
        OnVmtrUnloading,
        OnLlcUnloading,
        OnLlcVentUnloading,
        OnAtrHandUnloading,
        OnLlcVacuumUnloading,
        OnRotatorUnloading,
        OnRotatorAfterRotateUnloading,
        OnAtrAfterRotateUnloading,
        OnFlipperUnloading,
        OnFlipperAfterFlipUnloading,
        OnAtrAfterFlipUnloading,
        OnMtsPodUnloading,
        UnloadingComplete
    }

    enum ProcessingState
    {
        None = -1,
        SystemPowerUp = 1,
        Empty,
        Idle,
        Standby,
        Processing,
        HoldProcessing
    }

    enum TransferState
    {
        OutOfService = 0,
        TransferBlocked,
        ReadyToLoad,
        ReadyToUnload,
        InService = 100
    }

    enum AccessMode
    {
        Manual = 0,
        Auto
    }

    enum ReservationState
    {
        NotReserved = 0,
        Reserved
    }

    enum AssociationState
    {
        NotAssociated = 0,
        Associated
    }

    enum SensorState
    {
        SensorOff = 0,
        SensorOn
    }

    enum Signal
    {
        Valid = 1,
        CS_0,
        CS_1,
        TR_REQ,
        L_REQ,
        U_REQ,
        READY,
        BUSY,
        COMPT,
        CONT,
        H0_AVBL,
        ES
    }

    enum SignalState
    {
        SignalOff = 0,
        SignalOn
    }

    enum CarrierAccessState
    {
        NotAccessed = 0,
        InAccessed,
        CarrierCompleted,
        CarrierStopped
    }

    enum SubstProcessingState
    {
        NeedsProcessing = 0,
        InProcess,
        Processed,
        Aborted,
        Stopped,
        Rejected,
        Lost,
        Skipped
    }

    enum Result
    {
        Succeeded = 0,
        Failed
    }

    enum Header
    {
        MsgId = 0,
        MsgSize
    }

    enum MsgId
    {
        Monitoring = 0,
        EtcData,
        CalibrationData,
        RecoveryData,
        VacuumSeqData,
        PtrData,
        PhaseData,
        XRayCamData,
        Message
    }

    enum PRJobState
    {
        Pj_NoState = -1,
        Pj_JobQueued,			//0
        Pj_SettingUp,			//1 
        Pj_WaintingForStart,	//2
        Pj_Processing,			//3
        Pj_ProcessComplete,		//4
        Pj_Reserved,			//5
        Pj_Pausing,				//6
        Pj_Paused,				//7
        Pj_Stopping,			//8
        Pj_Aborting,			//9
        Pj_Stopped,				//10
        Pj_Aborted,				//11
        Pj_JobCanceled,			//12
        Pj_JobActive,			//13
        Pj_NotPaused,			//14
        Pj_NotStopping,			//15
        Pj_NotAbroting,			//16
        Pj_JobComplete,			//17
    }
}
