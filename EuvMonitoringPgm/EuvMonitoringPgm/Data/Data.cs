﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows;

namespace EuvMonitoringPgm.Data
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MonitoringDataPacket
    {
        #region ** DataPacket Struct **

        [MarshalAs(UnmanagedType.I4)]
        public int message_id;
        [MarshalAs(UnmanagedType.I4)]
        public int message_size;

        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectAdam;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectCrevis;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectMts;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectVmtr;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectCam1;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectScanStage;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectNaviStage;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectFilterStage;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectSource;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectVacuumGauge;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectLlcTmp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectMcTmp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectAfm;             //not use
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectRevolver;             //not use
        [MarshalAs(UnmanagedType.Bool)]
        public bool ConnectXrayCamera;

        [MarshalAs(UnmanagedType.Bool)]
        public bool SequenceWork;             //not use

        [MarshalAs(UnmanagedType.Bool)]
        public bool LpmPodExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LpmMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LpmInitComp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LpmPodOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LpmWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LpmError;

        [MarshalAs(UnmanagedType.Bool)]
        public bool AtrMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool AtrInitComp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool AtrWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool AtrError;

        [MarshalAs(UnmanagedType.Bool)]
        public bool FlipperMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool FlipperInitComp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool FlipperWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool FlipperError;

        [MarshalAs(UnmanagedType.Bool)]
        public bool RotatorMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool RotatorInitComp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool RotatorWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool RotatorError;

        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcGateValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcTmpGateValveOpened;        
        [MarshalAs(UnmanagedType.I4)]
        public int LlcTmpState;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcTmpError;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcForelineValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcSlowRoughingValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcFastRoughingValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcDryPumpWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcDryPumpAlarm;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcDryPumpWarning;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string LlcVacuumRate;

        [MarshalAs(UnmanagedType.Bool)]
        public bool McChuckMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McTrGateValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McTmpGateValveOpened;
        [MarshalAs(UnmanagedType.I4)]
        public int McTmpState;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McTmpError;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McForelineValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McSlowRoughingValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McFastRoughingValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McDryPumpWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McDryPumpAlarm;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McDryPumpWarning;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string McVacuumRate;

        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrMaskExist;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrServoOn;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrInitComp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrWorking;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrExtended;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrRetracted;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrError;
        [MarshalAs(UnmanagedType.Bool)]
        public bool VmtrEMO;
        [MarshalAs(UnmanagedType.R8)]
        public double VmtrPositionT;
        [MarshalAs(UnmanagedType.R8)]
        public double VmtrPositionL;
        [MarshalAs(UnmanagedType.R8)]
        public double VmtrPositionZ;

        [MarshalAs(UnmanagedType.Bool)]
        public bool NaviLoadingPos;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string NaviFeedbackPosX;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string NaviFeedbackPosY;

        [MarshalAs(UnmanagedType.Bool)]
        public bool SlowVentValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool SlowInletValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool SlowOutletValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool FastVentValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool FastInletValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool FastOutletValveOpened;
        [MarshalAs(UnmanagedType.R8)]
        public double Mfc1N2;
        [MarshalAs(UnmanagedType.R8)]
        public double Mfc2N2;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string SrcMcVacuumRate;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string SrcScVacuumRate;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string SrcBcVacuumRate;
        [MarshalAs(UnmanagedType.Bool)]
        public bool EuvLaserShutterSol;
        [MarshalAs(UnmanagedType.Bool)]
        public bool EuvOn;
        [MarshalAs(UnmanagedType.Bool)]
        public bool SrcMechShutterOpened;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string SrcMirrorStagePos;

        [MarshalAs(UnmanagedType.Bool)]
        public bool WaterSupplyValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool WaterReturnValveOpened;
        [MarshalAs(UnmanagedType.Bool)]
        public bool MainAir;
        [MarshalAs(UnmanagedType.Bool)]
        public bool MainWater;
        [MarshalAs(UnmanagedType.Bool)]
        public bool WaterLeakLlcTmp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool WaterLeakMcTmp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool SmokeDetectCb;
        [MarshalAs(UnmanagedType.Bool)]
        public bool SmokeDetectVacSt;
        [MarshalAs(UnmanagedType.Bool)]
        public bool AlarmWaterReturnTemp;
        [MarshalAs(UnmanagedType.Bool)]
        public bool AvailableSrcGateOpened;

        [MarshalAs(UnmanagedType.Bool)]
        public bool McMaskTiltError1;
        [MarshalAs(UnmanagedType.Bool)]
        public bool McMaskTiltError2;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcMaskTiltError1;
        [MarshalAs(UnmanagedType.Bool)]
        public bool LlcMaskTiltError2;

        [MarshalAs(UnmanagedType.I4)]
        public int MaskLocation;

        public byte[] Serialize()
        {
            var buffer  = new byte[Marshal.SizeOf(typeof(MonitoringDataPacket))];
            var gch     = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();
            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();

            return buffer;
        }

        public void Deserialize(ref byte[] data)
        {
            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this    = (MonitoringDataPacket)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(MonitoringDataPacket));
            gch.Free();
        }

        #endregion
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MessageDataPacket
    {
        #region ** DataPacket Struct **

        [MarshalAs(UnmanagedType.I4)]
        public int message_id;

        [MarshalAs(UnmanagedType.I4)]
        public int message_size;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string Message;

        public byte[] Serialize()
        {
            var buffer  = new byte[Marshal.SizeOf(typeof(MessageDataPacket))];
            var gch     = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();
            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();

            return buffer;
        }

        public void Deserialize(ref byte[] data)
        {
            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this    = (MessageDataPacket)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(MessageDataPacket));
            gch.Free();
        }

        #endregion
    }

    public class SystemData : INotifyPropertyChanged
    {
        private string _version;

        public string Version
        {
            get { return _version; }
            set
            {
                if (_version != value)
                {
                    _version = value;
                    OnPropertyChanged("Version");
                }
            }
        }

        private int _cpuUsage;
        public int CpuUsage
        {
            get { return _cpuUsage; }
            set
            {
                if (_cpuUsage != value)
                {
                    _cpuUsage = value;
                    OnPropertyChanged("CpuUsage");
                }
            }
        }

        private int _ramUsage;
        public int RamUsage
        {
            get { return _ramUsage; }
            set
            {
                if(_ramUsage != value)
                {
                    _ramUsage = value;
                    OnPropertyChanged("RamUsage");
                }
            }
        }

        private int _hdd1Usage;
        public int Hdd1Usage
        {
            get { return _hdd1Usage; }
            set
            {
                if(_hdd1Usage != value)
                {
                    _hdd1Usage = value;
                    OnPropertyChanged("Hdd1Usage");
                }
            }
        }

        private int _hdd2Usage;
        public int Hdd2Usage
        {
            get { return _hdd2Usage; }
            set
            {
                if(_hdd2Usage != value)
                {
                    _hdd2Usage = value;
                    OnPropertyChanged("Hdd2Usage");
                }
            }
        }

        private string _dateTime;

        public string Datetime
        {
            get { return _dateTime; }
            set
            {
                if (_dateTime != value)
                {
                    _dateTime = value;
                    OnPropertyChanged("Datetime");
                }
            }
        }

        private bool _isOnlineEq;

        public bool IsOnlineEq
        {
            get { return _isOnlineEq; }
            set
            {
                if(_isOnlineEq != value)
                {
                    _isOnlineEq = value;
                    OnPropertyChanged("IsOnlineEq");
                }
            }
        }

        private bool _isConnAdam;

        public bool IsConnAdam
        {
            get { return _isConnAdam; }
            set
            {
                if (_isConnAdam != value)
                {
                    _isConnAdam = value;
                    OnPropertyChanged("IsConnAdam");
                }
            }
        }

        private bool _isConnCrevis;

        public bool IsConnCrevis
        {
            get { return _isConnCrevis; }
            set
            {
                if (_isConnCrevis != value)
                {
                    _isConnCrevis = value;
                    OnPropertyChanged("IsConnCrevis");
                }
            }
        }

        private bool _isConnMts;

        public bool IsConnMts
        {
            get { return _isConnMts; }
            set
            {
                if (_isConnMts != value)
                {
                    _isConnMts = value;
                    OnPropertyChanged("IsConnMts");
                }
            }
        }

        private bool _isConnVmtr;

        public bool IsConnVmtr
        {
            get { return _isConnVmtr; }
            set
            {
                if (_isConnVmtr != value)
                {
                    _isConnVmtr = value;
                    OnPropertyChanged("IsConnVmtr");
                }
            }
        }

        private bool _isConnOmCam;

        public bool IsConnOmCam
        {
            get { return _isConnOmCam; }
            set
            {
                if (_isConnOmCam != value)
                {
                    _isConnOmCam = value;
                    OnPropertyChanged("IsConnOmCam");
                }
            }
        }

        private bool _isConnScanStage;

        public bool IsConnScanStage
        {
            get { return _isConnScanStage; }
            set
            {
                if (_isConnScanStage != value)
                {
                    _isConnScanStage = value;
                    OnPropertyChanged("IsConnScanStage");
                }
            }
        }

        private bool _isConnNaviStage;

        public bool IsConnNaviStage
        {
            get { return _isConnNaviStage; }
            set
            {
                if (_isConnNaviStage != value)
                {
                    _isConnNaviStage = value;
                    OnPropertyChanged("IsConnNaviStage");
                }
            }
        }

        private bool _isConnFilterStage;

        public bool IsConnFilterStage
        {
            get { return _isConnFilterStage; }
            set
            {
                if (_isConnFilterStage != value)
                {
                    _isConnFilterStage = value;
                    OnPropertyChanged("IsConnFilterStage");
                }
            }
        }

        private bool _isConnSource;

        public bool IsConnSource
        {
            get { return _isConnSource; }
            set
            {
                if (_isConnSource != value)
                {
                    _isConnSource = value;
                    OnPropertyChanged("IsConnSource");
                }
            }
        }

        private bool _isConnVacuumGauge;

        public bool IsConnVacuumGauge
        {
            get { return _isConnVacuumGauge; }
            set
            {
                if (_isConnVacuumGauge != value)
                {
                    _isConnVacuumGauge = value;
                    OnPropertyChanged("IsConnVacuumGauge");
                }
            }
        }

        private bool _isConnLlcTmp;

        public bool IsConnLlcTmp
        {
            get { return _isConnLlcTmp; }
            set
            {
                if (_isConnLlcTmp != value)
                {
                    _isConnLlcTmp = value;
                    OnPropertyChanged("IsConnLlcTmp");
                }
            }
        }

        private bool _isConnMcTmp;

        public bool IsConnMcTmp
        {
            get { return _isConnMcTmp; }
            set
            {
                if (_isConnMcTmp != value)
                {
                    _isConnMcTmp = value;
                    OnPropertyChanged("IsConnMcTmp");
                }
            }
        }

        private bool _isConnAfm;

        public bool IsConnAfm
        {
            get { return _isConnAfm; }
            set
            {
                if (_isConnAfm != value)
                {
                    _isConnAfm = value;
                    OnPropertyChanged("IsConnAfm");
                }
            }
        }

        private bool _isConnRevolver;

        public bool IsConnRevolver
        {
            get { return _isConnRevolver; }
            set
            {
                if (_isConnRevolver != value)
                {
                    _isConnRevolver = value;
                    OnPropertyChanged("IsConnRevolver");
                }
            }
        }

        private bool _isConnXrayCamera;

        public bool IsConnXrayCamera
        {
            get { return _isConnXrayCamera; }
            set
            {
                if (_isConnXrayCamera != value)
                {
                    _isConnXrayCamera = value;
                    OnPropertyChanged("IsConnXrayCamera");
                }
            }
        }

        #region ** INotifyPropertyChanged **

        /// <summary>
        /// Excute When Property Changed
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged(String name)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(name)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

    public class MonitoringData : INotifyPropertyChanged
    {
        private bool _isSeqWork;

        public bool IsSeqWork
        {
            get { return _isSeqWork; }
            set
            {
                if (_isSeqWork != value)
                {
                    _isSeqWork = value;
                    OnPropertyChanged("IsSeqWork");
                }
            }
        }

        private bool _isSimulMode;

        public bool IsSimulMode
        {
            get { return _isSimulMode; }
            set
            {
                if (_isSimulMode != value)
                {
                    _isSimulMode = value;
                    OnPropertyChanged("IsSimulMode");
                }
            }
        }

        private bool _useFlipper;

        public bool UseFlipper
        {
            get { return _useFlipper; }
            set
            {
                if (_useFlipper != value)
                {
                    _useFlipper = value;
                    OnPropertyChanged("UseFlipper");
                }
            }
        }

        private bool _useRotator;

        public bool UseRotator
        {
            get { return _useRotator; }
            set
            {
                if (_useRotator != value)
                {
                    _useRotator = value;
                    OnPropertyChanged("UseRotator");
                }
            }
        }

        private bool _useBSTool;

        public bool UseBSTool
        {
            get { return _useBSTool; }
            set
            {
                if (_useBSTool != value)
                {
                    _useBSTool = value;
                    OnPropertyChanged("UseBSTool");
                }
            }
        }

        private Thickness _stageMargin;

        public Thickness StageMargin
        {
            get { return _stageMargin; }
            set
            {
                if (_stageMargin != value)
                {
                    _stageMargin = value;
                    OnPropertyChanged("StageMargin");
                }
            }
        }

        private Thickness _sensorMargin;

        public Thickness SensorMargin
        {
            get { return _sensorMargin; }
            set
            {
                if (_sensorMargin != value)
                {
                    _sensorMargin = value;
                    OnPropertyChanged("SensorMargin");
                }
            }
        }

        private Thickness _omMargin;

        public Thickness OmMargin
        {
            get { return _omMargin; }
            set
            {
                if (_omMargin != value)
                {
                    _omMargin = value;
                    OnPropertyChanged("OmMargin");
                }
            }
        }

        private Thickness _euvMargin;

        public Thickness EuvMargin
        {
            get { return _euvMargin; }
            set
            {
                if (_euvMargin != value)
                {
                    _euvMargin = value;
                    OnPropertyChanged("EuvMargin");
                }
            }
        }

        private double _stagePosX;

        public double StageUiPosX
        {
            get { return _stagePosX; }
            set
            {
                if (_stagePosX != value)
                {
                    _stagePosX = value;
                    OnPropertyChanged("StageUiPosX");
                }
            }
        }

        private double _stagePosY;

        public double StageUiPosY
        {
            get { return _stagePosY; }
            set
            {
                if (_stagePosY != value)
                {
                    _stagePosY = value;
                    OnPropertyChanged("StageUiPosY");
                }
            }
        }

        private double _tiltSensorPos;

        public double TiltSensorPos
        {
            get { return _tiltSensorPos; }
            set
            {
                if (_tiltSensorPos != value)
                {
                    _tiltSensorPos = value;
                    OnPropertyChanged("TiltSensorPos");
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////
        ///  TMP, DRY PUMP DATA
        ///////////////////////////////////////////////////////////////////////////////

        private int llcTmpState;

        public int LlcTmpState
        {
            get { return llcTmpState; }
            set
            {
                if (llcTmpState != value)
                {
                    llcTmpState = value;
                    OnPropertyChanged("LlcTmpState");
                }
            }
        }

        private bool _isLlcTmpError;

        public bool IsLlcTmpError
        {
            get { return _isLlcTmpError; }
            set
            {
                if (_isLlcTmpError != value)
                {
                    _isLlcTmpError = value;
                    OnPropertyChanged("IsLlcTmpError");
                }
            }
        }

        private int mcTmpState;

        public int McTmpState
        {
            get { return mcTmpState; }
            set
            {
                if (mcTmpState != value)
                {
                    mcTmpState = value;
                    OnPropertyChanged("McTmpState");
                }
            }
        }

        private bool _isMcTmpError;

        public bool IsMcTmpError
        {
            get { return _isMcTmpError; }
            set
            {
                if (_isMcTmpError != value)
                {
                    _isMcTmpError = value;
                    OnPropertyChanged("IsMcTmpError");
                }
            }
        }

        private bool _isSubTmpWork;

        public bool IsSubTmpWork
        {
            get { return _isSubTmpWork; }
            set
            {
                if (_isSubTmpWork != value)
                {
                    _isSubTmpWork = value;
                    OnPropertyChanged("IsSubTmpWork");
                }
            }
        }

        private bool _isMainTmpWork;

        public bool IsMainTmpWork
        {
            get { return _isMainTmpWork; }
            set
            {
                if (_isMainTmpWork != value)
                {
                    _isMainTmpWork = value;
                    OnPropertyChanged("IsMainTmpWork");
                }
            }
        }

        private bool _isLlcDryPumpWork;

        public bool IsLlcDryPumpWork
        {
            get { return _isLlcDryPumpWork; }
            set
            {
                if (_isLlcDryPumpWork != value)
                {
                    _isLlcDryPumpWork = value;
                    OnPropertyChanged("IsLlcDryPumpWork");
                }
            }
        }

        private bool _isLlcDryPumpAlarm;

        public bool IsLlcDryPumpAlarm
        {
            get { return _isLlcDryPumpAlarm; }
            set
            {
                if (_isLlcDryPumpAlarm != value)
                {
                    _isLlcDryPumpAlarm = value;
                    OnPropertyChanged("IsLlcDryPumpAlarm");
                }
            }
        }

        private bool _isLlcDryPumpWarning;

        public bool IsLlcDryPumpWarning
        {
            get { return _isLlcDryPumpWarning; }
            set
            {
                if (_isLlcDryPumpWarning != value)
                {
                    _isLlcDryPumpWarning = value;
                    OnPropertyChanged("IsLlcDryPumpWarning");
                }
            }
        }

        private bool _isMcDryPumpWork;

        public bool IsMcDryPumpWork
        {
            get { return _isMcDryPumpWork; }
            set
            {
                if (_isMcDryPumpWork != value)
                {
                    _isMcDryPumpWork = value;
                    OnPropertyChanged("IsMcDryPumpWork");
                }
            }
        }

        private bool _isMcDryPumpAlarm;

        public bool IsMcDryPumpAlarm
        {
            get { return _isMcDryPumpAlarm; }
            set
            {
                if (_isMcDryPumpAlarm != value)
                {
                    _isMcDryPumpAlarm = value;
                    OnPropertyChanged("IsMcDryPumpAlarm");
                }
            }
        }

        private bool _isMcDryPumpWarning;

        public bool IsMcDryPumpWarning
        {
            get { return _isMcDryPumpWarning; }
            set
            {
                if (_isMcDryPumpWarning != value)
                {
                    _isMcDryPumpWarning = value;
                    OnPropertyChanged("IsMcDryPumpWarning");
                }
            }
        }

        private bool _isSrcDryPumpWork;

        public bool IsSrcDryPumpWork
        {
            get { return _isSrcDryPumpWork; }
            set
            {
                if (_isSrcDryPumpWork != value)
                {
                    _isSrcDryPumpWork = value;
                    OnPropertyChanged("IsSrcDryPumpWork");
                }
            }
        }

        private bool _isSrcDryPumpAlarm;

        public bool IsSrcDryPumpAlarm
        {
            get { return _isSrcDryPumpAlarm; }
            set
            {
                if (_isSrcDryPumpAlarm != value)
                {
                    _isSrcDryPumpAlarm = value;
                    OnPropertyChanged("IsSrcDryPumpAlarm");
                }
            }
        }

        private bool _isSrcDryPumpWarning;

        public bool IsSrcDryPumpWarning
        {
            get { return _isSrcDryPumpWarning; }
            set
            {
                if (_isSrcDryPumpWarning != value)
                {
                    _isSrcDryPumpWarning = value;
                    OnPropertyChanged("IsSrcDryPumpWarning");
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////
        ///  MASK POSITION DATA
        ///////////////////////////////////////////////////////////////////////////////
        
        private bool _isMaskOnMtsRobot;

        public bool IsMaskOnMtsRobot
        {
            get { return _isMaskOnMtsRobot; }
            set
            {
                if (_isMaskOnMtsRobot != value)
                {
                    _isMaskOnMtsRobot = value;
                    OnPropertyChanged("IsMaskOnMtsRobot");
                }
            }
        }

        private bool _isMaskOnMtsFlipper;

        public bool IsMaskOnMtsFlipper
        {
            get { return _isMaskOnMtsFlipper; }
            set
            {
                if (_isMaskOnMtsFlipper != value)
                {
                    _isMaskOnMtsFlipper = value;
                    OnPropertyChanged("IsMaskOnMtsFlipper");
                }
            }
        }

        private bool _isMaskOnMtsRotator;

        public bool IsMaskOnMtsRotator
        {
            get { return _isMaskOnMtsRotator; }
            set
            {
                if (_isMaskOnMtsRotator != value)
                {
                    _isMaskOnMtsRotator = value;
                    OnPropertyChanged("IsMaskOnMtsRotator");
                }
            }
        }

        private bool _isMaskOnLLC;

        public bool IsMaskOnLLC
        {
            get { return _isMaskOnLLC; }
            set
            {
                if (_isMaskOnLLC != value)
                {
                    _isMaskOnLLC = value;
                    OnPropertyChanged("IsMaskOnLLC");
                }
            }
        }

        private bool _isMaskOnVmtr;

        public bool IsMaskOnVmtr
        {
            get { return _isMaskOnVmtr; }
            set
            {
                if (_isMaskOnVmtr != value)
                {
                    _isMaskOnVmtr = value;
                    OnPropertyChanged("IsMaskOnVmtr");
                }
            }
        }

        private bool _isMaskOnChuck;

        public bool IsMaskOnChuck
        {
            get { return _isMaskOnChuck; }
            set
            {
                if (_isMaskOnChuck != value)
                {
                    _isMaskOnChuck = value;
                    OnPropertyChanged("IsMaskOnChuck");
                }
            }
        }

        private bool _isLpmOpened;

        public bool IsLpmOpened
        {
            get { return _isLpmOpened; }
            set
            {
                if (_isLpmOpened != value)
                {
                    _isLpmOpened = value;
                    OnPropertyChanged("IsLpmOpened");
                }
            }
        }

        private bool _isLlcGateOpened;

        public bool IsLlcGateOpened
        {
            get { return _isLlcGateOpened; }
            set
            {
                if (_isLlcGateOpened != value)
                {
                    _isLlcGateOpened = value;
                    OnPropertyChanged("IsLlcGateOpened");
                }
            }
        }

        private bool _isTrGateOpened;

        public bool IsTrGateOpened
        {
            get { return _isTrGateOpened; }
            set
            {
                if (_isTrGateOpened != value)
                {
                    _isTrGateOpened = value;
                    OnPropertyChanged("IsTrGateOpened");
                }
            }
        }

        private bool _isLlcTmpGateOpened;

        public bool IsLlcTmpGateOpened
        {
            get { return _isLlcTmpGateOpened; }
            set
            {
                if (_isLlcTmpGateOpened != value)
                {
                    _isLlcTmpGateOpened = value;
                    OnPropertyChanged("IsLlcTmpGateOpened");
                }
            }
        }

        private bool _isMcTmpGateOpened;

        public bool IsMcTmpGateOpened
        {
            get { return _isMcTmpGateOpened; }
            set
            {
                if (_isMcTmpGateOpened != value)
                {
                    _isMcTmpGateOpened = value;
                    OnPropertyChanged("IsMcTmpGateOpened");
                }
            }
        }

        private bool _isLlcForelineOpened;

        public bool IsLlcForelineOpened
        {
            get { return _isLlcForelineOpened; }
            set
            {
                if (_isLlcForelineOpened != value)
                {
                    _isLlcForelineOpened = value;
                    OnPropertyChanged("IsLlcForelineOpened");
                }
            }
        }

        private bool _isLlcSlowRoughOpened;

        public bool IsLlcSlowRoughOpened
        {
            get { return _isLlcSlowRoughOpened; }
            set
            {
                if (_isLlcSlowRoughOpened != value)
                {
                    _isLlcSlowRoughOpened = value;
                    OnPropertyChanged("IsLlcSlowRoughOpened");
                }
            }
        }

        private bool _isLlcFastRoughOpened;

        public bool IsLlcFastRoughOpened
        {
            get { return _isLlcFastRoughOpened; }
            set
            {
                if (_isLlcFastRoughOpened != value)
                {
                    _isLlcFastRoughOpened = value;
                    OnPropertyChanged("IsLlcFastRoughOpened");
                }
            }
        }

        private bool _isMcForelineOpened;

        public bool IsMcForelineOpened
        {
            get { return _isMcForelineOpened; }
            set
            {
                if (_isMcForelineOpened != value)
                {
                    _isMcForelineOpened = value;
                    OnPropertyChanged("IsMcForelineOpened");
                }
            }
        }

        private bool _isMcSlowRoughOpened;

        public bool IsMcSlowRoughOpened
        {
            get { return _isMcSlowRoughOpened; }
            set
            {
                if (_isMcSlowRoughOpened != value)
                {
                    _isMcSlowRoughOpened = value;
                    OnPropertyChanged("IsMcSlowRoughOpened");
                }
            }
        }

        private bool _isMcFastRoughOpened;

        public bool IsMcFastRoughOpened
        {
            get { return _isMcFastRoughOpened; }
            set
            {
                if (_isMcFastRoughOpened != value)
                {
                    _isMcFastRoughOpened = value;
                    OnPropertyChanged("IsMcFastRoughOpened");
                }
            }
        }

        private double _llcVacuumRate;

        public double LlcVacuumRate
        {
            get { return _llcVacuumRate; }
            set
            {
                if (_llcVacuumRate != value)
                {
                    _llcVacuumRate = value;
                    OnPropertyChanged("LlcVacuumRate");
                }
            }
        }

        private double _mcVacuumRate;

        public double McVacuumRate
        {
            get { return _mcVacuumRate; }
            set
            {
                if (_mcVacuumRate != value)
                {
                    _mcVacuumRate = value;
                    OnPropertyChanged("McVacuumRate");
                }
            }
        }

        private bool _isPodOnLpm;

        public bool IsPodOnLpm
        {
            get { return _isPodOnLpm; }
            set
            {
                if (_isPodOnLpm != value)
                {
                    _isPodOnLpm = value;
                    OnPropertyChanged("IsPodOnLpm");
                }
            }
        }

        private bool _isMaskInPod;

        public bool IsMaskInPod
        {
            get { return _isMaskInPod; }
            set
            {
                if (_isMaskInPod != value)
                {
                    _isMaskInPod = value;
                    OnPropertyChanged("IsMaskInPod");
                }
            }
        }

        private bool _isLpmInit;

        public bool IsLpmInit
        {
            get { return _isLpmInit; }
            set
            {
                if (_isLpmInit != value)
                {
                    _isLpmInit = value;
                    OnPropertyChanged("IsLpmInit");
                }
            }
        }

        private bool _isLpmWorking;

        public bool IsLpmWorking
        {
            get { return _isLpmWorking; }
            set
            {
                if (_isLpmWorking != value)
                {
                    _isLpmWorking = value;
                    OnPropertyChanged("IsLpmWorking");
                }
            }
        }

        private bool _isLpmError;

        public bool IsLpmError
        {
            get { return _isLpmError; }
            set
            {
                if (_isLpmError != value)
                {
                    _isLpmError = value;
                    OnPropertyChanged("IsLpmError");
                }
            }
        }

        private bool _isMtsRobotInit;

        public bool IsMtsRobotInit
        {
            get { return _isMtsRobotInit; }
            set
            {
                if (_isMtsRobotInit != value)
                {
                    _isMtsRobotInit = value;
                    OnPropertyChanged("IsMtsRobotInit");
                }
            }
        }

        private bool _isMtsRobotWorking;

        public bool IsMtsRobotWorking
        {
            get { return _isMtsRobotWorking; }
            set
            {
                if (_isMtsRobotWorking != value)
                {
                    _isMtsRobotWorking = value;
                    OnPropertyChanged("IsMtsRobotWorking");
                }
            }
        }

        private bool _isMtsRobotError;

        public bool IsMtsRobotError
        {
            get { return _isMtsRobotError; }
            set
            {
                if (_isMtsRobotError != value)
                {
                    _isMtsRobotError = value;
                    OnPropertyChanged("IsMtsRobotError");
                }
            }
        }

        private bool _isMtsRobotExtended;

        public bool IsMtsRobotExtended
        {
            get { return _isMtsRobotExtended; }
            set
            {
                if (_isMtsRobotExtended != value)
                {
                    _isMtsRobotExtended = value;
                    OnPropertyChanged("IsMtsRobotExtended");
                }
            }
        }

        private bool _isMtsRobotRetracted;

        public bool IsMtsRobotRetracted
        {
            get { return _isMtsRobotRetracted; }
            set
            {
                if (_isMtsRobotRetracted != value)
                {
                    _isMtsRobotRetracted = value;
                    OnPropertyChanged("IsMtsRobotRetracted");
                }
            }
        }

        private bool _isFlipperInit;

        public bool IsFlipperInit
        {
            get { return _isFlipperInit; }
            set
            {
                if (_isFlipperInit != value)
                {
                    _isFlipperInit = value;
                    OnPropertyChanged("IsFlipperInit");
                }
            }
        }

        private bool _isFlipperWorking;

        public bool IsFlipperWorking
        {
            get { return _isFlipperWorking; }
            set
            {
                if (_isFlipperWorking != value)
                {
                    _isFlipperWorking = value;
                    OnPropertyChanged("IsFlipperWorking");
                }
            }
        }

        private bool _isFlipperError;

        public bool IsFlipperError
        {
            get { return _isFlipperError; }
            set
            {
                if (_isFlipperError != value)
                {
                    _isFlipperError = value;
                    OnPropertyChanged("IsFlipperError");
                }
            }
        }

        private bool _isRotatorInit;

        public bool IsRotatorInit
        {
            get { return _isRotatorInit; }
            set
            {
                if (_isRotatorInit != value)
                {
                    _isRotatorInit = value;
                    OnPropertyChanged("IsRotatorInit");
                }
            }
        }

        private bool _isRotatorWorking;

        public bool IsRotatorWorking
        {
            get { return _isRotatorWorking; }
            set
            {
                if (_isRotatorWorking != value)
                {
                    _isRotatorWorking = value;
                    OnPropertyChanged("IsRotatorWorking");
                }
            }
        }

        private bool _isRotatorError;

        public bool IsRotatorError
        {
            get { return _isRotatorError; }
            set
            {
                if (_isRotatorError != value)
                {
                    _isRotatorError = value;
                    OnPropertyChanged("IsRotatorError");
                }
            }
        }

        private bool _isVmtrServoOn;

        public bool IsVmtrServoOn
        {
            get { return _isVmtrServoOn; }
            set
            {
                if (_isVmtrServoOn != value)
                {
                    _isVmtrServoOn = value;
                    OnPropertyChanged("IsVmtrServoOn");
                }
            }
        }

        private bool _isVmtrInit;

        public bool IsVmtrInit
        {
            get { return _isVmtrInit; }
            set
            {
                if (_isVmtrInit != value)
                {
                    _isVmtrInit = value;
                    OnPropertyChanged("IsVmtrInit");
                }
            }
        }

        private bool _isVmtrWorking;

        public bool IsVmtrWorking
        {
            get { return _isVmtrWorking; }
            set
            {
                if (_isVmtrWorking != value)
                {
                    _isVmtrWorking = value;
                    OnPropertyChanged("IsVmtrWorking");
                }
            }
        }

        private bool _isVmtrError;

        public bool IsVmtrError
        {
            get { return _isVmtrError; }
            set
            {
                if (_isVmtrError != value)
                {
                    _isVmtrError = value;
                    OnPropertyChanged("IsVmtrError");
                }
            }
        }

        private bool _isVmtrEmo;

        public bool IsVmtrEmo
        {
            get { return _isVmtrEmo; }
            set
            {
                if (_isVmtrEmo != value)
                {
                    _isVmtrEmo = value;
                    OnPropertyChanged("IsVmtrEmo");
                }
            }
        }

        private bool _isVmtrRobotExtended;

        public bool IsVmtrRobotExtended
        {
            get { return _isVmtrRobotExtended; }
            set
            {
                if (_isVmtrRobotExtended != value)
                {
                    _isVmtrRobotExtended = value;
                    OnPropertyChanged("IsVmtrRobotExtended");
                }
            }
        }

        private bool _isVmtrRobotRetracted;

        public bool IsVmtrRobotRetracted
        {
            get { return _isVmtrRobotRetracted; }
            set
            {
                if (_isVmtrRobotRetracted != value)
                {
                    _isVmtrRobotRetracted = value;
                    OnPropertyChanged("IsVmtrRobotRetracted");
                }
            }
        }

        private double _vmtrPosT;

        public double VmtrPosT
        {
            get { return _vmtrPosT; }
            set
            {
                if (_vmtrPosT != value)
                {
                    _vmtrPosT = value;
                    OnPropertyChanged("VmtrPosT");
                }
            }
        }

        private double _vmtrPosL;

        public double VmtrPosL
        {
            get { return _vmtrPosL; }
            set
            {
                if (_vmtrPosL != value)
                {
                    _vmtrPosL = value;
                    OnPropertyChanged("VmtrPosL");
                }
            }
        }

        private double _vmtrPosZ;

        public double VmtrPosZ
        {
            get { return _vmtrPosZ; }
            set
            {
                if (_vmtrPosZ != value)
                {
                    _vmtrPosZ = value;
                    OnPropertyChanged("VmtrPosZ");
                }
            }
        }

        private bool _isNaviLoadingPos;

        public bool IsNaviLoadingPos
        {
            get { return _isNaviLoadingPos; }
            set
            {
                if (_isNaviLoadingPos != value)
                {
                    _isNaviLoadingPos = value;
                    OnPropertyChanged("IsNaviLoadingPos");
                }
            }
        }

        private double _naviPosX;

        public double NaviPosX
        {
            get { return _naviPosX; }
            set
            {
                if (_naviPosX != value)
                {
                    _naviPosX = value;
                    OnPropertyChanged("NaviPosX");
                }
            }
        }

        private double _naviPosY;

        public double NaviPosY
        {
            get { return _naviPosY; }
            set
            {
                if (_naviPosY != value)
                {
                    _naviPosY = value;
                    OnPropertyChanged("NaviPosY");
                }
            }
        }

        private string _naviRealPosX;

        public string NaviRealPosX
        {
            get { return _naviRealPosX; }
            set
            {
                if (_naviRealPosX != value)
                {
                    _naviRealPosX = value;
                    OnPropertyChanged("NaviRealPosX");
                }
            }
        }

        private string _naviRealPosY;

        public string NaviRealPosY
        {
            get { return _naviRealPosY; }
            set
            {
                if (_naviRealPosY != value)
                {
                    _naviRealPosY = value;
                    OnPropertyChanged("NaviRealPosY");
                }
            }
        }

        private int _mtsRobotAngle = -270;

        public int MtsRobotAngle
        {
            get { return _mtsRobotAngle; }
            set
            {
                if (_mtsRobotAngle != value)
                {
                    _mtsRobotAngle = value;
                    OnPropertyChanged("MtsRobotAngle");
                }
            }
        }

        private int _vmtrRobotAngle = -90;

        public int VmtrRobotAngle
        {
            get { return _vmtrRobotAngle; }
            set
            {
                if (_vmtrRobotAngle != value)
                {
                    _vmtrRobotAngle = value;
                    OnPropertyChanged("VmtrRobotAngle");
                }
            }
        }

        private string _mcVacuum;

        public string McVacuum
        {
            get { return _mcVacuum; }
            set
            {
                if (_mcVacuum != value)
                {
                    _mcVacuum = value;
                    OnPropertyChanged("McVacuum");
                }
            }
        }

        private string _llcVacuum;

        public string LlcVacuum
        {
            get { return _llcVacuum; }
            set
            {
                if (_llcVacuum != value)
                {
                    _llcVacuum = value;
                    OnPropertyChanged("LlcVacuum");
                }
            }
        }

        private bool _isSlowVentValveOpened;

        public bool IsSlowVentValveOpened
        {
            get { return _isSlowVentValveOpened; }
            set
            {
                if (_isSlowVentValveOpened != value)
                {
                    _isSlowVentValveOpened = value;
                    OnPropertyChanged("IsSlowVentValveOpened");
                }
            }
        }

        private bool _isSlowInletValveOpened;

        public bool IsSlowInletValveOpened
        {
            get { return _isSlowInletValveOpened; }
            set
            {
                if (_isSlowInletValveOpened != value)
                {
                    _isSlowInletValveOpened = value;
                    OnPropertyChanged("IsSlowInletValveOpened");
                }
            }
        }

        private bool _isSlowOutletValveOpened;

        public bool IsSlowOutletValveOpened
        {
            get { return _isSlowOutletValveOpened; }
            set
            {
                if (_isSlowOutletValveOpened != value)
                {
                    _isSlowOutletValveOpened = value;
                    OnPropertyChanged("IsSlowOutletValveOpened");
                }
            }
        }

        private bool _isFastVentValveOpened;

        public bool IsFastVentValveOpened
        {
            get { return _isFastVentValveOpened; }
            set
            {
                if (_isFastVentValveOpened != value)
                {
                    _isFastVentValveOpened = value;
                    OnPropertyChanged("IsFastVentValveOpened");
                }
            }
        }

        private bool _isFastInletValveOpened;

        public bool IsFastInletValveOpened
        {
            get { return _isFastInletValveOpened; }
            set
            {
                if (_isFastInletValveOpened != value)
                {
                    _isFastInletValveOpened = value;
                    OnPropertyChanged("IsFastInletValveOpened");
                }
            }
        }

        private bool _isFastOutletValveOpened;

        public bool IsFastOutletValveOpened
        {
            get { return _isFastOutletValveOpened; }
            set
            {
                if (_isFastOutletValveOpened != value)
                {
                    _isFastOutletValveOpened = value;
                    OnPropertyChanged("IsFastOutletValveOpened");
                }
            }
        }

        private string _mfc1N2Voltage;

        public string Mfc1N2Voltage
        {
            get { return _mfc1N2Voltage; }
            set
            {
                if (_mfc1N2Voltage != value)
                {
                    _mfc1N2Voltage = value;
                    OnPropertyChanged("Mfc1N2Voltage");
                }
            }
        }

        private string _mfc2N2Voltage;

        public string Mfc2N2Voltage
        {
            get { return _mfc2N2Voltage; }
            set
            {
                if (_mfc2N2Voltage != value)
                {
                    _mfc2N2Voltage = value;
                    OnPropertyChanged("Mfc2N2Voltage");
                }
            }
        }

        private bool _isWaterSupplyValveOpened;

        public bool IsWaterSupplyValveOpened
        {
            get { return _isWaterSupplyValveOpened; }
            set
            {
                if (_isWaterSupplyValveOpened != value)
                {
                    _isWaterSupplyValveOpened = value;
                    OnPropertyChanged("IsWaterSupplyValveOpened");
                }
            }
        }

        private bool _isWaterReturnValveOpened;

        public bool IsWaterReturnValveOpened
        {
            get { return _isWaterReturnValveOpened; }
            set
            {
                if (_isWaterReturnValveOpened != value)
                {
                    _isWaterReturnValveOpened = value;
                    OnPropertyChanged("IsWaterReturnValveOpened");
                }
            }
        }

        private bool _isWaterLeakLlcTmp;

        public bool IsWaterLeakLlcTmp
        {
            get { return _isWaterLeakLlcTmp; }
            set
            {
                if (_isWaterLeakLlcTmp != value)
                {
                    _isWaterLeakLlcTmp = value;
                    OnPropertyChanged("IsWaterLeakLlcTmp");
                }
            }
        }

        private bool _isWaterLeakMcTmp;

        public bool IsWaterLeakMcTmp
        {
            get { return _isWaterLeakMcTmp; }
            set
            {
                if (_isWaterLeakMcTmp != value)
                {
                    _isWaterLeakMcTmp = value;
                    OnPropertyChanged("IsWaterLeakMcTmp");
                }
            }
        }

        private bool _isMainAir;

        public bool IsMainAir
        {
            get { return _isMainAir; }
            set
            {
                if (_isMainAir != value)
                {
                    _isMainAir = value;
                    OnPropertyChanged("IsMainAir");
                }
            }
        }

        private bool _isMainWater;

        public bool IsMainWater
        {
            get { return _isMainWater; }
            set
            {
                if (_isMainWater != value)
                {
                    _isMainWater = value;
                    OnPropertyChanged("IsMainWater");
                }
            }
        }

        private bool _isSmokeDetectCb;

        public bool IsSmokeDetectCb
        {
            get { return _isSmokeDetectCb; }
            set
            {
                if (_isSmokeDetectCb != value)
                {
                    _isSmokeDetectCb = value;
                    OnPropertyChanged("IsSmokeDetectCb");
                }
            }
        }

        private bool _isSmokeDetectVacSt;

        public bool IsSmokeDetectVacSt
        {
            get { return _isSmokeDetectVacSt; }
            set
            {
                if (_isSmokeDetectVacSt != value)
                {
                    _isSmokeDetectVacSt = value;
                    OnPropertyChanged("IsSmokeDetectVacSt");
                }
            }
        }

        private bool _isAlarmWaterReturnTemp;

        public bool IsAlarmWaterReturnTemp
        {
            get { return _isAlarmWaterReturnTemp; }
            set
            {
                if (_isAlarmWaterReturnTemp != value)
                {
                    _isAlarmWaterReturnTemp = value;
                    OnPropertyChanged("IsAlarmWaterReturnTemp");
                }
            }
        }

        private bool _isAvailableSrcGateOpened;

        public bool IsAvailableSrcGateOpened
        {
            get { return _isAvailableSrcGateOpened; }
            set
            {
                if (_isAvailableSrcGateOpened != value)
                {
                    _isAvailableSrcGateOpened = value;
                    OnPropertyChanged("IsAvailableSrcGateOpened");
                }
            }
        }

        public int _maskLocation;

        public int MaskLocation
        {
            get { return _maskLocation; }
            set
            {
                if (_maskLocation != value)
                {
                    _maskLocation = value;
                    OnPropertyChanged("MaskLocation");
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////
        ///  SOURCE DATA
        ///////////////////////////////////////////////////////////////////////////////

        private string _srcMcVacuum;

        public string SrcMcVacuum
        {
            get { return _srcMcVacuum; }
            set
            {
                if (_srcMcVacuum != value)
                {
                    _srcMcVacuum = value;
                    OnPropertyChanged("SrcMcVacuum");
                }
            }
        }

        private string _srcScVacuum;

        public string SrcScVacuum
        {
            get { return _srcScVacuum; }
            set
            {
                if (_srcScVacuum != value)
                {
                    _srcScVacuum = value;
                    OnPropertyChanged("SrcScVacuum");
                }
            }
        }

        private string _srcBcVacuum;

        public string SrcBcVacuum
        {
            get { return _srcBcVacuum; }
            set
            {
                if (_srcBcVacuum != value)
                {
                    _srcBcVacuum = value;
                    OnPropertyChanged("SrcBcVacuum");
                }
            }
        }

        private bool _isSrcEuvLaserSol;

        public bool IsSrcEuvLaserSol
        {
            get { return _isSrcEuvLaserSol; }
            set
            {
                if (_isSrcEuvLaserSol != value)
                {
                    _isSrcEuvLaserSol = value;
                    OnPropertyChanged("IsSrcEuvLaserSol");
                }
            }
        }

        private bool _isSrcEuvOn;

        public bool IsSrcEuvOn
        {
            get { return _isSrcEuvOn; }
            set
            {
                if (_isSrcEuvOn != value)
                {
                    _isSrcEuvOn = value;
                    OnPropertyChanged("IsSrcEuvOn");
                }
            }
        }

        private bool _isSrcMechShutterOpened;

        public bool IsSrcMechShutterOpened
        {
            get { return _isSrcMechShutterOpened; }
            set
            {
                if (_isSrcMechShutterOpened != value)
                {
                    _isSrcMechShutterOpened = value;
                    OnPropertyChanged("IsSrcMechShutterOpened");
                }
            }
        }

        private string _srcMirrorStagePos;

        public string SrcMirrorStagePos
        {
            get { return _srcMirrorStagePos; }
            set
            {
                if (_srcMirrorStagePos != value)
                {
                    _srcMirrorStagePos = value;
                    OnPropertyChanged("SrcMirrorStagePos");
                }
            }
        }

        private bool _isMcMaskTiltError1;

        public bool IsMcMaskTiltError1
        {
            get { return _isMcMaskTiltError1; }
            set
            {
                if (_isMcMaskTiltError1 != value)
                {
                    _isMcMaskTiltError1 = value;
                    OnPropertyChanged("IsMcMaskTiltError1");
                }
            }
        }

        private bool _isMcMaskTiltError2;

        public bool IsMcMaskTiltError2
        {
            get { return _isMcMaskTiltError2; }
            set
            {
                if (_isMcMaskTiltError2 != value)
                {
                    _isMcMaskTiltError2 = value;
                    OnPropertyChanged("IsMcMaskTiltError2");
                }
            }
        }

        private bool _isLlcMaskTiltError1;

        public bool IsLlcMaskTiltError1
        {
            get { return _isLlcMaskTiltError1; }
            set
            {
                if (_isLlcMaskTiltError1 != value)
                {
                    _isLlcMaskTiltError1 = value;
                    OnPropertyChanged("IsLlcMaskTiltError1");
                }
            }
        }

        private bool _isLlcMaskTiltError2;

        public bool IsLlcMaskTiltError2
        {
            get { return _isLlcMaskTiltError2; }
            set
            {
                if (_isLlcMaskTiltError2 != value)
                {
                    _isLlcMaskTiltError2 = value;
                    OnPropertyChanged("IsLlcMaskTiltError2");
                }
            }
        }

        private double _euvTotalSeconds;

        public double EuvTotalSeconds
        {
            get { return _euvTotalSeconds; }
            set
            {
                if (_euvTotalSeconds != value)
                {
                    _euvTotalSeconds = value;
                    OnPropertyChanged("EuvTotalSeconds");
                }
            }
        }

        private double _euvTimeAfterReset;

        public double EuvTimeAfterReset
        {
            get { return _euvTimeAfterReset; }
            set
            {
                if (_euvTimeAfterReset != value)
                {
                    _euvTimeAfterReset = value;
                    OnPropertyChanged("EuvTimeAfterReset");
                }
            }
        }

        private string _euvResetDate;

        public string EuvResetDate
        {
            get { return _euvResetDate; }
            set
            {
                if (_euvResetDate != value)
                {
                    _euvResetDate = value;
                    OnPropertyChanged("EuvResetDate");
                }
            }
        }

        private string _euvStartDate;

        public string EuvStartDate
        {
            get { return _euvStartDate; }
            set
            {
                if (_euvStartDate != value)
                {
                    _euvStartDate = value;
                    OnPropertyChanged("EuvStartDate");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _isMaskInPodTemp;

        public bool IsMaskInPodTemp
        {
            get { return _isMaskInPodTemp; }
            set
            {
                if (_isMaskInPodTemp != value)
                {
                    _isMaskInPodTemp = value;
                    OnPropertyChanged("IsMaskInPodTemp");
                }
            }
        }

        private bool _isMaskOnMtsRobotTemp;

        public bool IsMaskOnMtsRobotTemp
        {
            get { return _isMaskOnMtsRobotTemp; }
            set
            {
                if (_isMaskOnMtsRobotTemp != value)
                {
                    _isMaskOnMtsRobotTemp = value;
                    OnPropertyChanged("IsMaskOnMtsRobotTemp");
                }
            }
        }

        private bool _isMaskOnMtsFlipperTemp;

        public bool IsMaskOnMtsFlipperTemp
        {
            get { return _isMaskOnMtsFlipperTemp; }
            set
            {
                if (_isMaskOnMtsFlipperTemp != value)
                {
                    _isMaskOnMtsFlipperTemp = value;
                    OnPropertyChanged("IsMaskOnMtsFlipperTemp");
                }
            }
        }

        private bool _isMaskOnMtsRotatorTemp;

        public bool IsMaskOnMtsRotatorTemp
        {
            get { return _isMaskOnMtsRotatorTemp; }
            set
            {
                if (_isMaskOnMtsRotatorTemp != value)
                {
                    _isMaskOnMtsRotatorTemp = value;
                    OnPropertyChanged("IsMaskOnMtsRotatorTemp");
                }
            }
        }

        private bool _isMaskOnLLCTemp;

        public bool IsMaskOnLLCTemp
        {
            get { return _isMaskOnLLCTemp; }
            set
            {
                if (_isMaskOnLLCTemp != value)
                {
                    _isMaskOnLLCTemp = value;
                    OnPropertyChanged("IsMaskOnLLCTemp");
                }
            }
        }

        private bool _isMaskOnVmtrTemp;

        public bool IsMaskOnVmtrTemp
        {
            get { return _isMaskOnVmtrTemp; }
            set
            {
                if (_isMaskOnVmtrTemp != value)
                {
                    _isMaskOnVmtrTemp = value;
                    OnPropertyChanged("IsMaskOnVmtrTemp");
                }
            }
        }

        private bool _isMaskOnChuckTemp;

        public bool IsMaskOnChuckTemp
        {
            get { return _isMaskOnChuckTemp; }
            set
            {
                if (_isMaskOnChuckTemp != value)
                {
                    _isMaskOnChuckTemp = value;
                    OnPropertyChanged("IsMaskOnChuckTemp");
                }
            }
        }

        #region ** INotifyPropertyChanged **

        /// <summary>
        /// Excute When Property Changed
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged(String name)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(name)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;

#endregion
    }
}
