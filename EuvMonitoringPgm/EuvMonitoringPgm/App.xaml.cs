﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using CommonLib;

namespace EuvMonitoringPgm
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : Application
    {
        public static ISplashScreen iSplash;
        private ManualResetEvent ResetSplashCreated;
        private Thread SplashThread;
        private MainWindow main = null;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            #region ** 프로그램 중복 실행 체크 **

            Process currentProcess = Process.GetCurrentProcess();
            var runningProcess = (from process in Process.GetProcesses()
                                  where process.Id != currentProcess.Id && process.ProcessName.Equals(currentProcess.ProcessName, StringComparison.Ordinal)
                                  select process).FirstOrDefault();


            if (runningProcess != null)
            {
                MessageBox.Show("This program is already running now.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                ExitApplication();
                return;
            }

            #endregion

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            main = new MainWindow();
            
            #region ** Show Splash Dialog **

            ResetSplashCreated = new ManualResetEvent(false);
            SplashThread = new Thread(ShowSplash);
            SplashThread.SetApartmentState(ApartmentState.STA);
            SplashThread.IsBackground = true;
            SplashThread.Name = "Splash Screen";
            SplashThread.Start();
            ResetSplashCreated.WaitOne();

            #endregion

            App.iSplash.AddMessage("Initialized main window.", Brushes.Green);
            App.iSplash.AddMessage("Starting Socket Server.", Brushes.Green);
            Thread.Sleep(100);

            if(main.Socket.SetRun(UiCommon.Knd.Var.IpAddress, UiCommon.Knd.Var.Port))
            {
                App.iSplash.AddMessage("Open Socket Server Success.", Brushes.Green);
            }
            else
            {
                App.iSplash.AddMessage("Open Socket Server Fail.", Brushes.OrangeRed);
            }

            App.iSplash.AddMessage("Loading Complete.", Brushes.Green);
            Thread.Sleep(2000);
            App.iSplash.LoadComplete();

            //main.ShowDialog();
            main.ni.Visible = true;
        }

        private void ShowSplash()
        {
            SplashScreenWindow splash = new SplashScreenWindow();
            iSplash = splash;
            splash.Show();
            ResetSplashCreated.Set();
            System.Windows.Threading.Dispatcher.Run();
        }

        /// <summary>
        /// UI Dispatcher Unhandled Exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDispatcher_UnhandledException(Object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Error : " + e.Exception.Message, "Unhandled Exception Caught", MessageBoxButton.OK, MessageBoxImage.Error);

            DateTime dt = DateTime.Now;
            StreamWriter sw = null;
            Exception ex = null;

            string sFilePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Exception");

            try
            {
                DirectoryInfo di = new DirectoryInfo(sFilePath);

                if (!di.Exists) di.Create();

                string sDate = string.Format("Error_{0:D4}{1:D2}{2:D2}_{3:D2}{4:D2}{5:D2}_UI_Dispatcher.txt", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);

                sw = new StreamWriter(Path.Combine(sFilePath, sDate));
                ex = (Exception)e.Exception;

                sw.WriteLine("[ Error ]");
                sw.WriteLine(ex.Message + Environment.NewLine);
                sw.WriteLine("[ Detail ]");
                sw.WriteLine(ex.StackTrace);
                sw.Close();
            }
            finally
            {
                if (sw != null) sw.Close();

                ExitApplication();
            }

            e.Handled = true;
        }

        /// <summary>
        /// Unhandled Exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Error : " + (e.ExceptionObject as Exception).Message, "Unhandled Exception Caught", MessageBoxButton.OK, MessageBoxImage.Error);

            DateTime dt = DateTime.Now;
            StreamWriter sw = null;
            Exception ex = null;

            string sFilePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Exception");

            try
            {
                DirectoryInfo di = new DirectoryInfo(sFilePath);

                if (!di.Exists) di.Create();

                string sDate = string.Format("Error_{0:D4}{1:D2}{2:D2}_{3:D2}{4:D2}{5:D2}_AppDomain.txt", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);

                sw = new StreamWriter(Path.Combine(sFilePath, sDate));
                ex = e.ExceptionObject as Exception;

                sw.WriteLine("[ Error ]");
                sw.WriteLine(ex.Message + Environment.NewLine);
                sw.WriteLine("[ Detail ]");
                sw.WriteLine(ex.StackTrace);
                sw.Close();
            }
            finally
            {
                if (sw != null) sw.Close();

                ExitApplication();
            }
        }

        /// <summary>
        /// Exit Application
        /// </summary>
        private void ExitApplication()
        {
            ThreadStart ts = delegate ()
            {
                Dispatcher.BeginInvoke((Action)delegate ()
                {
                    if(main != null)
                    {
                        main.ni.Dispose();
                    }

                    Application.Current.Shutdown();
                });
            };

            Thread t = new Thread(ts);
            t.Start();
        }
    }
}
