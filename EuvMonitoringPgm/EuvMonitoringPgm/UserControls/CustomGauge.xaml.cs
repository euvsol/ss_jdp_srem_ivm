﻿using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace EuvMonitoringPgm.UserControls
{
    /// <summary>
    /// CustomGauge.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CustomGauge : UserControl
    {
        double m_dCurrValue = -90;

        string sMinValue = string.Empty;
        string sMaxValue = string.Empty;

        public CustomGauge()
        {
            InitializeComponent();
        }

        public double Value
        {
            get { return m_dCurrValue; }
            set
            {
                Storyboard sb = this.Resources["sbCurrPointer"] as Storyboard;

                if (sb != null)
                {
                    ((sb.Children[0] as DoubleAnimationUsingKeyFrames).KeyFrames[0] as EasingDoubleKeyFrame).Value = GetVacuumValueToProgress(value);
                    sb.Begin();
                }
            }
        }

        public string Min
        {
            get { return sMinValue; }
            set { lblMin.Content = value; }
        }

        public string Max
        {
            get { return sMaxValue; }
            set { lblMax.Content = value; }
        }

        #region ** Convert vacuum level to progress value **

        readonly double[] dbHigh = { 9.99E-7, 9.99E-6, 9.99E-5, 9.9E-4, 9.99E-3, 9.99E-2, 9.99E-1, 9.99E+0, 9.9E+1, 7.49E+2 };
        readonly double[] dbLow  = { 1.0E-7, 1.0E-6, 1.0E-5, 1.0E-4, 1.0E-3, 1.0E-2, 1.0E-1, 1.0E+0, 1.0E+1, 1.0E+2 };
        readonly double[] dbUnit = { 1.0E+7, 1.0E+6, 1.0E+5, 1.0E+4, 1.0E+3, 1.0E+2, 1.0E+1, 1.0E+0, 1.0E-1, 1.0E-2 };

        private double GetVacuumValueToProgress(double dVacuumValue)
        {
            double dbValue = 0.0;

            for (int nIdx = 0; nIdx < dbHigh.Length; nIdx++)
            {
                if (dVacuumValue <= dbHigh[nIdx] && dVacuumValue >= dbLow[nIdx])
                {
                    dbValue = ((double)nIdx * 10) + (dVacuumValue * dbUnit[nIdx]);

                    if (dbValue >= 70)
                        dbValue = (dbValue * 1.8) - 90;
                    else
                        dbValue = -90 + (dbValue * 1.8);

                    break;
                }
                else if (dVacuumValue == 0.0)
                {
                    dbValue = 36.0; // 진공 0 Torr
                    break;
                }
                else if (dVacuumValue > dbHigh[dbHigh.Length - 1])
                {
                    dbValue = 90;
                    break;
                }
                else if (dVacuumValue < dbLow[0])
                {
                    dbValue = -90;
                    break;
                }
            }

            return dbValue * -1;
        }

        #endregion
    }
}
