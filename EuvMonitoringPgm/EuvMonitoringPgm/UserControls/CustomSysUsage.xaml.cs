﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace EuvMonitoringPgm.UserControls
{
    /// <summary>
    /// CustomSysUsage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CustomSysUsage : UserControl
    {
        public CustomSysUsage()
        {
            InitializeComponent();
        }

        #region ** Title DependencyProperty **
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register(
            "Title",
            typeof(string),
            typeof(CustomSysUsage),
            new PropertyMetadata("Not Set"));

        public string Title
        {
            get { return (string)this.GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        #endregion

        #region ** Value DependencyProperty **
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
            "Value",
            typeof(int),
            typeof(CustomSysUsage),
            new PropertyMetadata(0));

        public int Value
        {
            get { return (int)this.GetValue(ValueProperty); }
            set { this.SetValue(ValueProperty, value); }
        }
        #endregion
    }
}
