﻿using System;
using System.ComponentModel;
using System.Windows;

namespace EuvMonitoringPgm.UserControls
{
    /// <summary>
    /// CustomMessageBox.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CustomMessageBox : Window
    {
        public enum MsgType
        {
            Question = 0,
            Alert
        }

        DialogData binding = new DialogData();

        public CustomMessageBox(MsgType _type, String _message)
        {
            InitializeComponent();

            DataContext = binding;

            Window main = Application.Current.MainWindow;
            this.Width  = main.Width;
            this.Height = main.Height;

            if (main.WindowState == WindowState.Maximized)
            {
                this.Left = 0;
                this.Top  = 0;
            }
            else
            {
                this.Left = main.Left;
                this.Top  = main.Top;
            }

            Type = (int)_type;
            Message = _message;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        #region ** Variables **
        public string Message
        {
            get { return binding.Message; }
            set { binding.Message = value; }
        }

        public int Type
        {
            get { return binding.Type; }
            set { binding.Type = value; }
        }
        #endregion
    }

    #region ** Binding Data **
    class DialogData : INotifyPropertyChanged
    {
        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                if (_message != value)
                {
                    _message = value;

                    OnPropertyChanged("Message");
                }
            }
        }

        private int _type;
        public int Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (_type != value)
                {
                    _type = value;

                    OnPropertyChanged("Type");
                }
            }
        }

        #region INotifyPropertyChanged
        /// <summary>
        /// Excute When Property Changed
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged(String name)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(name)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
    #endregion
}
