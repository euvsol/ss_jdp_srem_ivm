﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using EuvMonitoringPgm.Data;

namespace EuvMonitoringPgm.View
{
    /// <summary>
    /// ViewMain.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ViewMain : UserControl
    {
        int m_nMtsStation;
        int m_nVmtrStation;

        bool m_bFirstRun = false;

        #region ** Declare Storyboard for Robot Moving **

        Storyboard m_sbMtsRotator = null;
        Storyboard m_sbMtsFlipper = null;

        Storyboard m_sbMtsRobotAngle = null;
        Storyboard m_sbVacRobotAngle = null;

        Storyboard m_sbMtsRobotExPickLpm     = null;
        Storyboard m_sbMtsRobotExPickRotator = null;
        Storyboard m_sbMtsRobotExPickFlipper = null;
        Storyboard m_sbMtsRobotExPickLlc     = null;
        Storyboard m_sbVacRobotExPickLlc     = null;
        Storyboard m_sbVacRobotExPickMc      = null;

        Storyboard m_sbMtsRobotExPlaceLpm     = null;
        Storyboard m_sbMtsRobotExPlaceRotator = null;
        Storyboard m_sbMtsRobotExPlaceFlipper = null;
        Storyboard m_sbMtsRobotExPlaceLlc     = null;
        Storyboard m_sbVacRobotExPlaceLlc     = null;
        Storyboard m_sbVacRobotExPlaceMc      = null;

        Storyboard m_sbMtsRobotRePickLpm     = null;
        Storyboard m_sbMtsRobotRePickRotator = null;
        Storyboard m_sbMtsRobotRePickFlipper = null;
        Storyboard m_sbMtsRobotRePickLlc     = null;
        Storyboard m_sbVacRobotRePickLlc     = null;
        Storyboard m_sbVacRobotRePickMc      = null;

        Storyboard m_sbMtsRobotRePlaceLpm     = null;
        Storyboard m_sbMtsRobotRePlaceRotator = null;
        Storyboard m_sbMtsRobotRePlaceFlipper = null;
        Storyboard m_sbMtsRobotRePlaceLlc     = null;
        Storyboard m_sbVacRobotRePlaceLlc     = null;
        Storyboard m_sbVacRobotRePlaceMc      = null;

        #endregion

        public ViewMain()
        {
            InitializeComponent();

            Thickness StageMargin = new Thickness(0, 0, UiCommon.Knd.Data.EQUIP.StageUiPosX, UiCommon.Knd.Data.EQUIP.StageUiPosY);
            Thickness SensorMargin = new Thickness(0, 0, UiCommon.Knd.Data.EQUIP.TiltSensorPos, 0);

            m_sbMtsRobotAngle = (Storyboard)Resources["sbMtsRobotAngle"];
            m_sbVacRobotAngle = (Storyboard)Resources["sbVmtrRobotAngle"];

            #region ** Mts Complete Event **

            m_sbMtsRotator = chkRotator.Template.Resources["sbDoAlign"] as Storyboard;
            if (m_sbMtsRotator.IsSealed) m_sbMtsRotator = m_sbMtsRotator.Clone();
            m_sbMtsRotator.Completed += new EventHandler(MyEventMtsRotator_Completed);

            m_sbMtsFlipper = chkFlipper.Template.Resources["sbDoFlip"] as Storyboard;
            if (m_sbMtsFlipper.IsSealed) m_sbMtsFlipper = m_sbMtsFlipper.Clone();
            m_sbMtsFlipper.Completed += new EventHandler(MyEventMtsFlipper_Completed);

            m_sbMtsRobotExPickLpm = chkMtsRobot.Template.Resources["sbExPickLpm"] as Storyboard;
            if (m_sbMtsRobotExPickLpm.IsSealed) m_sbMtsRobotExPickLpm = m_sbMtsRobotExPickLpm.Clone();
            m_sbMtsRobotExPickLpm.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPickRotator = chkMtsRobot.Template.Resources["sbExPickRotator"] as Storyboard;
            if (m_sbMtsRobotExPickRotator.IsSealed) m_sbMtsRobotExPickRotator = m_sbMtsRobotExPickRotator.Clone();
            m_sbMtsRobotExPickRotator.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPickFlipper = chkMtsRobot.Template.Resources["sbExPickFlipper"] as Storyboard;
            if (m_sbMtsRobotExPickFlipper.IsSealed) m_sbMtsRobotExPickFlipper = m_sbMtsRobotExPickFlipper.Clone();
            m_sbMtsRobotExPickFlipper.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPickLlc = chkMtsRobot.Template.Resources["sbExPickLlc"] as Storyboard;
            if (m_sbMtsRobotExPickLlc.IsSealed) m_sbMtsRobotExPickLlc = m_sbMtsRobotExPickLlc.Clone();
            m_sbMtsRobotExPickLlc.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPlaceLpm = chkMtsRobot.Template.Resources["sbExPlaceLpm"] as Storyboard;
            if (m_sbMtsRobotExPlaceLpm.IsSealed) m_sbMtsRobotExPlaceLpm = m_sbMtsRobotExPlaceLpm.Clone();
            m_sbMtsRobotExPlaceLpm.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPlaceRotator = chkMtsRobot.Template.Resources["sbExPlaceRotator"] as Storyboard;
            if (m_sbMtsRobotExPlaceRotator.IsSealed) m_sbMtsRobotExPlaceRotator = m_sbMtsRobotExPlaceRotator.Clone();
            m_sbMtsRobotExPlaceRotator.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPlaceFlipper = chkMtsRobot.Template.Resources["sbExPlaceFlipper"] as Storyboard;
            if (m_sbMtsRobotExPlaceFlipper.IsSealed) m_sbMtsRobotExPlaceFlipper = m_sbMtsRobotExPlaceFlipper.Clone();
            m_sbMtsRobotExPlaceFlipper.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);

            m_sbMtsRobotExPlaceLlc = chkMtsRobot.Template.Resources["sbExPlaceLlc"] as Storyboard;
            if (m_sbMtsRobotExPlaceLlc.IsSealed) m_sbMtsRobotExPlaceLlc = m_sbMtsRobotExPlaceLlc.Clone();
            m_sbMtsRobotExPlaceLlc.Completed += new EventHandler(MyEventMtsRobotExtend_Completed);



            m_sbMtsRobotRePickLpm = chkMtsRobot.Template.Resources["sbRePickLpm"] as Storyboard;
            if (m_sbMtsRobotRePickLpm.IsSealed) m_sbMtsRobotRePickLpm = m_sbMtsRobotRePickLpm.Clone();
            m_sbMtsRobotRePickLpm.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePickRotator = chkMtsRobot.Template.Resources["sbRePickRotator"] as Storyboard;
            if (m_sbMtsRobotRePickRotator.IsSealed) m_sbMtsRobotRePickRotator = m_sbMtsRobotRePickRotator.Clone();
            m_sbMtsRobotRePickRotator.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePickFlipper = chkMtsRobot.Template.Resources["sbRePickFlipper"] as Storyboard;
            if (m_sbMtsRobotRePickFlipper.IsSealed) m_sbMtsRobotRePickFlipper = m_sbMtsRobotRePickFlipper.Clone();
            m_sbMtsRobotRePickFlipper.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePickLlc = chkMtsRobot.Template.Resources["sbRePickLlc"] as Storyboard;
            if (m_sbMtsRobotRePickLlc.IsSealed) m_sbMtsRobotRePickLlc = m_sbMtsRobotRePickLlc.Clone();
            m_sbMtsRobotRePickLlc.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePlaceLpm = chkMtsRobot.Template.Resources["sbRePlaceLpm"] as Storyboard;
            if (m_sbMtsRobotRePlaceLpm.IsSealed) m_sbMtsRobotRePlaceLpm = m_sbMtsRobotRePlaceLpm.Clone();
            m_sbMtsRobotRePlaceLpm.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePlaceRotator = chkMtsRobot.Template.Resources["sbRePlaceRotator"] as Storyboard;
            if (m_sbMtsRobotRePlaceRotator.IsSealed) m_sbMtsRobotRePlaceRotator = m_sbMtsRobotRePlaceRotator.Clone();
            m_sbMtsRobotRePlaceRotator.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePlaceFlipper = chkMtsRobot.Template.Resources["sbRePlaceFlipper"] as Storyboard;
            if (m_sbMtsRobotRePlaceFlipper.IsSealed) m_sbMtsRobotRePlaceFlipper = m_sbMtsRobotRePlaceFlipper.Clone();
            m_sbMtsRobotRePlaceFlipper.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            m_sbMtsRobotRePlaceLlc = chkMtsRobot.Template.Resources["sbRePlaceLlc"] as Storyboard;
            if (m_sbMtsRobotRePlaceLlc.IsSealed) m_sbMtsRobotRePlaceLlc = m_sbMtsRobotRePlaceLlc.Clone();
            m_sbMtsRobotRePlaceLlc.Completed += new EventHandler(MyEventMtsRobotRetract_Completed);

            #endregion

            #region ** Vac Robot Complete Event **

            m_sbVacRobotExPickLlc = chkVacRobot.Template.Resources["sbExPickLlc"] as Storyboard;
            if (m_sbVacRobotExPickLlc.IsSealed) m_sbVacRobotExPickLlc = m_sbVacRobotExPickLlc.Clone();
            m_sbVacRobotExPickLlc.Completed += new EventHandler(MyEventVacRobotExtend_Completed);
            
            m_sbVacRobotExPickMc = chkVacRobot.Template.Resources["sbExPickMc"] as Storyboard;
            if (m_sbVacRobotExPickMc.IsSealed) m_sbVacRobotExPickMc = m_sbVacRobotExPickMc.Clone();
            m_sbVacRobotExPickMc.Completed += new EventHandler(MyEventVacRobotExtend_Completed);
            
            m_sbVacRobotExPlaceLlc = chkVacRobot.Template.Resources["sbExPlaceLlc"] as Storyboard;
            if (m_sbVacRobotExPlaceLlc.IsSealed) m_sbVacRobotExPlaceLlc = m_sbVacRobotExPlaceLlc.Clone();
            m_sbVacRobotExPlaceLlc.Completed += new EventHandler(MyEventVacRobotExtend_Completed);
            
            m_sbVacRobotExPlaceMc = chkVacRobot.Template.Resources["sbExPlaceMc"] as Storyboard;
            if (m_sbVacRobotExPlaceMc.IsSealed) m_sbVacRobotExPlaceMc = m_sbVacRobotExPlaceMc.Clone();
            m_sbVacRobotExPlaceMc.Completed += new EventHandler(MyEventVacRobotExtend_Completed);


            m_sbVacRobotRePickLlc = chkVacRobot.Template.Resources["sbRePickLlc"] as Storyboard;
            if (m_sbVacRobotRePickLlc.IsSealed) m_sbVacRobotRePickLlc = m_sbVacRobotRePickLlc.Clone();
            m_sbVacRobotRePickLlc.Completed += new EventHandler(MyEventVacRobotRetract_Completed);

            m_sbVacRobotRePickMc = chkVacRobot.Template.Resources["sbRePickMc"] as Storyboard;
            if (m_sbVacRobotRePickMc.IsSealed) m_sbVacRobotRePickMc = m_sbVacRobotRePickMc.Clone();
            m_sbVacRobotRePickMc.Completed += new EventHandler(MyEventVacRobotRetract_Completed);

            m_sbVacRobotRePlaceLlc = chkVacRobot.Template.Resources["sbRePlaceLlc"] as Storyboard;
            if (m_sbVacRobotRePlaceLlc.IsSealed) m_sbVacRobotRePlaceLlc = m_sbVacRobotRePlaceLlc.Clone();
            m_sbVacRobotRePlaceLlc.Completed += new EventHandler(MyEventVacRobotRetract_Completed);

            m_sbVacRobotRePlaceMc = chkVacRobot.Template.Resources["sbRePlaceMc"] as Storyboard;
            if (m_sbVacRobotRePlaceMc.IsSealed) m_sbVacRobotRePlaceMc = m_sbVacRobotRePlaceMc.Clone();
            m_sbVacRobotRePlaceMc.Completed += new EventHandler(MyEventVacRobotRetract_Completed);

            #endregion

            DataContext = UiCommon.Knd.Data.EQUIP;

            SetMaterial("MASK");
            //Sim_MoveNavigationStage(309.048, 138.956);
        }

        #region ** Storyboard Completed Event **

        private void MyEventVacRobotRetract_Completed(object sender, EventArgs e)
        {
            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }
        }

        private void MyEventMtsRobotRetract_Completed(object sender, EventArgs e)
        {
            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }
        }

        private void MyEventMtsRotator_Completed(object sender, EventArgs e)
        {
            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }
        }

        private void MyEventMtsFlipper_Completed(object sender, EventArgs e)
        {
            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }
        }

        private void MyEventMtsRobotRotate_Completed(object sender, EventArgs e)
        {
            switch (m_nMtsStation)
            {
                case (int)MtsRobotSt.READY_LLC:
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        if (waitforsinglesignal != null)
                            waitforsinglesignal.Set();
                    }
                    break;
                case (int)MtsRobotSt.PICK_LPM:
                    if (m_sbMtsRobotExPickLpm != null)
                        m_sbMtsRobotExPickLpm.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_LPM:
                    if (m_sbMtsRobotExPlaceLpm != null)
                        m_sbMtsRobotExPlaceLpm.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PICK_LLC:
                    if (m_sbMtsRobotExPickLlc != null)
                        m_sbMtsRobotExPickLlc.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_LLC:
                    if (m_sbMtsRobotExPlaceLlc != null)
                        m_sbMtsRobotExPlaceLlc.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PICK_FLIPPER:
                    if (m_sbMtsRobotExPickFlipper != null)
                        m_sbMtsRobotExPickFlipper.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_FLIPPER:
                    if (m_sbMtsRobotExPlaceFlipper != null)
                        m_sbMtsRobotExPlaceFlipper.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PICK_ROTATOR:
                    if (m_sbMtsRobotExPickRotator != null)
                        m_sbMtsRobotExPickRotator.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_ROTATOR:
                    if (m_sbMtsRobotExPlaceRotator != null)
                        m_sbMtsRobotExPlaceRotator.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                default:
                    break;
            }
        }

        private void MyEventVmtrRobotRotate_Completed(object sender, EventArgs e)
        {
            switch (m_nVmtrStation)
            {
                case (int)VmtrRobotSt.PICK_LLC:
                    if (m_sbVacRobotExPickLlc != null)
                    {
                        if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                        {
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = true;
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = false;
                        }

                        m_sbVacRobotExPickLlc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    }
                    break;
                case (int)VmtrRobotSt.PICK_MC:
                    if (m_sbVacRobotExPickMc != null)
                    {
                        if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                        {
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = true;
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = false;
                        }

                        m_sbVacRobotExPickMc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    }
                    break;
                case (int)VmtrRobotSt.PLACE_LLC:
                    if (m_sbVacRobotExPlaceLlc != null)
                    {
                        if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                        {
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = true;
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = false;
                        }

                        m_sbVacRobotExPlaceLlc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    }
                    break;
                case (int)VmtrRobotSt.PLACE_MC:
                    if (m_sbVacRobotExPlaceMc != null)
                    {
                        if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                        {
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = true;
                            UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = false;
                        }

                        m_sbVacRobotExPlaceMc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    }
                    break;
                default:
                    break;
            }
        }

        private void MyEventMtsLpmLoad_Completed(object sender, EventArgs e)
        {
            //20220721 jkseo, 실제 로딩동작 보다 Animation 동작이 빨리 끝나서 대기
            while(true)
            {
                Thread.Sleep(100);
                if (!UiCommon.Knd.Data.EQUIP.IsLpmWorking)
                    break;
            }

            if(UiCommon.Knd.Data.EQUIP.IsMaskInPod)
                UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = true;

            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }
        }

        private void MyEventMtsLpmUnload_Completed(object sender, EventArgs e)
        {
            UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = false;

            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }
        }

        private async void MyEventMtsRobotExtend_Completed(object sender, EventArgs e)
        {
            //MTS Robot 마스크 Pick을 위한 Extend 완료 후 Retract 동작을 위한 이벤트
            await PutTaskDelay(2);

            switch (m_nMtsStation)
            {
                case (int)MtsRobotSt.PICK_LPM:
                    UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = true;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskInPod = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePickLpm != null)
                        m_sbMtsRobotRePickLpm.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_LPM:
                    UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = true;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = false;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskInPod = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePlaceLpm != null)
                        m_sbMtsRobotRePlaceLpm.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PICK_LLC:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = true;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnLLC = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePickLlc != null)
                        m_sbMtsRobotRePickLlc.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_LLC:
                    await PutTaskDelay(1);
                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = true;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = false;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnLLC = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePlaceLlc != null)
                        m_sbMtsRobotRePlaceLlc.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PICK_FLIPPER:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipperTemp = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = true;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipper = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePickFlipper != null)
                        m_sbMtsRobotRePickFlipper.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_FLIPPER:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipperTemp = true;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = false;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipper = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePlaceFlipper != null)
                        m_sbMtsRobotRePlaceFlipper.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PICK_ROTATOR:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotatorTemp = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = true;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotator = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePickRotator != null)
                        m_sbMtsRobotRePickRotator.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                case (int)MtsRobotSt.PLACE_ROTATOR:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotatorTemp = true;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = false;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobot = false;
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotator = true;
                    }
                    await PutTaskDelay(2);
                    if (m_sbMtsRobotRePlaceRotator != null)
                        m_sbMtsRobotRePlaceRotator.Begin(chkMtsRobot, chkMtsRobot.Template, true);
                    break;
                default:
                    break;
            }
        }

        private async void MyEventVacRobotExtend_Completed(object sender, EventArgs e)
        {
            //VAC Robot 마스크 Pick을 위한 Extend 완료 후 Retract 동작을 위한 이벤트
            await PutTaskDelay(5);

            switch (m_nVmtrStation)
            {
                case (int)VmtrRobotSt.PICK_LLC:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = true;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnLLC = false;
                    }
                    await PutTaskDelay(3);
                    if(m_sbVacRobotRePickLlc != null)
                        m_sbVacRobotRePickLlc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    break;
                case (int)VmtrRobotSt.PLACE_LLC:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = true;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = false;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnLLC = true;
                    }
                    await PutTaskDelay(3);
                    if (m_sbVacRobotRePlaceLlc != null)
                        m_sbVacRobotRePlaceLlc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    break;
                case (int)VmtrRobotSt.PICK_MC:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = true;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnChuck = false;
                    }
                    await PutTaskDelay(5);
                    if (m_sbVacRobotRePickMc != null)
                        m_sbVacRobotRePickMc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    break;
                case (int)VmtrRobotSt.PLACE_MC:
                    UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = true;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = false;
                    if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
                    {
                        UiCommon.Knd.Data.EQUIP.IsMaskOnChuck = true;
                    }
                    await PutTaskDelay(5);
                    if (m_sbVacRobotRePlaceMc != null)
                        m_sbVacRobotRePlaceMc.Begin(chkVacRobot, chkVacRobot.Template, true);
                    break;
                default:
                    break;
            }
        }

        async Task PutTaskDelay(int sec)
        {
            await Task.Delay(sec * 1000);
        }

        #endregion

        public void UpdateEquipmentStatus()
        {
            if (!UiCommon.Knd.Data.EQUIP.IsPodOnLpm)
                UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = false;

#if false   //20220721 jkseo, 불필요한 코드
            if(UiCommon.Knd.Data.EQUIP.IsNaviLoadingPos)
            {
                UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = UiCommon.Knd.Data.EQUIP.IsMaskOnChuck;
            }
            else
            {
              if (UiCommon.Knd.Data.EQUIP.MaskLocation == (int)MaterialLoc.CHUCK)
                  UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = true;
              else
                  UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = false;
            }
#endif

#if true   //Navigation Stage Moving Animation
            double dTempNaviPosX, dTempNaviPosY;
            if(!double.TryParse(UiCommon.Knd.Data.EQUIP.NaviRealPosX, out dTempNaviPosX))
                dTempNaviPosX = 0.0;
            if (!double.TryParse(UiCommon.Knd.Data.EQUIP.NaviRealPosY, out dTempNaviPosY))
                dTempNaviPosY = 0.0;
            
            if (dTempNaviPosX < 0) dTempNaviPosX = 0.0;
            if (dTempNaviPosY < 0) dTempNaviPosY = 0.0;
            
            UiCommon.Knd.Data.EQUIP.NaviPosX = (dTempNaviPosX / UiCommon.Knd.Var.NaviRealRangeX) * UiCommon.Knd.Var.NaviUIRangeX;
            UiCommon.Knd.Data.EQUIP.NaviPosY = (dTempNaviPosY / UiCommon.Knd.Var.NaviRealRangeY) * UiCommon.Knd.Var.NaviUIRangeY;
#endif

            if (!m_bFirstRun/* && UiCommon.Knd.Data.EQUIP.MaskLocation != -1*/)
            {
                m_bFirstRun = true;

                if(UiCommon.Knd.Data.EQUIP.IsLpmOpened) LoadLPModule();

                switch (UiCommon.Knd.Data.EQUIP.MaskLocation)
                {
                    case (int)MaterialLoc.MTS_POD:
                        UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = true;
                        break;
                    case (int)MaterialLoc.MTS_ROBOT:
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = true;
                        break;
                    case (int)MaterialLoc.MTS_ROTATOR:
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotatorTemp = true;
                        break;
                    case (int)MaterialLoc.MTS_FLIPPER:
                        UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipperTemp = true;
                        break;
                    case (int)MaterialLoc.LLC:
                        UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = true;
                        break;
                    case (int)MaterialLoc.VAC_ROBOT:
                        UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = true;
                        break;
                    case (int)MaterialLoc.CHUCK:
                        UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = true;
                        break;
                }
            }
        }

        public void SetMaterial(string sMaterial)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                switch (sMaterial)
                {
                    case "MASK":
                        chkMaskOnLpm.Tag = "Mask";
                        chkRotator.Tag = "Mask";
                        chkFlipper.Tag = "Mask";
                        chkMaskOnLlc.Tag = "Mask";
                        chkMaskOnChuck.Tag = "Mask";
                        chkMtsRobot.Tag = "Mask";
                        chkVacRobot.Tag = "Mask";
                        break;
                    case "COUPON":
                        chkMaskOnLpm.Tag = "Coupon";
                        chkRotator.Tag = "Coupon";
                        chkFlipper.Tag = "Coupon";
                        chkMaskOnLlc.Tag = "Coupon";
                        chkMaskOnChuck.Tag = "Coupon";
                        chkMtsRobot.Tag = "Coupon";
                        chkVacRobot.Tag = "Coupon";
                        break;
                    case "FULL":
                        chkMaskOnLpm.Tag = "Full";
                        chkRotator.Tag = "Full";
                        chkFlipper.Tag = "Full";
                        chkMaskOnLlc.Tag = "Full";
                        chkMaskOnChuck.Tag = "Full";
                        chkMtsRobot.Tag = "Full";
                        chkVacRobot.Tag = "Full";
                        break;
                    default:
                        chkMaskOnLpm.Tag = "Default";
                        chkRotator.Tag = "Default";
                        chkFlipper.Tag = "Default";
                        chkMaskOnLlc.Tag = "Default";
                        chkMaskOnChuck.Tag = "Default";
                        chkMtsRobot.Tag = "Default";
                        chkVacRobot.Tag = "Default";
                        break;
                }
            });
        }

        public void LoadLPModule()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                Storyboard sb = this.Resources["sbMtsLpmLoad"] as Storyboard;

                if (sb != null) sb.Begin();
            });
        }

        public void UnloadLPModule()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                Storyboard sb = this.Resources["sbMtsLpmUnload"] as Storyboard;

                if (sb != null) sb.Begin();
            });
        }

        public void RotateMaterial()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                m_sbMtsRotator.Begin(chkRotator, chkRotator.Template, true);
            });
        }

        public void FlipMaterial()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                m_sbMtsFlipper.Begin(chkFlipper, chkFlipper.Template, true);
            });
        }

        public void MoveMtsRobot(int nStation)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                var anim = (DoubleAnimationUsingKeyFrames)m_sbMtsRobotAngle.Children[0];
                anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(5);

                m_nMtsStation = nStation;

                switch ((MtsRobotSt)nStation)
                {
                    case MtsRobotSt.HOME:
                        if (UiCommon.Knd.Data.EQUIP.MtsRobotAngle == (int)MtsRobotDir.HOME)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.MtsRobotAngle = (int)MtsRobotDir.HOME;
                        break;
                    case MtsRobotSt.PICK_LPM:
                    case MtsRobotSt.PLACE_LPM:
                        if (UiCommon.Knd.Data.EQUIP.MtsRobotAngle == (int)MtsRobotDir.LPM)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.MtsRobotAngle = (int)MtsRobotDir.LPM;
                        break;
                    case MtsRobotSt.PICK_LLC:
                    case MtsRobotSt.PLACE_LLC:
                    case MtsRobotSt.READY_LLC:
                        if (UiCommon.Knd.Data.EQUIP.MtsRobotAngle == (int)MtsRobotDir.LLC)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.MtsRobotAngle = (int)MtsRobotDir.LLC;
                        break;
                    case MtsRobotSt.PICK_FLIPPER:
                    case MtsRobotSt.PLACE_FLIPPER:
                    case MtsRobotSt.PICK_ROTATOR:
                    case MtsRobotSt.PLACE_ROTATOR:
                        if (UiCommon.Knd.Data.EQUIP.MtsRobotAngle == (int)MtsRobotDir.UTIL)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.MtsRobotAngle = (int)MtsRobotDir.UTIL;
                        break;
                }

                if (m_sbMtsRobotAngle != null)
                    m_sbMtsRobotAngle.Begin(chkMtsRobot, true);
            });
        }

        public void MoveVmtrRobot(int nStation)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                var anim = (DoubleAnimationUsingKeyFrames)m_sbVacRobotAngle.Children[0];
                anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(6);

                m_nVmtrStation = nStation;

                switch ((VmtrRobotSt)nStation)
                {
                    case VmtrRobotSt.HOME:
                        if (UiCommon.Knd.Data.EQUIP.VmtrRobotAngle == (int)VmtrRobotDir.HOME)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.VmtrRobotAngle = (int)MtsRobotDir.HOME;
                        break;
                    case VmtrRobotSt.PICK_LLC:
                    case VmtrRobotSt.PLACE_LLC:
                    case VmtrRobotSt.READY_LLC:
                        if (UiCommon.Knd.Data.EQUIP.VmtrRobotAngle == (int)VmtrRobotDir.LLC)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.VmtrRobotAngle = (int)VmtrRobotDir.LLC;
                        break;
                    case VmtrRobotSt.PICK_MC:
                    case VmtrRobotSt.PLACE_MC:
                    case VmtrRobotSt.READY_MC:
                        if (UiCommon.Knd.Data.EQUIP.VmtrRobotAngle == (int)VmtrRobotDir.MC)
                            anim.KeyFrames[0].KeyTime = TimeSpan.FromSeconds(1);
                        anim.KeyFrames[0].Value = UiCommon.Knd.Data.EQUIP.VmtrRobotAngle = (int)VmtrRobotDir.MC;
                        break;
                }

                if(m_sbVacRobotAngle != null)
                    m_sbVacRobotAngle.Begin(chkVacRobot, true);
            });
        }

        public void PauseAtrRobot()
        {
            m_sbMtsRobotAngle.Pause(chkMtsRobot);

            m_sbMtsRobotExPickLpm.Pause(chkMtsRobot);
            m_sbMtsRobotRePickLpm.Pause(chkMtsRobot);
            m_sbMtsRobotExPlaceLpm.Pause(chkMtsRobot);
            m_sbMtsRobotRePlaceLpm.Pause(chkMtsRobot);

            m_sbMtsRobotExPickLlc.Pause(chkMtsRobot);
            m_sbMtsRobotRePickLlc.Pause(chkMtsRobot);
            m_sbMtsRobotExPlaceLlc.Pause(chkMtsRobot);
            m_sbMtsRobotRePlaceLlc.Pause(chkMtsRobot);

            m_sbMtsRobotExPickFlipper.Pause(chkMtsRobot);
            m_sbMtsRobotRePickFlipper.Pause(chkMtsRobot);
            m_sbMtsRobotExPlaceFlipper.Pause(chkMtsRobot);
            m_sbMtsRobotRePlaceFlipper.Pause(chkMtsRobot);

            m_sbMtsRobotExPickRotator.Pause(chkMtsRobot);
            m_sbMtsRobotRePickRotator.Pause(chkMtsRobot);
            m_sbMtsRobotExPlaceRotator.Pause(chkMtsRobot);
            m_sbMtsRobotRePlaceRotator.Pause(chkMtsRobot);
        }

        public void ResetAtrRobot()
        {
            //Angle Stop시 회전까지 원복 ㅠㅠ;
            //m_sbMtsRobotAngle.Stop(chkMtsRobot);

            m_sbMtsRobotExPickLpm.Stop(chkMtsRobot);
            m_sbMtsRobotRePickLpm.Stop(chkMtsRobot);
            m_sbMtsRobotExPlaceLpm.Stop(chkMtsRobot);
            m_sbMtsRobotRePlaceLpm.Stop(chkMtsRobot);

            m_sbMtsRobotExPickLlc.Stop(chkMtsRobot);
            m_sbMtsRobotRePickLlc.Stop(chkMtsRobot);
            m_sbMtsRobotExPlaceLlc.Stop(chkMtsRobot);
            m_sbMtsRobotRePlaceLlc.Stop(chkMtsRobot);

            m_sbMtsRobotExPickFlipper.Stop(chkMtsRobot);
            m_sbMtsRobotRePickFlipper.Stop(chkMtsRobot);
            m_sbMtsRobotExPlaceFlipper.Stop(chkMtsRobot);
            m_sbMtsRobotRePlaceFlipper.Stop(chkMtsRobot);

            m_sbMtsRobotExPickRotator.Stop(chkMtsRobot);
            m_sbMtsRobotRePickRotator.Stop(chkMtsRobot);
            m_sbMtsRobotExPlaceRotator.Stop(chkMtsRobot);
            m_sbMtsRobotRePlaceRotator.Stop(chkMtsRobot);
        }

        public void PauseVmtrRobot()
        {
            m_sbVacRobotAngle.Pause(chkVacRobot);

            m_sbVacRobotExPickLlc.Pause(chkVacRobot);
            m_sbVacRobotRePickLlc.Pause(chkVacRobot);
            m_sbVacRobotExPlaceLlc.Pause(chkVacRobot);
            m_sbVacRobotRePlaceLlc.Pause(chkVacRobot);

            m_sbVacRobotExPickMc.Pause(chkVacRobot);
            m_sbVacRobotRePickMc.Pause(chkVacRobot);
            m_sbVacRobotExPlaceMc.Pause(chkVacRobot);
            m_sbVacRobotRePlaceMc.Pause(chkVacRobot);
        }

        public void ResetVmtrRobot()
        {
            //Angle Stop시 회전까지 원복 ㅠㅠ;
            //m_sbVacRobotAngle.Stop(chkVacRobot);

            m_sbVacRobotExPickLlc.Stop(chkVacRobot);
            m_sbVacRobotRePickLlc.Stop(chkVacRobot);
            m_sbVacRobotExPlaceLlc.Stop(chkVacRobot);
            m_sbVacRobotRePlaceLlc.Stop(chkVacRobot);
            
            m_sbVacRobotExPickMc.Stop(chkVacRobot);
            m_sbVacRobotRePickMc.Stop(chkVacRobot);
            m_sbVacRobotExPlaceMc.Stop(chkVacRobot);
            m_sbVacRobotRePlaceMc.Stop(chkVacRobot);
        }

        private void AddMask_Clicked(object sender, RoutedEventArgs e)
        {
            string sName = ((MenuItem)sender).Name;

            switch (sName)
            {
                case "AddLpm":
                    UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = true;
                    break;
                case "AddMtsRobot":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = true;
                    break;
                case "AddRotator":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotatorTemp = true;
                    break;
                case "AddFlipper":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipperTemp = true;
                    break;
                case "AddLlc":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = true;
                    break;
                case "AddVacRobot":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = true;
                    break;
                case "AddChuck":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = true;
                    break;
            }
        }

        private void DelMask_Clicked(object sender, RoutedEventArgs e)
        {
            string sName = ((MenuItem)sender).Name;

            switch (sName)
            {
                case "DelLpm":
                    UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = false;
                    break;
                case "DelMtsRobot":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRobotTemp = false;
                    break;
                case "DelRotator":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsRotatorTemp = false;
                    break;
                case "DelFlipper":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnMtsFlipperTemp = false;
                    break;
                case "DelLlc":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnLLCTemp = false;
                    break;
                case "DelVacRobot":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtrTemp = false;
                    break;
                case "DelChuck":
                    UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp = false;
                    break;
            }
        }

        //////////////////////////////////////////////////////////////
        //  Simulation Test
        //////////////////////////////////////////////////////////////
        Thread m_thSequence = null;
        Thread m_thVenting = null;
        Thread m_thPumping = null;

        EventWaitHandle waitforsinglesignal;
        EventWaitHandle WaitForVentPumpComplete;
        bool m_bLoading = true;
        bool m_bSeqThreadStop = true;
        SeqProcess emSeqProcess;

        private void BtnLoadingTest_Click(object sender, RoutedEventArgs e)
        {
            if (!UiCommon.Knd.Data.EQUIP.IsSimulMode) return;

            if (m_thSequence != null)
            {
                MessageBox.Show("이미 다른 작업이 수행 중이므로 동작할 수 없습니다.");
                return;
            }

            m_bLoading = true;

            emSeqProcess = SeqProcess.OnMtsPodLoading;
            waitforsinglesignal = new EventWaitHandle(false, EventResetMode.AutoReset);
            WaitForVentPumpComplete = new EventWaitHandle(false, EventResetMode.AutoReset);
            m_thSequence = new Thread(new ThreadStart(DoWork));
            m_thSequence.Start();
            
        }

        private void BtnUnloadingTest_Click(object sender, RoutedEventArgs e)
        {
            if (!UiCommon.Knd.Data.EQUIP.IsSimulMode) return;

            if (m_thSequence != null)
            {
                MessageBox.Show("이미 다른 작업이 수행 중이므로 동작할 수 없습니다.");
                return;
            }

            m_bLoading = false;

            emSeqProcess = SeqProcess.OnChuck;
            waitforsinglesignal = new EventWaitHandle(false, EventResetMode.AutoReset);
            WaitForVentPumpComplete = new EventWaitHandle(false, EventResetMode.AutoReset);
            m_thSequence = new Thread(new ThreadStart(DoWork));
            m_thSequence.Start();
        }
        
        private void DoWork()
        {
            m_bSeqThreadStop = false;

            if (m_bLoading)
            {
                if (UiCommon.Knd.Data.EQUIP.IsMaskOnChuckTemp)
                {
                    MessageBox.Show("Mask가 Chuck에 있습니다.");
                    m_bSeqThreadStop = true;
                    m_thSequence = null; 
                    return;
                }
            }
            else
            {

            }

            //mask 위치에 따라 리커버리

            while (!m_bSeqThreadStop)
            {


                SeqAutoLoop();
            }

            string sTemp;
            if (m_bLoading)
                sTemp = "Loading이 완료 되었습니다.";
            else
                sTemp = "Unloading이 완료 되었습니다.";

            MessageBox.Show(sTemp);
            m_thSequence = null;
           
        }
        
        bool m_bUseWaitEvent = false;
        private void SeqAutoLoop()
        {
            switch(emSeqProcess)
            {
                case SeqProcess.OnMtsPodLoading:
                    if(UiCommon.Knd.Data.EQUIP.IsNaviLoadingPos != true)
                        Sim_MoveNavigationStage(345.75, 128.05);

                    //LLC VENTING
                    m_bUseWaitEvent = false;
                    m_thVenting = new Thread(new ThreadStart(DoVent));
                    m_thVenting.Start();

                    UiCommon.Knd.Data.EQUIP.IsPodOnLpm = true;

                    if (UiCommon.Knd.Data.EQUIP.IsLpmOpened != true)
                    {
                        UiCommon.Knd.Data.EQUIP.IsLpmWorking = true;
                        LoadLPModule();
                        waitforsinglesignal.WaitOne();
                        UiCommon.Knd.Data.EQUIP.IsLpmWorking = false;
                        UiCommon.Knd.Data.EQUIP.IsLpmOpened = true;
                    }

                    UiCommon.Knd.Data.EQUIP.IsMaskInPod = UiCommon.Knd.Data.EQUIP.IsMaskInPodTemp = true;
                    emSeqProcess = SeqProcess.OnAtrHandLoading;
                    break;
                case SeqProcess.OnAtrHandLoading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PICK_LPM);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnRotatorLoading;
                    break;
#if false
                case SeqProcess.OnFlipperLoading:
                    emSeqProcess = SeqProcess.OnFlipperAfterFlipLoading;
                    break;
                case SeqProcess.OnFlipperAfterFlipLoading:
                    emSeqProcess = SeqProcess.OnAtrAfterFlipLoading;
                    break;
                case SeqProcess.OnAtrAfterFlipLoading:
                    emSeqProcess = SeqProcess.OnRotatorLoading;
                    break;
#endif
                case SeqProcess.OnRotatorLoading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PLACE_ROTATOR);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnRotatorAfterRotateLoading;
                    break;
                case SeqProcess.OnRotatorAfterRotateLoading:
                    Thread.Sleep(1000);
                    UiCommon.Knd.Data.EQUIP.IsRotatorWorking = true;
                    RotateMaterial();
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsRotatorWorking = false;
                    emSeqProcess = SeqProcess.OnAtrAfterRotateLoading;
                    break;
                case SeqProcess.OnAtrAfterRotateLoading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PICK_ROTATOR);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;

                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.READY_LLC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnLlcLoading;
                    break;
                case SeqProcess.OnLlcLoading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PLACE_LLC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnLlcVacuumLoading;
                    break;
                case SeqProcess.OnLlcVacuumLoading:
                    //LLC PUMPING
                    Thread.Sleep(2000);
                    m_bUseWaitEvent = true;
                    m_thPumping = new Thread(new ThreadStart(DoPump));
                    m_thPumping.Start();
                    waitforsinglesignal.WaitOne();
                    emSeqProcess = SeqProcess.OnVmtrHandLoading;
                    break;
                case SeqProcess.OnVmtrHandLoading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = true;
                    MoveVmtrRobot((int)VmtrRobotSt.PICK_LLC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = false;
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = true;
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtr = true;
                    emSeqProcess = SeqProcess.OnChuckLoading;
                    break;
                case SeqProcess.OnChuckLoading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = true;
                    MoveVmtrRobot((int)VmtrRobotSt.PLACE_MC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = false;
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = true;
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtr = false;
                    emSeqProcess = SeqProcess.LoadingComplete;
                    break;
                case SeqProcess.LoadingComplete:
                    m_bSeqThreadStop = true;
                    break;
                case SeqProcess.OnChuck:
                    if (UiCommon.Knd.Data.EQUIP.IsNaviLoadingPos != true)
                        Sim_MoveNavigationStage(350, 139.8);

                    if(UiCommon.Knd.Data.EQUIP.IsTrGateOpened != true)
                    {
                        //pumping
                        UiCommon.Knd.Data.EQUIP.IsTrGateOpened = true;
                    }
                    
                    emSeqProcess = SeqProcess.OnVmtrUnloading;
                    break;
                case SeqProcess.OnVmtrUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = true;
                    MoveVmtrRobot((int)VmtrRobotSt.PICK_MC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = false;
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = true;
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtr = true;
                    emSeqProcess = SeqProcess.OnLlcUnloading;
                    break;
                case SeqProcess.OnLlcUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = true;
                    MoveVmtrRobot((int)VmtrRobotSt.PLACE_LLC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotExtended = false;
                    UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = true;
                    UiCommon.Knd.Data.EQUIP.IsVmtrWorking = false;
                    UiCommon.Knd.Data.EQUIP.IsMaskOnVmtr = false;
                    emSeqProcess = SeqProcess.OnLlcVentUnloading;
                    break;
                case SeqProcess.OnLlcVentUnloading:
                    //LLC VENTING
                    m_thVenting = new Thread(new ThreadStart(DoVent));
                    m_thVenting.Start();
                    waitforsinglesignal.WaitOne();
                    emSeqProcess = SeqProcess.OnAtrHandUnloading;
                    break;
                case SeqProcess.OnAtrHandUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PICK_LLC);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnLlcVacuumUnloading;
                    break;
                case SeqProcess.OnLlcVacuumUnloading:
                    //LLC PUMPING
                    m_bUseWaitEvent = false;
                    m_thPumping = new Thread(new ThreadStart(DoPump));
                    m_thPumping.Start();
                    emSeqProcess = SeqProcess.OnRotatorUnloading;
                    break;
                case SeqProcess.OnRotatorUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PLACE_ROTATOR);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnRotatorAfterRotateUnloading;
                    break;
                case SeqProcess.OnRotatorAfterRotateUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsRotatorWorking = true;
                    RotateMaterial();
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsRotatorWorking = false;
                    emSeqProcess = SeqProcess.OnAtrAfterRotateUnloading;
                    break;
                case SeqProcess.OnAtrAfterRotateUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PICK_ROTATOR);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;
                    emSeqProcess = SeqProcess.OnMtsPodUnloading;
                    break;
#if false
                case SeqProcess.OnFlipperUnloading:
                    emSeqProcess = SeqProcess.OnFlipperAfterFlipUnloading;
                    break;
                case SeqProcess.OnFlipperAfterFlipUnloading:
                    emSeqProcess = SeqProcess.OnAtrAfterFlipUnloading;
                    break;
                case SeqProcess.OnAtrAfterFlipUnloading:
                    emSeqProcess = SeqProcess.OnMtsPodUnloading;
                    break;
#endif
                case SeqProcess.OnMtsPodUnloading:
                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = true;
                    MoveMtsRobot((int)MtsRobotSt.PLACE_LPM);
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsMtsRobotWorking = false;

                    Thread.Sleep(2000);
                    UiCommon.Knd.Data.EQUIP.IsLpmWorking = true;
                    UnloadLPModule();
                    waitforsinglesignal.WaitOne();
                    UiCommon.Knd.Data.EQUIP.IsLpmWorking = false;
                    UiCommon.Knd.Data.EQUIP.IsLpmOpened = false;
                    emSeqProcess = SeqProcess.UnloadingComplete;
                    break;
                case SeqProcess.UnloadingComplete:
                    m_bSeqThreadStop = true;
                    break;
            }
        }

        private void DoPump()
        {
            UiCommon.Knd.Data.EQUIP.IsLlcGateOpened = false;
            Thread.Sleep(2000);
            UiCommon.Knd.Data.EQUIP.IsLlcForelineOpened = false;
            Thread.Sleep(2000);
            UiCommon.Knd.Data.EQUIP.IsLlcSlowRoughOpened = true;
            Thread.Sleep(3000);
            UiCommon.Knd.Data.EQUIP.IsLlcSlowRoughOpened = false;
            UiCommon.Knd.Data.EQUIP.IsLlcFastRoughOpened = true;
            Thread.Sleep(3000);
            UiCommon.Knd.Data.EQUIP.IsLlcFastRoughOpened = false;
            Thread.Sleep(2000);
            UiCommon.Knd.Data.EQUIP.IsLlcForelineOpened = true;
            Thread.Sleep(2000);
            UiCommon.Knd.Data.EQUIP.IsLlcTmpGateOpened = true;
            Thread.Sleep(5000);
            UiCommon.Knd.Data.EQUIP.IsTrGateOpened = true;

            if(m_bUseWaitEvent)
                waitforsinglesignal.Set();
        }

        private void DoVent()
        {
            UiCommon.Knd.Data.EQUIP.IsTrGateOpened = false;
            Thread.Sleep(2000);
            UiCommon.Knd.Data.EQUIP.IsLlcTmpGateOpened = false;
            Thread.Sleep(3000);
            UiCommon.Knd.Data.EQUIP.IsSlowInletValveOpened = true;
            UiCommon.Knd.Data.EQUIP.IsSlowVentValveOpened = true;
            UiCommon.Knd.Data.EQUIP.IsSlowOutletValveOpened = true;
            Thread.Sleep(3000);
            UiCommon.Knd.Data.EQUIP.IsSlowInletValveOpened = false;
            UiCommon.Knd.Data.EQUIP.IsSlowVentValveOpened = false;
            UiCommon.Knd.Data.EQUIP.IsSlowOutletValveOpened = false;


            Thread.Sleep(3000);
            UiCommon.Knd.Data.EQUIP.IsFastInletValveOpened = true;
            UiCommon.Knd.Data.EQUIP.IsFastVentValveOpened = true;
            UiCommon.Knd.Data.EQUIP.IsFastOutletValveOpened = true;
            Thread.Sleep(3000);
            UiCommon.Knd.Data.EQUIP.IsFastInletValveOpened = false;
            UiCommon.Knd.Data.EQUIP.IsFastVentValveOpened = false;
            UiCommon.Knd.Data.EQUIP.IsFastOutletValveOpened = false;
            Thread.Sleep(2000);
            UiCommon.Knd.Data.EQUIP.IsLlcGateOpened = true;

            if (m_bUseWaitEvent)
                waitforsinglesignal.Set();
        }

        public void Stop()
        {
            if (UiCommon.Knd.Data.EQUIP.IsSimulMode)
            {
                if (waitforsinglesignal != null)
                    waitforsinglesignal.Set();
            }

            m_thSequence = null;
        }

        private void BtnGemTestStop_Click(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        private void Sim_MoveNavigationStage(Double dPosX, Double dPosY)
        {
            UiCommon.Knd.Data.EQUIP.NaviPosX = (dPosX / UiCommon.Knd.Var.NaviRealRangeX) * UiCommon.Knd.Var.NaviUIRangeX;
            UiCommon.Knd.Data.EQUIP.NaviPosY = (dPosY / UiCommon.Knd.Var.NaviRealRangeY) * UiCommon.Knd.Var.NaviUIRangeY;
        }

        private void BtnGemTestInit_Click(object sender, RoutedEventArgs e)
        {
            UiCommon.Knd.Data.EQUIP.IsLlcDryPumpWork = true;
            UiCommon.Knd.Data.EQUIP.IsMcDryPumpWork = true;
            UiCommon.Knd.Data.EQUIP.LlcTmpState = 1;
            UiCommon.Knd.Data.EQUIP.McTmpState = 1;
            UiCommon.Knd.Data.EQUIP.IsLlcTmpGateOpened = true;
            UiCommon.Knd.Data.EQUIP.IsMcTmpGateOpened = true;
            UiCommon.Knd.Data.EQUIP.IsLlcForelineOpened = true;
            UiCommon.Knd.Data.EQUIP.IsMcForelineOpened = true;
            UiCommon.Knd.Data.EQUIP.IsTrGateOpened = true;

            UiCommon.Knd.Data.EQUIP.IsMtsRobotInit = true;

            UiCommon.Knd.Data.EQUIP.IsVmtrServoOn = true;
            UiCommon.Knd.Data.EQUIP.IsVmtrInit = true;
            UiCommon.Knd.Data.EQUIP.IsVmtrRobotRetracted = true;

            UiCommon.Knd.Data.EQUIP.IsLpmInit = true;

            RotateMaterial();
            FlipMaterial();

            UiCommon.Knd.Data.EQUIP.IsRotatorInit = true;
            UiCommon.Knd.Data.EQUIP.IsFlipperInit = true;
        }
    }
}
